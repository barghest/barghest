// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#include <iostream> // std::cout
#include <sys/mman.h> // mmap / munmap
#include <cstring> // memset & co, strerror

#include <sys/types.h> // open()
#include <sys/stat.h> // open()
#include <fcntl.h> // open()
#include <unistd.h> // open()

#include <cassert> // assert()
#include <cstdint> // std::*int*_t type
#include <cerrno> // for strerror and errno
#include <fstream> // open/close file

#include <exception> // std::exception
#include <stdexcept> // std::runtime_error

#include <boost/current_function.hpp> // BOOST_CURRENT_FUNCTION

// #include <fstream> // fopen()

#include "Barghest/VM/static_part/symtab/symtab.hpp"

// Supported file format extractor
#include "Barghest/VM/static_part/symtab/elf/symtab_elf.hpp"

namespace Barghest
{
  namespace VM
  {
    namespace static_part
    {
      namespace symtab
      {
        // err                            last_err;
        // std::string                    last_err_msg;

        symtab::symtab(std::string const &file_name)
          : sff_(nullptr)
        {
          if (file_name.size())
            this->symtab::open(file_name);
        }

        symtab::~symtab()
        {
          if (this->sff_)
            delete this->sff_;
        }

        auto    symtab::open(std::string const &file_name) -> void
        {
          int           magic;
          bool          success;
          std::string   err_msg;
          std::string   detailed_err_msg;
          std::ifstream ifs;

          success = false;
          ifs.open(file_name.c_str(), std::ifstream::binary);
          if (ifs && ifs.is_open() && ifs.read((char *)&magic, 4))
          {
            try
            {
              if (*reinterpret_cast<int const *>(ELFMAG) == magic)
                this->sff_ = new elf::symtab_elf(file_name);
              // else if () // Other file format
              else
                throw std::runtime_error("Unsupported file format");
              success = true;
            }
            catch (std::exception &e)
            {
              detailed_err_msg = e.what();
            }
            ifs.close();
          }
          if (success == false)
          {
            err_msg = "Unable to open " + file_name;
            if (detailed_err_msg.size())
              err_msg = err_msg + ": " + detailed_err_msg;
            throw std::runtime_error(err_msg);
          }
          // this->sff_ = new elf::symtab_elf(file_name);
        }

        auto    symtab::close(void) -> void
        {
          if (this->sff_)
            this->sff_->close();
          else
            throw std::logic_error("No file previously opened");
        }

        auto    symtab::get_entry_point(size_t &entry) const -> bool
        {
          if (this->sff_)
            return (this->sff_->get_entry_point(entry));
          throw std::logic_error("No file previously opened");
        }

        auto    symtab::get_file_name(void) const -> std::string const &
        {
          if (this->sff_)
            return (this->sff_->get_file_name());
          throw std::logic_error("No file previously opened");
        }

        auto    symtab::get_mapped_region(std::vector<mapped_region> &mr) const -> void
        {
          if (this->sff_)
            this->sff_->get_mapped_region(mr);
          else
            throw std::logic_error("No file previously opened");
        }

        auto    symtab::free_mapped_region(std::vector<mapped_region> &mr) const -> void
        {
          if (this->sff_)
            this->sff_->free_mapped_region(mr);
          else
            throw std::logic_error("No file previously opened");
        }

      } // !namespace symtab
    } // !namespace static_part
  } // !namespace VM
} // !namespace Barghest
