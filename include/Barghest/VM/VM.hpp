// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#ifndef VM_HPP
# define VM_HPP

# include <map>
# include <boost/config.hpp>
# include <boost/graph/adjacency_list.hpp>

# include "Barghest/VM/static_part/symtab/symtab.hpp"

// TODO: move to an "impl" header ?
# include <capstone/capstone.h> // desassembly engine

# include "Barghest/VM/static_part/parse/block.hpp"

namespace Barghest
{
  namespace VM
  {
    enum class  err : int
    {
      SUCCESS = 0,
        MAX,
        };

    extern err                          last_err;
    extern std::string                  last_err_msg;
    std::map<err, std::string> const    err_list =
    {
      {err::SUCCESS, "Success"},
    };

    // static_assert()

    class VM
    {
    public:
      typedef boost::adjacency_list<boost::setS, boost::vecS, boost::bidirectionalS,
                                    Barghest::VM::static_part::parse::block,
                                    int/*Barghest::core::parse_API::edge*/> BGLGraph;
      typedef BGLGraph::vertex_descriptor                       vertex_descriptor;
      typedef boost::graph_traits<BGLGraph>::vertex_iterator    node_iterator;
      typedef boost::graph_traits<BGLGraph>::in_edge_iterator   in_edge_iterator;
      typedef boost::graph_traits<BGLGraph>::out_edge_iterator  out_edge_iterator;
    private:

      /// The symtab object responsible for dealing with the file format
      static_part::symtab::symtab   sym_;
      /// capstone
      csh                       capstone_handle_;
      std::vector<cs_insn *>    insns_;

      // cs_insn               *inst_list_;
      // size_t                inst_count_;

      BGLGraph              graph_;

      size_t                entry_point_;
      // TODO: a changer
      vertex_descriptor     entry_point_descriptor_;

      /*!
      ** The name of the binary loaded.
      */
      std::string           binary_;

    public:
      VM();
      VM(std::string const &binaryPath);
      ~VM();

      /*!
      ** Load the binary with the given path and initialise all necessary metadata.
      ** If this method is called with an other graph, the first one is cleared.
      ** @param binaryPath The path of the binary to load.
      ** @return A const reference to a BoostGraphLibrary graph.
      */
      auto  parse(std::string const &binaryPath) -> BGLGraph const &;

      /*!
      ** Return the descriptor of the vertex containing the block where is situated
      ** the entry point. (by default the node '0' if the BGL graph
      ** is always the root)
      */
      auto  entry_point(void) const -> vertex_descriptor;

      /*!
      ** @return A const reference to a BoostGraphLibrary graph.
      */
      auto  get_BGL_graph(void) const -> BGLGraph const &;

      // /*!
      // ** Create a boost graph from an instruction list.
      // ** Note: this function clear the old graph.
      // */
      // template <template <typename, typename ...ARGS> class I>
      // auto  insns_to_graph(I const &insns, BGLGraph &g) const -> void;

      /*!
      ** helper to work with the graph
      */

    private:
      auto    init_capstone_(void) -> bool;
      auto    init_boost_graph_(void) -> void;
      auto    first_pass_create_basic_bloc_(void) -> void;
      auto    extract_basic_block(void) -> void;

    };
  } // !namespace VM
} // !namespace Barghest

#endif /* !VM_HPP */
