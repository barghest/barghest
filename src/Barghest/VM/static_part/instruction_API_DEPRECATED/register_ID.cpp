// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#include <dyninst/Register.h>

#include "Barghest/core/dyninst_helper.hpp"
#include "Barghest/core/instruction_API/register_ID.hpp"

/*
** Stringification
*/
#define _XSTR(s) _STR(s)
#define _STR(s) #s

/*
** Ease the initialisation of all the predefined global.
** These global are designed for fast comparaison. See the header files
** for more informations.
*/
#define INIT_REF_REGISTER(name, size, aliasing, base, category, namespace) \
  register_ID const name = {{{size, aliasing, base, category}}, #name,  \
                            _XSTR(({name, aliasing, base, namespace, category}))}

#define INIT_REF_GPR_REGISTER(name, size, aliasing, base, namespace)    \
  INIT_REF_REGISTER(name, size, aliasing, base, category::GPR, x86)

namespace Barghest
{
  namespace core
  {
    namespace instruction_API
    {
      auto register_ID::operator=(register_ID const &other) -> register_ID &
      {
        ID = other.ID;
        name = other.name;
        return *this;
      }

      auto register_ID::operator=(uint32_t ID) -> register_ID &
      {
        ID = ID;
        name.clear();
        return *this;
      }

      auto operator<<(std::ostream &a, register_ID const &other)
        -> std::ostream &
      {
        a << other.name;
        return a;
      }

      auto operator==(const register_ID &a, const register_ID &b) -> bool
      {
        return a.ID == b.ID;
      }

      namespace x86 // 32b
      {
        /* A BASE */
        INIT_REF_GPR_REGISTER(al,  8,  aliasing::L,     base::A, x86);
        INIT_REF_GPR_REGISTER(ah,  8,  aliasing::H,     base::A, x86);
        INIT_REF_GPR_REGISTER(ax,  16, aliasing::W,     base::A, x86);
        INIT_REF_GPR_REGISTER(eax, 32, aliasing::FULL,  base::A, x86);
        /* B BASE */
        INIT_REF_GPR_REGISTER(bl,  8,  aliasing::L,     base::B, x86);
        INIT_REF_GPR_REGISTER(bh,  8,  aliasing::H,     base::B, x86);
        INIT_REF_GPR_REGISTER(bx,  16, aliasing::W,     base::B, x86);
        INIT_REF_GPR_REGISTER(ebx, 32, aliasing::FULL,  base::B, x86);
        /* C BASE */
        INIT_REF_GPR_REGISTER(cl,  8,  aliasing::L,     base::C, x86);
        INIT_REF_GPR_REGISTER(ch,  8,  aliasing::H,     base::C, x86);
        INIT_REF_GPR_REGISTER(cx,  16, aliasing::W,     base::C, x86);
        INIT_REF_GPR_REGISTER(ecx, 32, aliasing::FULL,  base::C, x86);
        /* D BASE */
        INIT_REF_GPR_REGISTER(dl,  8,  aliasing::L,     base::D, x86);
        INIT_REF_GPR_REGISTER(dh,  8,  aliasing::H,     base::D, x86);
        INIT_REF_GPR_REGISTER(dx,  16, aliasing::W,     base::D, x86);
        INIT_REF_GPR_REGISTER(edx, 32, aliasing::FULL,  base::D, x86);

        /* DI BASE */
        // INIT_REF_GPR_REGISTER(dil, 8, aliasing::L, base::DI, x86); // No 8b reg in 32b for DI
        INIT_REF_GPR_REGISTER(di,  16, aliasing::W,     base::DI, x86);
        INIT_REF_GPR_REGISTER(edi, 32, aliasing::FULL,  base::DI, x86);

        /* SI BASE */
        // INIT_REF_GPR_REGISTER(sil, 8, aliasing::L, base::SI, x86); // No 8b reg in 32b for SI
        INIT_REF_GPR_REGISTER(si,  16, aliasing::W,     base::SI, x86);
        INIT_REF_GPR_REGISTER(esi, 32, aliasing::FULL,  base::SI, x86);

        /* BP BASE */
        // INIT_REF_GPR_REGISTER(bpl, 8, aliasing::L, base::BP, x86); // No 8b reg in 32b for BP
        INIT_REF_GPR_REGISTER(bp,  16, aliasing::W,     base::BP, x86);
        INIT_REF_GPR_REGISTER(ebp, 32, aliasing::FULL,  base::BP, x86);

        /* SP BASE */
        // INIT_REF_GPR_REGISTER(spl, 8, aliasing::L, base::SP, x86); // No 8b reg in 32b for SP
        INIT_REF_GPR_REGISTER(sp,  16, aliasing::W,     base::SP, x86);
        INIT_REF_GPR_REGISTER(esp, 32, aliasing::FULL,  base::SP, x86);

        INIT_REF_GPR_REGISTER(eip, 32, aliasing::FULL,  base::IP, x86);

        /* EFLAGS */
        INIT_REF_REGISTER(eflags, 32, aliasing::FULL, base::FLAGS, category::FLAGS, x86);

        /* TODO: Revoir sur comment gerer les flags sans conflits avec x86::base::*/
        INIT_REF_REGISTER(cf,   1, aliasing::FULL, base::CF,   category::FLAG, x86);
        INIT_REF_REGISTER(pf,   1, aliasing::FULL, base::PF,   category::FLAG, x86);
        INIT_REF_REGISTER(af,   1, aliasing::FULL, base::AF,   category::FLAG, x86);
        INIT_REF_REGISTER(zf,   1, aliasing::FULL, base::ZF,   category::FLAG, x86);
        INIT_REF_REGISTER(sf,   1, aliasing::FULL, base::SF,   category::FLAG, x86);
        INIT_REF_REGISTER(tf,   1, aliasing::FULL, base::TF,   category::FLAG, x86);
        /* called "if_" as the 'if' is a C reserved keyword */
        INIT_REF_REGISTER(if_,  1, aliasing::FULL, base::IF,   category::FLAG, x86);
        INIT_REF_REGISTER(df,   1, aliasing::FULL, base::DF,   category::FLAG, x86);
        INIT_REF_REGISTER(of,   1, aliasing::FULL, base::OF,   category::FLAG, x86);
        INIT_REF_REGISTER(iopl, 1, aliasing::FULL, base::IOPL, category::FLAG, x86);
        INIT_REF_REGISTER(nt,   1, aliasing::FULL, base::NT,   category::FLAG, x86);
        INIT_REF_REGISTER(rf,   1, aliasing::FULL, base::RF,   category::FLAG, x86);
        INIT_REF_REGISTER(vm,   1, aliasing::FULL, base::VM,   category::FLAG, x86);
        INIT_REF_REGISTER(ac,   1, aliasing::FULL, base::AC,   category::FLAG, x86);
        INIT_REF_REGISTER(vif,  1, aliasing::FULL, base::VIF,  category::FLAG, x86);
        INIT_REF_REGISTER(vip,  1, aliasing::FULL, base::VIP,  category::FLAG, x86);
        INIT_REF_REGISTER(id,   1, aliasing::FULL, base::ID,   category::FLAG, x86);

        /* SEGMENT REGISTERS */
        INIT_REF_REGISTER(cs, 32, aliasing::FULL, base::CS, category::SEG, x86);
        INIT_REF_REGISTER(ds, 32, aliasing::FULL, base::DS, category::SEG, x86);
        INIT_REF_REGISTER(ss, 32, aliasing::FULL, base::SS, category::SEG, x86);
        INIT_REF_REGISTER(es, 32, aliasing::FULL, base::ES, category::SEG, x86);
        INIT_REF_REGISTER(fs, 32, aliasing::FULL, base::FS, category::SEG, x86);
        INIT_REF_REGISTER(gs, 32, aliasing::FULL, base::GS, category::SEG, x86);
      }

#if defined(__x86_64__) || defined(__ia64__) || defined(_WIN64)
      namespace x86_64 // 64b
      {
        /*
        ** 32b register aliasing are DWORD in 64b and not FULL anymore
        ** because there is a greater register size above.
        */
        INIT_REF_GPR_REGISTER(eax, 32, aliasing::DW, base::A, x86_64);
        INIT_REF_GPR_REGISTER(ebx, 32, aliasing::DW, base::B, x86_64);
        INIT_REF_GPR_REGISTER(ecx, 32, aliasing::DW, base::C, x86_64);
        INIT_REF_GPR_REGISTER(edx, 32, aliasing::DW, base::D, x86_64);

        /* DI and SI have a low 8b reg in x86_64 */
        INIT_REF_GPR_REGISTER(dil, 8, aliasing::L, base::DI, x86_64);
        INIT_REF_GPR_REGISTER(sil, 8, aliasing::L, base::SI, x86_64);

        INIT_REF_GPR_REGISTER(edi, 32, aliasing::DW, base::DI, x86_64);
        INIT_REF_GPR_REGISTER(esi, 32, aliasing::DW, base::SI, x86_64);

        /* BP and SP have a low 8b reg in x86_64 */
        INIT_REF_GPR_REGISTER(bpl, 8, aliasing::L, base::BP, x86_64);
        INIT_REF_GPR_REGISTER(spl, 8, aliasing::L, base::SP, x86_64);

        INIT_REF_GPR_REGISTER(ebp, 32, aliasing::DW, base::BP, x86_64);
        INIT_REF_GPR_REGISTER(esp, 32, aliasing::DW, base::SP, x86_64);

        INIT_REF_GPR_REGISTER(eflags, 32, aliasing::DW, base::FLAGS, x86_64);

        INIT_REF_GPR_REGISTER(eip, 32, aliasing::DW, base::IP, x86_64);

        /* 64b GPR registers */
        INIT_REF_GPR_REGISTER(rax, 64, aliasing::FULL, base::A, x86_64);
        INIT_REF_GPR_REGISTER(rbx, 64, aliasing::FULL, base::B, x86_64);
        INIT_REF_GPR_REGISTER(rcx, 64, aliasing::FULL, base::C, x86_64);
        INIT_REF_GPR_REGISTER(rdx, 64, aliasing::FULL, base::D, x86_64);

        INIT_REF_GPR_REGISTER(rdi, 64, aliasing::FULL, base::DI, x86_64);
        INIT_REF_GPR_REGISTER(rsi, 64, aliasing::FULL, base::SI, x86_64);

        INIT_REF_GPR_REGISTER(rbp, 64, aliasing::FULL, base::BP, x86_64);
        INIT_REF_GPR_REGISTER(rsp, 64, aliasing::FULL, base::SP, x86_64);

        INIT_REF_GPR_REGISTER(rflags, 64, aliasing::FULL, base::FLAGS, x86_64);

        INIT_REF_GPR_REGISTER(rip, 64, aliasing::FULL, base::IP, x86_64);
        /*
        ** Ease the declaration of R8 --> R15 registers.
        ** Exemple: INIT_RN_REG(8); // Do not forget the ';' at the end
        ** Internal note: I miss the preprocessing of nasm which is much more usefull.
        */
# define INIT_RN_REG(number)                                            \
        INIT_REF_GPR_REGISTER(r##number##b, 8, aliasing::L, base::R##number, x86_64); \
        INIT_REF_GPR_REGISTER(r##number##w, 16, aliasing::W, base::R##number, x86_64); \
        INIT_REF_GPR_REGISTER(r##number##d, 32, aliasing::DW, base::R##number, x86_64); \
        INIT_REF_GPR_REGISTER(r##number, 64, aliasing::FULL, base::R##number, x86_64)

        /* R8 --> R15 registers */
        INIT_RN_REG(8);
        INIT_RN_REG(9);
        INIT_RN_REG(10);
        INIT_RN_REG(11);
        INIT_RN_REG(12);
        INIT_RN_REG(13);
        INIT_RN_REG(14);
        INIT_RN_REG(15);
      }
#endif /* 64b */

      std::map<signed int /* DyninstId */, register_ID const & /* BarghestId */> const g_regLinkTable =
      {
#define MAP_INIT_REG(name, namespace) {get_mach_register_val(Dyninst::namespace::name), namespace::name}

#if defined(__x86_64__) || defined(__ia64__) || defined(_WIN64)
# define MAP_INIT_SHARED(name) MAP_INIT_REG(name, x86), MAP_INIT_REG(name, x86_64)
#else /* 32b (we do not want anything to be stored for the x86_64 namespace if we are not in) */
# define MAP_INIT_SHARED(name) MAP_INIT_REG(name, x86)/*, MAP_INIT_REG(name, x86_64)*/
#endif /* !64b */

        // {getMachRegisterVal(Dyninst::x86::al), x86::al},
        MAP_INIT_SHARED(al), MAP_INIT_SHARED(ah), MAP_INIT_SHARED(ax), MAP_INIT_SHARED(eax),
        MAP_INIT_SHARED(bl), MAP_INIT_SHARED(bh), MAP_INIT_SHARED(bx), MAP_INIT_SHARED(ebx),
        MAP_INIT_SHARED(cl), MAP_INIT_SHARED(ch), MAP_INIT_SHARED(cx), MAP_INIT_SHARED(ecx),
        MAP_INIT_SHARED(dl), MAP_INIT_SHARED(dh), MAP_INIT_SHARED(dx), MAP_INIT_SHARED(edx),

        /* , , */ MAP_INIT_SHARED(di), MAP_INIT_SHARED(edi), /* In 32b no 8b reg for DI, SI, BP and SP */
        /* , , */ MAP_INIT_SHARED(si), MAP_INIT_SHARED(esi),

        /* , , */ MAP_INIT_SHARED(bp), MAP_INIT_SHARED(ebp),
        /* , , */ MAP_INIT_SHARED(sp), MAP_INIT_SHARED(esp),

        MAP_INIT_SHARED(eip),


        // MAP_INIT_SHARED(eflags) // Not in dyninst (they use flag (CF / ZF / etc) directly instead)

        MAP_INIT_SHARED(cf), MAP_INIT_SHARED(pf), MAP_INIT_SHARED(af),
        MAP_INIT_SHARED(zf), MAP_INIT_SHARED(sf), MAP_INIT_SHARED(tf),
        MAP_INIT_SHARED(if_), MAP_INIT_SHARED(df), MAP_INIT_SHARED(of),
        // MAP_INIT_SHARED(iopl), // Not in Dyninst
        // MAP_INIT_SHARED(nt), // Not in Dyninst
        // MAP_INIT_SHARED(rf),  MAP_INIT_SHARED(vm), MAP_INIT_SHARED(ac),  // Not in Dyninst
        // MAP_INIT_SHARED(vif), MAP_INIT_SHARED(vip), MAP_INIT_SHARED(id), // Not in Dyninst

        MAP_INIT_SHARED(cs), MAP_INIT_SHARED(ds), MAP_INIT_SHARED(ss),
        MAP_INIT_SHARED(es), MAP_INIT_SHARED(fs), MAP_INIT_SHARED(gs),

#if defined(__x86_64__) || defined(__ia64__) || defined(_WIN64)

        MAP_INIT_REG(dil, x86_64), MAP_INIT_REG(sil, x86_64), /* dil/sil allowed in x86_64 */
        MAP_INIT_REG(bpl, x86_64), MAP_INIT_REG(spl, x86_64), /* bpl/spl allowed in x86_64 */

        // MAP_INIT_REG(rflags, x86_64) // Not in dyninst (they use flag (CF / ZF / etc) directly instead)

        MAP_INIT_REG(rax, x86_64), MAP_INIT_REG(rbx, x86_64),
        MAP_INIT_REG(rcx, x86_64), MAP_INIT_REG(rdx, x86_64),
        MAP_INIT_REG(rdi, x86_64), MAP_INIT_REG(rsi, x86_64),
        MAP_INIT_REG(rbp, x86_64), MAP_INIT_REG(rsp, x86_64),

        MAP_INIT_REG(rip, x86_64),

        /* Macro to auto declare all the combinaison of R[8-15] register */
# define MAP_INIT_RN_REG(number)                \
        MAP_INIT_REG(r##number##b, x86_64),     \
        MAP_INIT_REG(r##number##w, x86_64),     \
        MAP_INIT_REG(r##number##d, x86_64),     \
        MAP_INIT_REG(r##number, x86_64)

        /**/
        MAP_INIT_RN_REG(8), MAP_INIT_RN_REG(9), MAP_INIT_RN_REG(10), MAP_INIT_RN_REG(11),
        MAP_INIT_RN_REG(12), MAP_INIT_RN_REG(13), MAP_INIT_RN_REG(14), MAP_INIT_RN_REG(15),

#endif /* !64b */
      };
    }
  }
}
