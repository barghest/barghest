#pragma once

#ifndef BARGHEST_BLOCK_HH_
# define BARGHEST_BLOCK_HH_

# include <map>
# include <ostream>
# include <iostream>

# include <dyninst/CFG.h>

# include "Barghest/core/instruction_API/instruction.hh"

namespace Barghest
{
  namespace core
  {
    namespace parse_API
    {
      class block
      {
      public:
	using insns = std::map<size_t, Barghest::core::instruction_API::instruction>;

	block();
	block(Dyninst::ParseAPI::Block const *block);

	/*!
	** Set this class with the Dyninst block type to wrap.
	** @param block The Dyninst block
	** @return A reference on this (the Barghest block)
	*/
	auto set_block(Dyninst::ParseAPI::Block const *) -> block &;
	/*!
	** @return a pointer to the internal Dyninst block type.
	*/
	auto get_block(void) const -> Dyninst::ParseAPI::Block const *;
	/*!
	** Pretty print all the instruction containted in the basic block.
	** @param stream The stream to pretty print on. By default it is set
	** to std::cout.
	** TODO : a function that prints should not be called "format"
	*/
	auto format(std::ostream& stream = std::cout) const -> void;

	/*!
	** All the following methods are Dyninst method direct overload.
	*/
	auto start(void) const -> size_t;
	auto end(void) const -> size_t;
	auto last(void) const -> size_t;
	auto last_insn_addr(void) const -> size_t;
	auto size(void) const -> size_t;
	/*!
	** @return all the (cached) instruction of the current block.
	** Internal notes: This method return a const ref on a Insns already
	**                 cached, this is the fastest way.
	*/
	auto get_insns(void) const -> insns const &;
	/*!
	** Append to insns all the instructions of the current block.
	** This method create a new set of Barghest::* wrapper
	** which do not interfer with the one in the graph. It is thread safe.
	** @param insns The destination list to store the instructions on.
	*/
	auto get_insns(insns &insns) const -> void;
	/*!
	** Return a single (cached) instruction in this block using the offset.
	** Like getInsns(void), it uses the precached decoded instruction.
	*/
	auto get_insn(size_t offset) const
	  -> instruction_API::instruction const &;
      private:
	/*!
	** The Raw Dyninst block.
	*/
	Dyninst::ParseAPI::Block const *block_;
	/*!
	** List of Dyninst instructons.
	*/
	Dyninst::ParseAPI::Block::Insns dyninst_instructions_;
	/*!
	** A precached list of all the instruction of the block.
	*/
	insns insns_;

      }; // !class block
    } // !namespace parse_API
  } // !namespace core
} // !namespace Barghest

auto operator<(Barghest::core::parse_API::block const &,
	       Barghest::core::parse_API::block const &) -> bool;
auto operator>(Barghest::core::parse_API::block const &,
	       Barghest::core::parse_API::block const &) -> bool;

#endif /* !BARGHEST_BLOCK_HH_ */
