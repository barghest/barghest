// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#include <iostream>    // std::cout

#include <boost/fusion/container/generation/make_list.hpp> // boost::make_list
#include <boost/fusion/include/make_list.hpp> // boost::make_list

// #include <algorithm>
// #include <boost/graph/graphviz.hpp>
// #include <boost/property_map/property_map.hpp>

/* Boost visitor algorithm */
// #include <boost/graph/visitors.hpp>
// #include <boost/graph/adjacency_list.hpp>
// #include <boost/graph/breadth_first_search.hpp>
// #include <boost/graph/depth_first_search.hpp>
// #include <boost/graph/graph_traits.hpp>

/* ?? */
// #include <boost/graph/graph_utility.hpp>

#include "Barghest/core/emulation_API/graph_visitor.hh"
#include "Barghest/core/emulation_API/dfs_events.hh"
#include "Barghest/core/emulation_API/barghest_depth_first_search.hpp"
// #include "Barghest/core/emulation_API/boost_event_filters/boost_event_filters.hh"

namespace Barghest
{
  namespace core
  {
    namespace emulation_API
    {

      graph_visitor::graph_visitor(Barghest::core::graph const &g)
        : g_(g)
      {
        // this->color_map_ = get(boost::vertex_color, g.get_boost_graph());
      }

      graph_visitor::~graph_visitor()
      {
      }

      auto    graph_visitor::get_context(void) -> context &
      {
        return (this->context_);
      }

      auto    graph_visitor::get_barghest_graph(void) const -> graph const &
      {
        return (this->g_);
      }
      // auto    graph_visitor::get_barghest_graph(void) -> graph &
      // {
      //     return (this->g_);
      // }

      auto    graph_visitor::start(void) -> void
      {
        dfs_events    boost_dfs_events(*this);

        // boost_event_filters::on_discover_vertex    on_discover_vertex_;

        // std::cout << sizeof(check_nodes::event_filter) << std::endl;
        std::cout << "*** Depth First ***" << std::endl;
        // boost::depth_first_search
        //   (this->get_barghest_graph().get_boost_graph(),
        //    visitor(make_dfs_visitor(boost::fusion::make_list(
        //                   /* boost_event_filters is a Barghest sub namespace  */
        //                   boost_event_filters::on_discover_vertex(*this, context_)
        //                   // boost_event_filters::on_examine_edge(*this, context_),
        //                   // boost_event_filters::on_finish_vertex(*this, context_),
        //                   // boost_event_filters::on_start_vertex(*this, context_)
        //                   ))));

#if 1
        boost::depth_first_search(this->get_barghest_graph().get_boost_graph(),
                                  boost::visitor(boost_dfs_events));

#else
        typedef typename boost::property_map<Barghest::core::parse_API::graph,
                                             boost::vertex_color_t>::type ColorMap;
        ColorMap color_map = get(boost::vertex_color,
                                 this->get_barghest_graph().get_boost_graph());

        boost::depth_first_search(this->get_barghest_graph().get_boost_graph(),
                                  boost_dfs_events,
                                  // this->color_map_,
                                  color_map,
                                  boost::detail::get_default_starting_vertex(this->get_barghest_graph().get_boost_graph()),
                                  boost::detail::nontruth2(),
                                  boost::detail::nontruth2(),
                                  boost::detail::nontruth2());
#endif

        std::cout << std::endl;
      }

      auto    graph_visitor::stop(void) -> void // Not implemented yet
      {
      }

      auto    graph_visitor::push_context(void) -> void
      {
        // std::cout << "==> Stack size before " << _contextStack.size() << std::endl;
        context_stack_.push_back(this->get_context());
        // std::cout << "Stack size after " << _contextStack.size() << std::endl;
      }

      auto    graph_visitor::pop_context(void) -> void
      {
        // std::cout << "Pop content" << std::endl;
        this->get_context() = context_stack_.back();
        // std::cout << "Pop content1" << std::endl;
        context_stack_.pop_back();
        // std::cout << "Pop content End" << std::endl;
      }

      // virtual auto    onInstruction(std::set<Instruction>) -> void; // Filtre
      auto    graph_visitor::on_instruction(std::pair<size_t,
                                            Barghest::core::instruction_API::instruction const &>) -> void
      {
      }

      auto    graph_visitor::on_back_edge(void) -> void // Not implemented yet
      {
      }

    } // !namespace emulation_API
  } // !namespace core
} // !namespace Barghest
