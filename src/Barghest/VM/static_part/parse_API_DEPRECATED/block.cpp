// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#include <map>
#include <stdexcept>

#include "Barghest/core/parse_API/block.hh"

namespace Barghest
{
  namespace core
  {
    namespace parse_API
    {
      block::block() : block_(nullptr)
      { }

      block::block(Dyninst::ParseAPI::Block const *block)
      {
        this->set_block(block);
      }

      auto block::set_block(Dyninst::ParseAPI::Block const *block_param) -> block &
      {
        this->block_ = block_param;

        // Clear instructions stored by a previous set_block(...)
        this->dyninst_instructions_.clear();
        // Get new dyninst instructions list
        this->block_->getInsns(this->dyninst_instructions_);
        // Store and create new Barghest instructions list
        this->get_insns(insns_);
        return (*this);
      }

      auto block::get_block(void) const -> Dyninst::ParseAPI::Block const *
      {
        return (this->block_);
      }

      auto block::format(std::ostream& stream) const -> void
      {
        using ins_map = std::map<Dyninst::Offset, Dyninst::InstructionAPI::Instruction::Ptr>;
        ins_map insns;

        auto flags_backup = stream.flags();
        if (get_block())
        {
          get_block()->getInsns(insns);
          for (auto inst : insns)
          {
            stream << std::hex << std::showbase << inst.first << ": "
                   << inst.second->format() << std::endl;
          }
        }
        stream.setf(flags_backup);
      }

      auto block::start(void) const -> size_t
      {
        return get_block()->start();
      }

      auto block::end(void) const -> size_t
      {
        return get_block()->end();
      }

      auto block::last(void) const -> size_t
      {
        return get_block()->last();
      }

      auto block::last_insn_addr(void) const -> size_t
      {
        return get_block()->lastInsnAddr();
      }

      auto block::size(void) const -> size_t
      {
        return get_block()->size();
      }

      auto block::get_insns(void) const -> insns const &
      {
        return insns_;
      }

      auto block::get_insns(insns &insns) const -> void
      {
        insns.insert(dyninst_instructions_.begin(),
                     dyninst_instructions_.end());
      }

      auto block::get_insn(size_t offset) const
        -> instruction_API::instruction const &
      {
        auto it = get_insns().find(offset);

        if (it != get_insns().end())
          return ((*it).second);
        throw std::out_of_range("BarghestBlock:getInsn(): Invalid offset value.");
      }

    }
  }
}
