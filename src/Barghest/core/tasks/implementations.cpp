// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#include <map>
#include <cstring>
#include <utility>

#include <dyninst/CodeObject.h>
#include <dyninst/CodeSource.h>

#include "Barghest/core/tasks/implementations.hpp"

namespace Barghest
{
  namespace core
  {
    namespace tasks
    {
      auto do_parse_file(std::string const &path, ::Barghest::core::graph &graph)
        -> void
      {
        // the strdup is ugly, but required for dyninst
        auto sts = new Dyninst::ParseAPI::SymtabCodeSource(strdup(path.c_str()));
        auto co = new Dyninst::ParseAPI::CodeObject(sts);
        auto entry_point = sts->getSymtabObject()->getEntryOffset();

        // co->parse(entry_point, true);
        co->parse(0x400644, true);
        co->finalize();

        auto && g = graph.get_boost_graph();
        parse_API::graph::vertex_descriptor block_decs;
        std::map<parse_API::graph::vertex_descriptor,
                 Dyninst::ParseAPI::Block const *> map_id_block;
        std::map<Dyninst::ParseAPI::Block const *,
                 parse_API::graph::vertex_descriptor> map_block_id;
        std::pair<parse_API::graph::vertex_iterator,
                  parse_API::graph::vertex_iterator> bound;
        auto entry_point_descriptor = boost::add_vertex(g);

        auto all = co->funcs();
        for (auto &function : co->funcs())
        {
          for (auto block : function->blocks())
          {
            if ((block->low() <= entry_point
                 && entry_point <= block->high()) == false)
              block_decs = boost::add_vertex(g);
            else
              block_decs = entry_point_descriptor;
            map_id_block[block_decs] = block;
            map_block_id[block] = block_decs;
          }
        }

        Dyninst::ParseAPI::Block const *dyn_block;
        parse_API::graph::vertex_descriptor bgl_block_id;
        std::set<std::pair<parse_API::graph::vertex_descriptor,
                           parse_API::graph::vertex_descriptor>> remove_duplicate;
        parse_API::edge edge_property;

        for (auto & map_block_desc : map_block_id)
        {
          dyn_block = map_block_desc.first;
          bgl_block_id = map_block_desc.second;
          g[bgl_block_id].set_block(dyn_block);
          for (auto out_edge_it = dyn_block->targets().begin();
               out_edge_it != dyn_block->targets().end(); out_edge_it++)
          {
            auto pair = std::make_pair(map_block_id[(*out_edge_it)->src()],
                                       map_block_id[(*out_edge_it)->trg()]);
            if (remove_duplicate.find(pair) == remove_duplicate.end())
            {
              edge_property.set_edge(*out_edge_it);
              boost::add_edge(map_block_id[(*out_edge_it)->src()],
                              map_block_id[(*out_edge_it)->trg()],
                              edge_property, g);
              remove_duplicate.insert(pair);
            }
          }
        }

        graph.set_co(co);
        graph.set_stcs(sts);
        graph.set_path(path);
      }

    } // !namespace tasks
  } // !namespace core
} // !namespace Barghest
