// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#include <cassert>

#include "Barghest/core/instruction_API/operand.hh"
#include "Barghest/core/instruction_API/expressions/register.hh"

#include "Barghest/core/instruction_API/expressions/binary_function.hh"
#include "Barghest/core/instruction_API/expressions/dereference.hh"
#include "Barghest/core/instruction_API/expressions/immediate.hh"
#include "Barghest/core/dyninst_helper.hpp"

namespace Barghest
{
  namespace core
  {
    namespace instruction_API
    {
      operand::operand(Dyninst::InstructionAPI::Operand const &dyn_operand,
                       Dyninst::Architecture arch)
        : op_(dyn_operand), arch_(arch), expr_(init_operand_expr(dyn_operand))
      { }

      operand::operand(operand &&other)
        : op_(std::move(other.op_))
      {
        arch_ = other.arch_;
        expr_ = other.expr_;
        other.expr_ = nullptr;
        std::cout << "Operand move constructor called" << std::endl;
      }

      operand::~operand()
      {
        delete this->expr_;
      }

      // auto operand::get_value(void) const -> MultiValEmulator<size_t> const &
      // {
      //     return (this->get_expr().get_value());
      // }

      // auto operand::set_value(emulation_API::context &,
      //                   MultiValEmulator<size_t> const &val) const -> void
      // {
      //     // this->expr_.set_value(val);
      // }

      auto operand::is_read(void) const -> bool
      {
        return (get_raw().isRead());
      }

      auto operand::is_written(void) const -> bool
      {
        return (get_raw().isWritten());
      }

      auto operand::reads_memory(void) const -> bool
      {
        return (get_raw().readsMemory());
      }

      auto operand::writes_memory(void) const -> bool
      {
        return (get_raw().writesMemory());
      }

      auto operand::format(size_t addr) const -> std::string
      {
        return (get_raw().format(arch_, addr));
      }

      auto operand::get_expr(void) const -> expression const &
      {
        return (*this->expr_);
      }

      auto operand::get_raw(void) const -> Dyninst::InstructionAPI::Operand const &
      {
        return (op_);
      }

      auto operand::get_raw_expr(void) const -> Dyninst::InstructionAPI::Expression const &
      {
        return (*get_raw().getValue());
      }

      auto operand::get_arch(void) const -> Dyninst::Architecture
      {
        return (arch_);
      }
    }
  }
}
