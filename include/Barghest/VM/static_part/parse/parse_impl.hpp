// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#ifndef PARSE_IMPL_HPP
# error "This a private header, include parse.hpp instead."
#endif

#include <iostream>
# include <type_traits> // std::add_lvalue_reference, std::remove_reference
#include <capstone/capstone.h> // capstone disassembler engine
#include "Barghest/VM/static_part/parse/parse.hpp"
#include "Barghest/VM/static_part/symtab/symtab.hpp"

template <template <typename, typename ...ARGS> class I>
class disassemble_linear_dumb
  : public decompiler<I, symtab::symtab, csh>
{
public:
  using decompiler<I, symtab::symtab, csh>::decompiler;
  auto  run(void) -> bool;
};

template <template <typename, typename ...ARGS> class I>
auto  disassemble_linear_dumb<I>::run(void) -> bool
{
  bool          ret;
  void          *mem;
  size_t        mem_size;
  size_t        addr;
  cs_insn       *insn;
  typename std::remove_reference<decltype(this->get_insns())>::type    insns;
  //decltype(auto) symtab = this->get_symtab();
  //decltype(auto) disass_engine = this->get_disass();
  auto          &symtab = this->get_symtab();
  auto          &disass_engine = this->get_disass();
  std::vector<static_part::symtab::mapped_region>   mr;

  mem = nullptr;
  mem_size = 0;
  symtab.get_mapped_region(mr);
  for (auto &region : mr)
  {
    std::cout << "Looping over section " << region.name << std::endl;
    if (region.name == ".text")
    {
      std::cout << ".text found" << std::endl;
      mem = region.mem;
      mem_size = region.size;
      addr = 0x1000;
      do {
        insn = cs_malloc(disass_engine);
        assert(insn && "Unable to alloc memory for capstone");
        ret = cs_disasm_iter(disass_engine,
                             (uint8_t const **)(&mem), &mem_size,
                             &addr, insn);
        if (ret)
          insns.push_back(insn);
        else
          cs_free(insn, 1);
      } while (ret);

      // std::cout << "instruction nb: " << insns.size() << std::endl;
      for (auto const &insn : insns)
      {
        printf("0x%x:\t%s\t%s\n",
               insn->address,
               insn->mnemonic,
               insn->op_str);
      }
      // commit transaction
      this->get_insns().reserve(this->get_insns().size() + insns.size());
      this->get_insns().insert(this->get_insns().end(), insns.begin(), insns.end());
      return (true);
      // }
      // else
      //   std::cerr << fct << "No instruction were disassembled" << std::endl;
    }
  }
  for (auto &insn : insns) // Clear non-commited memory
    cs_free(insn, 1);
  return (false);
}
