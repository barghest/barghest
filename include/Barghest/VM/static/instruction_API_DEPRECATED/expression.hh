// #pragma once
#ifndef BARGHEST_EXPRESSION_HH__
# define BARGHEST_EXPRESSION_HH__

# include <string>
# include "MultiValEmulator.hh"

namespace Barghest
{
  namespace core
  {
    namespace instruction_API
    {
      /*
      ** @todo add documention
      */
      class expression
      {
      public:
	enum e_sub_class_type
	{
	  UNKNOWN = 0,
	  REGISTER,
	  IMMEDIATE,
	  DEREFERENCE,
	  BINARY_FUNCTION,
	  ENUM_SIZE
	};

	e_sub_class_type const	e_;
      public:
	expression(e_sub_class_type = expression::UNKNOWN);
	virtual ~expression() = default;

	/*!
	** Return a std::string containing a prettyprint of the object represented.
	** TODO: ajouter un exemple de format
	*/
	virtual auto format(void) const -> std::string;

	/*!
	** Those method provide an easier and faster way than
	** typeid(myclass) == typeid(Barghest::InstructionAPI::Register) or
	** using dynamic_cast<>(...) to know what class are behind an Expression.
	** @return A boolean, true if this expression is the same as requested.
	*/
	virtual auto is_reg(void) const -> bool;
	virtual auto is_imm(void) const -> bool;
	virtual auto is_deref(void) const -> bool;
	virtual auto is_bin_func(void) const -> bool;

	/*!
	** Same as is_* but use the enum to switch faster and allows static_cast<>()
	*/
	auto	get_sub_class_type(void) const -> e_sub_class_type;

      }; // !class expression

    } // !namespace instruction_API
  } // !namespace core
} // !namespace Barghest

#endif /* !BARGHEST_EXPRESSION_HH__ */
