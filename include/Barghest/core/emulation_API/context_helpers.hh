#ifndef BARGHEST_CONTEXT_HELPERS_HH_
# define BARGHEST_CONTEXT_HELPERS_HH_

# include <vector>

# include "Barghest/core/instruction_API/operand.hh"
# include "Barghest/core/emulation_API/context.hh"
# include "MultiValEmulator.hh"

namespace Barghest
{
  namespace core
  {
    namespace emulation_API
    {

      /*!
      ** Return all values of the given operand. This function is usefull in
      ** case of a memory dereferencement where it contains each older write
      ** values on the given address unmerged.
      ** Note: Reference of the vector contains variable stored staticaly in the
      ** context. They change each time you call a get_*value*() function.
      */
      auto get_values(context &c, instruction_API::operand const &op,
		      std::vector<MultiValEmulator<size_t> const *> &values)
	throw(std::runtime_error) -> void;


      /*!
      ** @return The merge of all the values of the operand stored in the given context.
      ** @exception std::runtime_error if the operand contains an invalid expression.
      ** See @expression for more details.
      **
      ** Note: Some operand do not have an attached memory in the context, like
      ** immediate hard coded operand (numbers). Those are stored on a temp
      ** storage attribute of the context.
      **
      ** In consequences the returned values is not preserved between
      ** 2 call of this function.
      */
      auto get_value(context &c, instruction_API::operand const &op)
	throw(std::runtime_error) -> MultiValEmulator<size_t> const &;

      /*!
      ** @brief Set the value of the operand store on the given context with
      ** the value.
      ** @param c The context to store the value in
      ** @param op The operand we want to set
      ** @param val The new value of the operand
      */
      auto set_value(context &c, instruction_API::operand const &op,
		     MultiValEmulator<size_t> const &val)
	-> void;

    } // !namespace emulation_API
  } // !namespace core
} // !namespace Barghest

#endif /* !BARGHEST_CONTEXT_HELPERS_HH_ */
