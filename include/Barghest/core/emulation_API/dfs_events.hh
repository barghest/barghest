#ifndef BARGHEST_DFS_EVENTS_HH_
# define BARGHEST_DFS_EVENTS_HH_

# include "Barghest/core/emulation_API/graph_visitor.hh"

namespace Barghest
{
  namespace core
  {
    namespace emulation_API
    {
      class	dfs_events
      {
      private:
	graph_visitor		&visitor_;
      public:

	dfs_events(graph_visitor &v) : visitor_(v)
	{
	}

	template <class TVertex, class TGraph>
	void	initialize_vertex(TVertex, TGraph&){}
	template <class TVertex, class TGraph>
	void	start_vertex(TVertex, TGraph&){}
	template <class TVertex, class TGraph>
	void	discover_vertex(TVertex v, TGraph &g)
	{
// cout << endl << "arriving at " << names[u] << endl
//      << "  neighboring cities are: ";
	  // std::cout << "Visiting node " << std::hex << std::showbase << g[v].start() << std::endl;
	  // for (outIt = boost::out_edge(v, g), isThereMultipleOutEdge = 0
	  // 	     ; outIt.first != outIt.second && isThereMultipleOutEdge <= 1
	  // 	     ; outIt.first++, isThereMultipleOutEdge++);
	  // if (isThereMultipleOutEdge > 1)
	  // dfs_.push_context();
	  // std::cout << "context pushed" << std::endl;
	  // for (auto pair_offset_inst : g[v].getInsns())
	  for (auto it = g[v].get_insns().begin() ; it != g[v].get_insns().end() ; it++)
	  {
	    // std::cout << "1First: " << (*it).first << std::endl;
	    // std::cout << "1Second: " << (*it).second.format() << std::endl;
	    visitor_.on_instruction(*it);
	    // dfs.onInstruction(std::make_pair(pair_offset_inst.first, pair_offset_inst.second));
	    // dfs.onInstruction(std::pair<size_t, InstructionAPI::Instruction const &>(pair_offset_inst.first, pair_offset_inst.second));
	    visitor_.get_context().emulate(*it);
	  }
	  // g[v].format();
	}

	template <class TVertex, class TGraph>
	void	examine_vertex(TVertex, TGraph&){}
	template <class TVertex, class TGraph>
	void	finish_vertex(TVertex, TGraph&){}

	/**/
	template <class TEdge, class TGraph>
	void	tree_edge(TEdge, TGraph&){}
	template <class TEdge, class TGraph>
	void	back_edge(TEdge, TGraph&){}
	template <class TEdge, class TGraph>
	void	forward_or_cross_edge(TEdge, TGraph&){}

	template <class TEdge, class TGraph>
	void	examine_edge(TEdge, TGraph&){}
	template <class TEdge, class TGraph>
	void	finish_edge(TEdge, TGraph&){}
      };

    } // !namespace emulation_API
  } // !namespace core
} // !namespace Barghest

#endif /* !BARGHEST_DFS_EVENTS_HH_ */
