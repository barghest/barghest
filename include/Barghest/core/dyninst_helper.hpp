// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#include <dyninst/Operand.h>
#include <dyninst/Register.h>

#include "Barghest/core/instruction_API/expression.hh"

namespace Barghest
{
  namespace core
  {

    // /*!
    // ** @return the std::type_info of the underlaying class of the operand expression.
    // */
    // auto get_operand_expr_typeid(Dyninst::InstructionAPI::Operand const &op)
    //   -> std::type_info const &;

    /*!
    ** @return true if the operand is a register, false otherwise.
    */
    auto is_dyn_expr_reg(Dyninst::InstructionAPI::Expression const &expr) -> bool;
    /*!
    ** @return true if the operand is a dereference, false otherwise.
    */
    auto is_dyn_expr_deref(Dyninst::InstructionAPI::Expression const &expr) -> bool;
    /*!
    ** @return true if the operand is an immediate, false otherwise.
    */
    auto is_dyn_expr_imm(Dyninst::InstructionAPI::Expression const &expr) -> bool;
    /*!
    ** @return true if the operand is a binary function, false otherwise.
    */
    auto is_dyn_expr_binary_function(Dyninst::InstructionAPI::Expression const &expr)
      -> bool;

    auto get_mach_register(Dyninst::InstructionAPI::RegisterAST::Ptr reg)
      -> Dyninst::MachRegister;
    auto get_mach_register_val(Dyninst::InstructionAPI::RegisterAST::Ptr reg)
      -> signed int;
    auto get_mach_register_val(Dyninst::MachRegister const &reg) -> signed int;
    auto get_base_register(Dyninst::InstructionAPI::RegisterAST::Ptr reg)
      -> Dyninst::MachRegister;

    auto init_operand_expr(Dyninst::InstructionAPI::Operand const &dyn_operand)
      -> instruction_API::expression *;
    auto init_dyn_expr(Dyninst::InstructionAPI::Expression::Ptr expr)
      -> instruction_API::expression *;

  } // !namespace core
} // !namespace Barghest
