///
/// \file BarghestContext.hh
/// 
/// \author Jean-Baptiste Laurent
/// \<jeanbaptiste.laurent.pro@gmail.com\>
/// 
/// \date Started on  Sat Jun 28 19:49:38 2014 Jean-Baptiste Laurent
/// \date Last update Sat Jan 24 04:44:11 2015 Jean-Baptiste Laurent
/// \brief File description to place here
///

#ifndef BARGHEST_CONTEXT_HH_
# define BARGHEST_CONTEXT_HH_

// # include <fwdios>	// forward definition
# include <list>	// std::list
// # include <map>		// std::map
# include <vector>	// std::vector

# include <boost/numeric/interval.hpp>	// boost interval<>

# ifdef WIN32
#  include <Dyninst/include/entryIDs.h> // Dyninst instruction ID
# else
#  include <dyninst/entryIDs.h> // Dyninst instruction ID
# endif // WIN32

# include "Barghest/core/parse_API/block.hpp"
# include "Barghest/core/instruction_API/instruction.hpp"
# include <bitField/bitfield_t.hpp>		// bitfield_t
# include <bitField/BitFieldArithmetic/BitFieldArithmetic.hh>	// BitFieldArithmetic<T>

using namespace bitField;
using namespace bitField::BitFieldArithmetic;

// # include "BarghestInstruction.hh"
// # include "BarghestBlock.hh"
// # include "BitFieldAlloc.hh"
// # include "BitFieldArithmetic.hh"
// // # include "BarghestValueEmulation.hh"

namespace Barghest
{
  class Context
  {
  public:
    /*!
    ** A type allowing to store the trace: The list of all the instruction emulated by now.
    */
    typedef std::list<std::pair<size_t, InstructionAPI::Instruction const &> >	t_trace;
    // typedef std::map<unsigned int, BitField::BitFieldAlloc>			t_reg;

    // typedef std::vector<std::pair<
    // 			  /* Variable interval */	  boost::numeric::interval_lib::interval<size_t>,
    // 			  /* Variable interval */ int value>
    // 			> t_memory;

    /*!
    ** @brief An array of MAX_X86_64_BASE_VAL element which will hold data for
    ** the BitFieldArithmetic<T>. MAX_X86_64_BASE_VAL is the number of register
    ** this context have to emulate.
    ** Ex: The x86_64::base and x86::base can be used as index (Ex: array[x86_64::base::rax])
    */
    typedef std::array<bitfield_t<size_t>, InstructionAPI::x86_64::base::MAX_X86_64_BASE_VAL>	t_reg;
  private:
    /*!
    ** TODO: class ContextData
    ** TODO: class Context contient les methodes pour instrumenter ContextData
    ** Operand Fourni les informations sur le graph (quelles registres, quel memoire, etc)
    ** ContextData Fourni les valeurs de l'emulation
    ** Context utilise les deux. Ex: context.set(Operand -clefs-, Context -valeur-)
    */

    /*!
    ** @brief A list of all the instruction successfully emulated by this context.
    ** The first element of the pair is the offset of the instruction, while the
    ** the second one is the instruction itself.
    ** Internal notes: It is design to contain a reference on the Instruction
    ** precached by the ParseAP::Block in order to avoid a strong memory usage
    ** when used in a multhreaded environment.
    ** TODO: faire des methodes qui recree l'historique de valeur et memoire en
    **       utilisant la trace.
    ** Note: use the "_getTrace(void) -> t_trace &" method to use it and edit it
    ** Note: use the "getTrace(void) const -> t_trace const &" as a const
    */
    t_trace		_trace;

    /*!
    ** Note: use the "_getRegisters(void) -> t_trace &" method to use it and edit it
    ** Note: use the "getRegisters(void) const -> t_trace const &" as a const
    */
    // ValueEmulation	_valueEmul;
    t_reg		_registers;

    void    (Context::*_fct[::_entry_ids_max_])(size_t offset,
						InstructionAPI::Instruction const &,
						std::vector<InstructionAPI::Operand> const &operands);
    /*!
    ** Same version as the regular getTrace(void) but return a non const reference.
    */
    /* TODO: trouver une solution pour faire un context modifiable */
  public:
    auto	_getTrace(void) -> t_trace &;
    auto	_copyTrace(t_trace const &other_trace) -> void;

    auto	_getRegisters(void) -> t_reg &;
  public:
    Context();
    ~Context();

    Context(Context const &other);
    auto	operator=(Context const &other) -> Context &;


    /*!
    ** Return a reference of the trace of this context.
    */
    auto	getTrace(void) const -> t_trace const &;

    /*!
    ** Return the map with the emulated values of the register.
    */
    auto	getRegisters(void) const -> t_reg const &;
    /* TODO */
    // auto	getRegister(mon register) const -> ?? const &;

    /*!
    ** Update the current context with the instruction of the block at the
    ** given offset.
    */
    auto	emulate(std::pair<size_t, InstructionAPI::Instruction const &> inst) -> void;

    /*!
    ** Pretty print the instruction already emulated by this context.
    */
    auto	printTrace(std::ostream& stream = std::cout) const -> void;

  private:
    auto	_unknown_inst(size_t offset,
			      InstructionAPI::Instruction const &inst,
			      std::vector<InstructionAPI::Operand> const &operands) -> void;

    /**/
    auto	_add_emul(size_t offset,
			  InstructionAPI::Instruction const &inst,
			  std::vector<InstructionAPI::Operand> const &operands) -> void;

    /* TODO: Remplacer les param de Dyninst par les wrapper de Barghest */
    auto	__add_emul_reg_imm(Dyninst::InstructionAPI::Operand const &dst,
				   Dyninst::InstructionAPI::Operand const &src) -> void;
    auto	__add_emul_reg_reg(Dyninst::InstructionAPI::Operand const &dst,
				   Dyninst::InstructionAPI::Operand const &src) -> void;
    auto	__add_emul_reg_deref(Dyninst::InstructionAPI::Operand const &dst,
				     Dyninst::InstructionAPI::Operand const &src) -> void;

    /**/

    auto	_sub_emul(size_t offset,
			  InstructionAPI::Instruction const &inst,
			  std::vector<InstructionAPI::Operand> const &operands) -> void;

    /* TODO: Remplacer les param de Dyninst par les wrapper de Barghest */
    auto	__sub_emul_reg_imm(Dyninst::InstructionAPI::Operand const &dst,
				   Dyninst::InstructionAPI::Operand const &src) -> void;
    auto	__sub_emul_reg_reg(Dyninst::InstructionAPI::Operand const &dst,
				   Dyninst::InstructionAPI::Operand const &src) -> void;
    auto	__sub_emul_reg_deref(Dyninst::InstructionAPI::Operand const &dst,
				     Dyninst::InstructionAPI::Operand const &src) -> void;

    /**/

    auto	_mov_emul(size_t offset,
			  InstructionAPI::Instruction const &inst,
			  std::vector<InstructionAPI::Operand> const &operands) -> void;

    /* TODO: Remplacer les param de Dyninst par les wrapper de Barghest */
    auto	__mov_emul_reg_imm(Dyninst::InstructionAPI::Operand const &dst,
				   Dyninst::InstructionAPI::Operand const &src) -> void;
    auto	__mov_emul_reg_reg(Dyninst::InstructionAPI::Operand const &dst,
				   Dyninst::InstructionAPI::Operand const &src) -> void;
    auto	__mov_emul_reg_deref(Dyninst::InstructionAPI::Operand const &dst,
				     Dyninst::InstructionAPI::Operand const &src) -> void;

    /**/

    auto	_shl_sal_emul(size_t offset,
			  InstructionAPI::Instruction const &inst,
			  std::vector<InstructionAPI::Operand> const &operands) -> void;

    /* TODO: Remplacer les param de Dyninst par les wrapper de Barghest */
    auto	__shl_sal_emul_reg_imm(Dyninst::InstructionAPI::Operand const &dst,
				   Dyninst::InstructionAPI::Operand const &src) -> void;
    auto	__shl_sal_emul_reg_reg(Dyninst::InstructionAPI::Operand const &dst,
				   Dyninst::InstructionAPI::Operand const &src) -> void;
    auto	__shl_sal_emul_reg_deref(Dyninst::InstructionAPI::Operand const &dst,
				     Dyninst::InstructionAPI::Operand const &src) -> void;
  };
} // !namespace Barghest

#endif /* !BARGHEST_CONTEXT_HH_ */
