//=======================================================================
// Copyright 1997, 1998, 1999, 2000 University of Notre Dame.
// Copyright 2003 Bruce Barr
// Authors: Andrew Lumsdaine, Lie-Quan Lee, Jeremy G. Siek
//
// Distributed under the Boost Software License, Version 1.0. (See
// accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//=======================================================================

// Nonrecursive implementation of depth_first_visit_impl submitted by
// Bruce Barr, schmoost <at> yahoo.com, May/June 2003.
#ifndef BARGHEST_GRAPH_RECURSIVE_DFS_HPP
#define BARGHEST_GRAPH_RECURSIVE_DFS_HPP

#include <boost/graph/depth_first_search.hpp>

namespace boost
{

  namespace detail {

    /**
     * @author pradea_c
     * @param IncidenceGraph {Complet graph to analyse}
     * @param ColorMap {}
     * @param TerminatorFunc {Functior for test the en of the search}
     * @param ContinueFunc {Functor for coninue the search more than one time in a vertex}
     * @todo Keep the number of pass in each node
     * @brief depth first search modified
     */
    template <class IncidenceGraph, class DFSVisitor, class ColorMap,
              class TerminatorFunc, class ContinueFunc>
    void depth_first_visit_impl(const IncidenceGraph& g,
                                typename graph_traits<IncidenceGraph>::vertex_descriptor u,
                                DFSVisitor& vis,  // pass-by-reference here, important!
                                ColorMap color, TerminatorFunc func,
                                ContinueFunc func_boucle, ContinueFunc func_cross)
    {
      BOOST_CONCEPT_ASSERT(( IncidenceGraphConcept<IncidenceGraph> ));
      BOOST_CONCEPT_ASSERT(( DFSVisitorConcept<DFSVisitor, IncidenceGraph> ));
      typedef typename graph_traits<IncidenceGraph>::vertex_descriptor Vertex;
      BOOST_CONCEPT_ASSERT(( ReadWritePropertyMapConcept<ColorMap, Vertex> ));
      typedef typename property_traits<ColorMap>::value_type ColorValue;
      BOOST_CONCEPT_ASSERT(( ColorValueConcept<ColorValue> ));
      typedef color_traits<ColorValue> Color;
      typename graph_traits<IncidenceGraph>::out_edge_iterator ei, ei_end;

      put(color, u, Color::gray());
      vis.discover_vertex(u, g);

      // call of the terminator function <= Warning, it will be call a lot of time before the the DFS exit really
      if (!func(u, g))
        for (boost::tie(ei, ei_end) = out_edges(u, g); ei != ei_end; ++ei)
        {
          Vertex v = target(*ei, g);
          vis.examine_edge(*ei, g);
          ColorValue v_color = get(color, v);
          if (v_color == Color::white())
          {
            vis.tree_edge(*ei, g);
            depth_first_visit_impl(g, v, vis, color, func, func_boucle, func_cross);
          }
          else if (v_color == Color::gray() && func_boucle(v, g))
          {
            vis.back_edge(*ei, g); // boucle
            depth_first_visit_impl(g, v, vis, color, func, func_boucle, func_cross);
            // keep the number call
          }
          else if (v_color == Color::gray())
          {
            vis.back_edge(*ei, g);
          }
          else if (func_cross(v, g))
          {
            vis.forward_or_cross_edge(*ei, g); // multi path
            depth_first_visit_impl(g, v, vis, color, func, func_boucle, func_cross);
            // keep the number call
          }
          else
          {
            vis.forward_or_cross_edge(*ei, g); // multi path
          }
        }
      put(color, u, Color::black());
      vis.finish_vertex(u, g);
    }
  } // namespace detail

    /**
     * @author pradea_c
     * @param g {Complet graph to analyse}
     * @param vis {graph visitor}
     * @param color {ColorMap}
     * @param func {Functior for test the en of the search}
     * @param func_boucle {Functor for coninue the search more than one time in a vertex (on color black)}
     * @param func_boucle {Functor for coninue the search more than one time in a vertex (on color gray)}
     * @brief Init function of DFS and call of the DFS
     */
  template <class VertexListGraph, class DFSVisitor, class ColorMap,
            class TerminatorFunc, class ContinueFunc>
  void
  depth_first_search(const VertexListGraph& g, DFSVisitor vis, ColorMap color,
                     typename graph_traits<VertexListGraph>::vertex_descriptor start_vertex,
                     TerminatorFunc func,
                     ContinueFunc func_boucle, ContinueFunc func_cross)
  {
    typedef typename graph_traits<VertexListGraph>::vertex_descriptor Vertex;
    BOOST_CONCEPT_ASSERT(( DFSVisitorConcept<DFSVisitor, VertexListGraph> ));
    typedef typename property_traits<ColorMap>::value_type ColorValue;
    typedef color_traits<ColorValue> Color;

    // Init every vertex at "white"
    typename graph_traits<VertexListGraph>::vertex_iterator ui, ui_end;
    for (boost::tie(ui, ui_end) = vertices(g); ui != ui_end; ++ui)
    {
      Vertex u = implicit_cast<Vertex>(*ui);
      put(color, u, Color::white());
      vis.initialize_vertex(u, g);
    }

    // Begin ths DFS at the specified vertex
    vis.start_vertex(start_vertex, g);
    detail::depth_first_visit_impl(g, start_vertex, vis, color, func, func_boucle, func_cross);
  }

} // namespace boost

#endif
