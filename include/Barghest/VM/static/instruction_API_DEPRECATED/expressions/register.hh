#pragma once

#ifndef BARGHEST_REGISTER_HH__
# define BARGHEST_REGISTER_HH__

# include <string>

# include <dyninst/Expression.h>
# include <dyninst/Register.h>

# include "Barghest/core/instruction_API/expression.hh"
# include "Barghest/core/instruction_API/register_ID.hpp"

namespace Barghest
{
  namespace core
  {
    namespace instruction_API
    {
      namespace expressions
      {

	/*
	** TODO: revoir l'utilisation des shared ptr de boost !
	*/
	class register_t : public expression
	{
	public:
	  register_t(Dyninst::InstructionAPI::Expression::Ptr reg);
	  register_t(register_t &&other) = default;
	  virtual ~register_t() = default;

	  /*!
	  ** @See "BarghestExpression.hh" for documentation about those function.
	  */
	  virtual auto is_reg(void) const -> bool;
	  /*!
	  ** Return the ID of the register represented. Each register has his own
	  ** ID.
	  ** This id can be used to perform "by hand" comparaison with other
	  ** register.
	  */
	  auto get_raw_expr(void) const
	    -> Dyninst::InstructionAPI::Expression::Ptr;
	  auto get_raw(void) const -> Dyninst::InstructionAPI::RegisterAST::Ptr;

	  /*!
	  ** Change the internal Dyninst class hosted by Barghest::Register.
	  ** Note: The Register_ID is updated according the RegisterAST given.
	  ** Note: Underlaying values are all reset.
	  ** @param dynReg The new RegisterAST to set and host.
	  ** @param A reference on "this"
	  ** @exception std::runtime_error There is no corresponding Register_ID
	  **                               (unknown register).
	  */
	  auto set_raw(Dyninst::InstructionAPI::RegisterAST::Ptr dyn_reg)
	    -> register_t const &;

	  /*!
	  ** Directly set the Register_ID.
	  ** Warning: The Dyninst RegisterAST won't correspond anymore with the ID
	  ** @param A reference on "this"
	  */
	  auto set_ID(register_ID ID) -> register_t const &;

	  /*!
	  ** Return the Register_ID corresponding to the hosted Register.
	  */
	  auto get_ID(void) const -> register_ID const &;
	  /*!
	  ** Return only the name in lower case in of the Register wrapped.
	  ** Ex: (al, eax, cs, rsp, etc)
	  */
	  auto format(void) const -> std::string;

	private:
	  /*!
	  ** The MachRegister Dyninst raw representation of a Register. (
	  ** using _reg.get_ID())
	  ** We do not use a Dyninst::InstructionAPI::RegisterAST because it does
	  ** not provide enought information and method (like promote) allocate
	  ** new instance of a class when we only need to perform const and fast
	  ** comparaison. For this purpose we directly use the value and bitfield
	  ** to find out what we are willing to known. (Cf: dyn_regs.h header for
	  ** Dyninst::MachRegister class definition)
	  **
	  ** This class can be obtained using the Operand class methods and is
	  ** not intended to be constructed by hand by the user.
	  */
	  Dyninst::InstructionAPI::RegisterAST::Ptr dyn_reg_;

	  /*!
	  ** The ID of the register. It contains the aliasing/base/category and
	  ** size.
	  ** Ex: x86_eax is a FULL GPR (General Purpose Register) of base A.
	  ** Ex: x86_64_edi is a DW GPR (General Purpose Register) of base DI.
	  ** Ex: x86_64_CS is a FULL Segment of base CS.
	  ** @see Register_ID documentation
	  */
	  register_ID ID_;

	  /*!
	  ** Return the raw value of the internal class Dyninst::MachRegister.
	  */
	  auto get_raw_val(void) const -> signed int;
	  /*!
	  ** Return the base register ID of a Dyninst::MachRegister
	  ** (Ex: BASEA, BASEDI, etc)
	  ** Cf: /usr/include/dyn_regs.h
	  */
	  auto get_base_mach_register(void) const -> signed int;
	  /*!
	  ** Return the aliasing and subrange ID-used on x86/x86_64 to distinguish
	  ** between things like EAX and AH.
	  */
	  auto get_mach_register_aliasing(void) const -> signed int;
	  /*!
	  ** Return the register categorie (GPR/FPR/MMX/...)
	  */
	  auto get_mach_register_category(void) const -> signed int;
	}; // !class register

	/*!
	** Return the Dyninst::MachRegister associated with a
	** Dyninst::InstructionAPI::RegisterAST
	*/
	auto get_mach_register(Dyninst::InstructionAPI::RegisterAST::Ptr reg)
	  -> Dyninst::MachRegister;
	/*!
	** Return the Dyninst::MachRegister associated with a
	** Dyninst::InstructionAPI::RegisterAST
	*/
	auto get_mach_register_val(Dyninst::InstructionAPI::RegisterAST::Ptr reg)
	  -> signed int;
	auto get_mach_register_val(Dyninst::MachRegister const &reg)
	  -> signed int;
	/*!
	** Return the base register ID from a RegisterAST.
	*/
	auto get_base_register(Dyninst::InstructionAPI::RegisterAST::Ptr reg)
	  -> Dyninst::MachRegister;

	auto operator<(register_t const &a, register_t const &b) -> bool;
	auto operator>(register_t const &a, register_t const &b) -> bool;

      } // !namespace expressions
    } // !namespace instruction_API
  } // !namespace core
} // !namespace Barghest

#endif /* !BARGHEST_REGISTER_HH__ */
