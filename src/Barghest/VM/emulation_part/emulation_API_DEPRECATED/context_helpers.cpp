// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#include <typeinfo>    // std::bad_cast
#include <stdexcept>    // std::runtime_error
#include "Barghest/core/emulation_API/context_helpers.hh"

#include "Barghest/core/instruction_API/expressions/register.hh"
#include "Barghest/core/instruction_API/expressions/immediate.hh"
#include "Barghest/core/instruction_API/expressions/dereference.hh"
#include "Barghest/core/instruction_API/expressions/binary_function.hh"

namespace Barghest
{
  namespace core
  {
    namespace emulation_API
    {
      namespace ix86a = instruction_API::x86::aliasing;
      namespace ix86_64a = instruction_API::x86_64::aliasing;

      /*!
      ** Clear the aliasing area.
      ** Ex: clear_aliasing_area(eax_mem, 1, H)
      ** for AH from EAX, (EAX & 0xFF FF 00 FF) is done.
      */
      static auto clear_aliasing_area(MultiValEmulator<size_t> &mem,
                                      int size, int aliasing) -> void
      {
        switch (size)
        {
          case 8:
          {
            // TODO: make code capable of working in 32b and in 64b (for 32b and 64b binaries)
            if (aliasing == ix86a::L || aliasing == ix86_64a::L)
              // TODO: we lost precision with the boost interval, use operator[] or
              // a special aliasing function for sub registers
              mem &= ~0xFF; // Clear everything to 0xFF except the last byte
            else if (aliasing == ix86a::H || aliasing == ix86_64a::H)
              mem &= ~0xFF00;
            else
            {
              throw std::runtime_error(
                "Unknown 8bits aliasing : " + std::to_string(aliasing) + ". "
                + "Available: " + std::to_string(ix86a::L) + ", "
                + std::to_string(ix86_64a::L) + ", "
                + std::to_string(ix86a::H) + ", "
                + std::to_string(ix86_64a::H) + ".");
            }
          } break;

          case 16:
          {
            if (aliasing == ix86a::W || aliasing == ix86_64a::W)
              mem &= ~0xFFFF;
            else
              throw std::runtime_error("Unknown 16bits aliasing.");
          } break;

          case 32:
          {
            if (aliasing == ix86a::FULL)
              mem = 0;
            else if (aliasing == ix86_64a::DW)
              // Intel man Vol 1: 3.4.1.1
              // 32-bit operands generate a 32-bit result, zero-extended to
              // a 64-bit result in the destination general-purpose register.
              mem &= ~0xFFFFFFFF;
            else
              throw std::runtime_error("Unknown 32bits aliasing.");
          } break;

          case 64:
          {
            if (aliasing == ix86_64a::FULL)
              mem = 0;
            else
              throw std::runtime_error("Unknown 64bits aliasing.");
          } break;

          default: throw std::runtime_error(std::string("Unknown register size: ")
                                            + std::to_string(size));
        };
      }

      /*!
      ** Clear everything outside the aliasing area.
      ** Ex: get_aliasing_area(eax_mem, 1, H)
      ** for AH from EAX, (EAX & 0x00 00 FF 00) is done.
      */
      static auto get_aliasing_area(MultiValEmulator<size_t> &mem,
                                    int size, int aliasing) -> void
      {
        switch (size)
        {
          case 8:
          {
            // TODO: make code capable of working in 32b and in 64b (for 32b and 64b binaries)
            if (aliasing == ix86a::L || aliasing == ix86_64a::L)
              // TODO: we lost precision with the boost interval, use operator[] or
              // a special aliasing function for sub registers
              mem &= 0xFF; // Clear everything to 0xFF except the last byte
            else if (aliasing == ix86a::H || aliasing == ix86_64a::H)
            {
              mem >>= 8*1;
              mem &= 0xFF;
            }
            else
            {
              throw std::runtime_error(
                "Unknown 8bits aliasing : "
                + std::to_string(aliasing) + ". "
                + "Available: "
                + std::to_string(ix86a::L) + ", "
                + std::to_string(ix86_64a::L) + ", "
                + std::to_string(ix86a::H) + ", "
                + std::to_string(ix86_64a::H) + "."
                );
            }
          } break;

          case 16:
          {
            if (aliasing == ix86a::W || aliasing == ix86_64a::W)
              mem &= ~((uint16_t)0); // 0xFFFF
            else
              throw std::runtime_error("Unknown 16bits aliasing.");
          } break;

          case 32:
          {
            if (aliasing == ix86a::FULL); // Nothing to do
            else if (aliasing == ix86_64a::DW)
            {
              // Intel man Vol 1: 3.4.1.1
              // 32-bit operands generate a 32-bit result, zero-extended to
              // a 64-bit result in the destination general-purpose register.
              std::cout << "Clearing high part of the register" << std::endl;
              mem &= 0xFFFFFFFF;
            }
            else
              throw std::runtime_error("Unknown 32bits aliasing.");
          } break;

          case 64:
          {
            if (aliasing == ix86_64a::FULL); // Nothing to do
            else
              throw std::runtime_error("Unknown 64bits aliasing.");
          } break;

          default: throw std::runtime_error(std::string("Unknown register size: ")
                                            + std::to_string(size));
        };
      }

      static auto get_value_reg_(context &c,
                                 instruction_API::expressions::register_t const &reg,
                                 std::vector<MultiValEmulator<size_t> const *> &values)
        -> void
      {
        MultiValEmulator<size_t> &mem = c.get_value_last_return();

        mem = c.get_registers()[reg.get_ID().base];
        // std::cout << "Size " << +reg.get_ID().size << std::endl;
        // std::cout << "Aliasing " << +reg.get_ID().aliasing << std::endl;
        get_aliasing_area(mem, reg.get_ID().size, reg.get_ID().aliasing);
        values.push_back(&mem);
        // return (mem);
      }
      static auto get_value_imm_(context &c,
                                 instruction_API::expressions::immediate const &imm,
                                 std::vector<MultiValEmulator<size_t> const *> &values)
        -> void
      {
        MultiValEmulator<size_t> &mem = c.get_value_last_return();

        mem = imm.get_value<size_t>();
        values.push_back(&mem);
        // return (mem);
      }

      static auto get_value_binary_func_(context &c,
                                         instruction_API::expressions::binary_function const &bf,
                                         std::vector<MultiValEmulator<size_t> const *> &values)
        -> void
      {
        MultiValEmulator<size_t> &mem = c.get_value_last_return();

        values.push_back(&mem);
        // return (mem);
      }

      static auto get_values_deref_(context &c,
                                    instruction_API::expressions::dereference const &deref,
                                    std::vector<MultiValEmulator<size_t> const *> &values)
        -> void
      {
        get_value_binary_func_(c, static_cast<instruction_API::expressions::binary_function const &>(deref.get_binary_func_expr()), values);
      }

      auto get_values(context &c, instruction_API::operand const &op,
                      std::vector<MultiValEmulator<size_t> const *> &values)
        throw(std::runtime_error) -> void
      {
        auto const    &expr = op.get_expr();

        // std::cout << "expr enum: " << expr.get_sub_class_type() << std::endl;
        switch (expr.get_sub_class_type())
        {
          namespace exprs = instruction_API::expressions;

          case instruction_API::expression::REGISTER:
            get_value_reg_(c, dynamic_cast<exprs::register_t const &>(expr), values);
            break;
          case instruction_API::expression::IMMEDIATE:
            get_value_imm_(c, dynamic_cast<exprs::immediate const &>(expr), values);
            break;
          case instruction_API::expression::DEREFERENCE:
            get_values_deref_(c, dynamic_cast<exprs::dereference const &>(expr), values);
            break;
          case instruction_API::expression::BINARY_FUNCTION:
            get_value_binary_func_(c, dynamic_cast<exprs::binary_function const &>(expr),
                                   values);

          default:     throw std::runtime_error(std::string(__func__)
                                                + "Unable to downcast the expression.");
        };
      }

      /*
      ** Note: To find the sub class we use an ID and a switch to optimise.
      ** This code used at least once for each instruction emulation and is critical.
      ** Using a try/catch block and a dynamic_cast() would have cost too much.
      */
      auto get_value(context &c, instruction_API::operand const &op)
        throw(std::runtime_error) -> MultiValEmulator<size_t> const &
      {
        MultiValEmulator<size_t>            tmp;
        MultiValEmulator<size_t>            mem;
        std::vector<MultiValEmulator<size_t> const *>    values;

        mem = c.get_value_last_return();
        get_values(c, op, values);
        // std::cout << values.size() << std::endl;
        if (values.size() > 1)
        {
          tmp = *values[0];
          for (auto val : values)
            tmp.merge(*val);
          mem = tmp;
          return (mem);
        }
        else
          return (*values[0]);
      }

      /***********************************************************/

      static auto set_value_reg_(context &c,
                                 instruction_API::expressions::register_t const &reg,
                                 MultiValEmulator<size_t> const &val) -> void
      {
        auto    size = reg.get_ID().size;
        auto    aliasing = reg.get_ID().aliasing;

        // std::cout << &reg << std::endl;
        // std::cout << reg.get_ID() << std::endl;
        // std::cout << instruction_API::x86_64::base::MAX_X86_64_BASE_VAL << std::endl;

        MultiValEmulator<size_t>    &reg_mem
          = c.get_registers_mutable().at(reg.get_ID().base);
        MultiValEmulator<size_t>    val_cpy = val;

        get_aliasing_area(val_cpy, size, aliasing);
        clear_aliasing_area(reg_mem, size, aliasing);
        reg_mem |= val_cpy;
      }

      static auto set_value_imm_(context &c,
                                 instruction_API::expressions::immediate const &imm,
                                 MultiValEmulator<size_t> const &val) -> void
      {
        throw std::runtime_error("Trying to set an immediate value, which is not possible.");
      }
      static auto set_value_deref_(context &c,
                                   instruction_API::expressions::dereference const &deref,
                                   MultiValEmulator<size_t> const &val) -> void
      {
        std::cout << "We set a deref" << std::endl;
      }
      static auto set_value_binary_func_(context &c,
                                         instruction_API::expressions::binary_function const &bf,
                                         MultiValEmulator<size_t> const &val) -> void
      {
        std::cout << "We set a BF" << std::endl;
      }

      auto set_value(context &c, instruction_API::operand const &op,
                     MultiValEmulator<size_t> const &val)
        -> void
      {
        auto const    &expr = op.get_expr();

        switch (expr.get_sub_class_type())
        {
          namespace exprs = instruction_API::expressions;

          /* TODO: changer les dynamic par des statics une fois que tout est OK */
          case instruction_API::expression::REGISTER:
            set_value_reg_(c, dynamic_cast<exprs::register_t const &>(expr), val);
            break;
          case instruction_API::expression::IMMEDIATE:
            set_value_imm_(c, dynamic_cast<exprs::immediate const &>(expr), val);
            break;
          case instruction_API::expression::DEREFERENCE:
            set_value_deref_(c, dynamic_cast<exprs::dereference const &>(expr), val);
            break;
          case instruction_API::expression::BINARY_FUNCTION:
            set_value_binary_func_(c, dynamic_cast<exprs::binary_function const &>(expr), val);
            break;
          default: throw std::runtime_error(std::string(__func__)
                                            + "Unable to downcast the expression.");
        };
      }


    } // !namespace emulation_API
  } // !namespace core
} // !namespace Barghest
