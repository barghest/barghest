// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#ifndef BARGHEST_REGISTER_ID_HPP
# define BARGHEST_REGISTER_ID_HPP

# include <ostream>
# include <string>
# include <map>

namespace Barghest
{
  namespace core
  {
    namespace instruction_API
    {
      extern "C"
      {
        /*!
        ** This structure order and represent an ID for every BarghestRegister
        ** element that can be found on the binary graph. It is aimed for speed
        ** and allows the user to get the information directly and in details.
        ** It contains the following field:
        ** -----------------------------------------------------------------------
        ** /\ size /\: The size in bits of the register (ex: the size for eax is 32)
        ** -----------------------------------------------------------------------
        ** /\ aliasing /\: The aliasing of the register in order to distinguish ah
        ** from al, ax, eax and rax. There are the following categorie:
        ** * bit (1b): A Register, or flags of 1bit. (CF / ZF / etc)
        ** * low (8b): The low part of the base register. (al / bl / dil / etc)
        ** * high (8b): The high part of the base register when available.
        **              (ah / bh / ch / etc)
        ** * word (16b): The base register. (ax / bx / di).
        ** * dword (32b): A double binary word. The "Extended" base
        **                register (eax / ebx / edi / etc)
        ** * full (32b or 64b): The maximum size allowed for the register
        **                      by the current architecture
        ** Exemple: in 64b:
        ** * rax is a FULL register
        ** * eax is a DW (Double Word) register
        ** * ax  is a W (Word) register
        ** * al  is a L (Low) register
        ** * ah  is a H (High) register
        ** -----    in 32b:
        ** * eax is a FULL register
        ** * ax  is a W (Word) register
        ** * ...
        ** -----------------------------------------------------------------------
        ** /\ base /\: The ID of the base register.
        **             (Ex: al/ah/ax/eax/rax have the same base A)
        ** -----------------------------------------------------------------------
        ** /\ category /\: The category of the register.
        ** Category include:
        ** * GPR (General Purpose register (eax / r8 / etc))
        ** * FPU ((Floating point register) (st0 / st1 / etc)) (see note)
        ** * MMX (SIMD instruction set) (mm0 / mm1 / etc) (see note)
        ** --> Note: For simplicity, FPU and XMM have the same value as they
        **           share the same register space.
        ** * XMM (SSE* registers) (xmm0 / xmm1 / etc)
        ** TODO: There is more register category to add (is a flags (CF) a
        ** register category ?)
        ** -----------------------------------------------------------------------
        ** -----------------------------------------------------------------------
        ** /\ id /\: A variable containing all the above to compare all at once.
        ** -----------------------------------------------------------------------
        ** /\ name /\: The name of the element for pretty print purpose.
        ** -----------------------------------------------------------------------
        */
        struct register_ID
        {
          union
          {
            struct
            {
              int8_t size;
              int8_t aliasing;
              int8_t base;
              int8_t category;
            }; // !anonymous struct
            uint32_t ID;
          }; // !anonymous union
          std::string name;
          std::string full_name;

          auto operator=(register_ID const &other) -> register_ID &;
          auto operator=(uint32_t ID) -> register_ID &;
        }; // !struct register_ID
      } // !extern "C"

      /*!
      ** @brief Print the name of the given element. (ex: "ax", "eax", "r10", "dil", etc)
      */
      auto operator<<(std::ostream &a, const register_ID &other) -> std::ostream &;
      auto operator==(const register_ID &a, const register_ID &b) -> bool;

      namespace x86 // 32b
      {
        namespace base
        { // Starting at 0
          static const int A = 0, B = 1, C = 2, D = 3;
          static const int DI = 4, SI = 5;
          static const int SP = 6, BP = 7;
          static const int IP = 8;
          static const int FLAGS = 9; // eflags / rflags

          static const int CF = 10; // Carry Flag
          static const int PF = 11; // Parity Flag
          static const int AF = 12; // Auxiliary Carry Flag
          static const int ZF = 13; // Zero Flag
          static const int SF = 14; // Sign Flag
          static const int TF = 15; // Trap Flag
          static const int IF = 16; // Interrupt Enable Flag
          static const int DF = 17; // Direction Flag
          static const int OF = 18; // Overflow Flag
          static const int IOPL = 19; // I/O Privilege Level
          static const int NT = 20; // Nested Task
          static const int RF = 21; // Resume Flag
          static const int VM = 22; // Virtual-8086 Mode
          static const int AC = 23; // Alignment Check
          static const int VIF = 24; // Virtual Interrupt Flag
          static const int VIP = 25; // Virtual Interrupt Pending
          static const int ID = 26; // ID Flag

          /* 6 Free number left to add flags later if necessary */

          static const int CS = 30;
          static const int DS = 31;
          static const int SS = 32;
          static const int ES = 33;
          static const int FS = 34;
          static const int GS = 35;

          /* The maximum value +1. This is used to correctly begin the x86_64 suite */
          static const int MAX_X86_BASE_VAL = 36;
          /* /!\ Do not use number above 100 (x86_64 start there) /!\ */
        } // !namespace base

        namespace category
        { // Starting at 0
          static const int GPR = 0;
          static const int XMM = 1;
          // Same value because MMX and FPU share the same space.
          static const int MMX = 2, FPU = 2;
          static const int FLAGS = 3; // eflags / rflags
          static const int FLAG = 4; // Flag in eflags/rflags (CF / ZF / OF / etc)
          static const int SEG = 5; // Segment register (CS / DS / etc)
        } // !namespace category

        namespace aliasing
        { // Starting at 0 (note: x86::aliasing does not share values with x86_64::aliasing)
          static const int FULL = 0;
          static const int L = 1;
          static const int H = 2;
          static const int W = 3;
        } // !namespace aliasing

        extern const register_ID al, ah, ax, eax; // Accumulator for operands and results data
        extern const register_ID bl, bh, bx, ebx; // Pointer to data in the DS segment
        extern const register_ID cl, ch, cx, ecx; // Counter for string and loop operations
        extern const register_ID dl, dh, dx, edx; // I/O pointer

        extern const register_ID /* none, none, */ di, edi; // No high 8b register
        extern const register_ID /* none, none, */ si, esi; // -------------------

        extern const register_ID /* none, none, */ bp, ebp; // -------------------
        extern const register_ID /* none, none, */ sp, esp; // -------------------

        extern const register_ID eflags;   // flags register

        extern const register_ID cs, ds, ss, es, fs, gs; // Segment registers
      } // !namespace x86

      namespace x87 // Floating point
      {
        // TODO: ajouter les registres cr[0-+]
      } // !namespace x87

# if defined(__x86_64__) || defined(__ia64__) || defined(_WIN64)
      namespace x86_64 // 64b
      {
        // Import registers from x86 (GPR / Segment / etc)
        using namespace x86;

        namespace base // share x86 base declaration
        { // Start x86_64 base at x86::MAX_X86_BASE_VAL (x86::base max value + 1)
          using namespace x86::base;
          static const int R8 = MAX_X86_BASE_VAL;
          static const int R9 = MAX_X86_BASE_VAL + 1;
          static const int R10 = MAX_X86_BASE_VAL + 2;
          static const int R11 = MAX_X86_BASE_VAL + 3;
          static const int R12 = MAX_X86_BASE_VAL + 4;
          static const int R13 = MAX_X86_BASE_VAL + 5;
          static const int R14 = MAX_X86_BASE_VAL + 6;
          static const int R15 = MAX_X86_BASE_VAL + 7;

          static const int MAX_X86_64_BASE_VAL = MAX_X86_BASE_VAL + 8;
        } // !namespace base

        namespace category // share x86 category declaration
        { // Start x86_64 category at 100
          using namespace x86::category;
        } // !namespace category

        namespace aliasing
        { // Start x86_64 aliasing at 100
          /*
          ** Because of the aliasing::FULL, we do not want the x86_64 to
          ** share the same value than the x86 version.
          */
          // using namespace x86::aliasing;
          static const int FULL = 100;
          static const int L = 101;
          static const int H = 102;
          static const int W = 103;
          static const int DW = 103;
        } // !namespace aliasing

        extern const register_ID rax;
        extern const register_ID rbx;
        extern const register_ID rcx;
        extern const register_ID rdx;

        extern const register_ID dil, rdi; // 8b reg in x86_64 available
        extern const register_ID sil, rsi; // 8b reg in x86_64 available

        extern const register_ID bpl, rbp; // 8b reg in x86_64 available
        extern const register_ID spb, rsp; // 8b reg in x86_64 available

        extern const register_ID rflags;

        extern const register_ID r8b, /* none, */ r8w, r8d, r8; // -------------------
        extern const register_ID r9b, /* none, */ r9w, r9d, r9; // -------------------
        extern const register_ID r10b, /* none, */ r10w, r10d, r10; // -------------------
        extern const register_ID r11b, /* none, */ r11w, r11d, r11; // -------------------
        extern const register_ID r12b, /* none, */ r12w, r12d, r12; // -------------------
        extern const register_ID r13b, /* none, */ r13w, r13d, r13; // -------------------
        extern const register_ID r14b, /* none, */ r14w, r14d, r14; // -------------------
        extern const register_ID r15b, /* none, */ r15w, r15d, r15; // -------------------
      } // !namespace x86_64

# endif /* 64b */

      /*!
      ** A Map to link the Dyninst lowest register id (an int) used with binary
      ** arithmetic with the Register of Barghest.
      */
      extern std::map<signed int /* DyninstId */, register_ID const & /* BarghestId */> const g_regLinkTable;

    } // !namespace instruction_API
  } // !namespace core
} // !namespace Barghest

#endif /* !BARGHEST_REGISTER_ID_HPP */
