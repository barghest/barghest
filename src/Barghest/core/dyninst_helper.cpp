// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#include <dyninst/Dereference.h>
#include <dyninst/Immediate.h>
#include <dyninst/BinaryFunction.h>

#include "Barghest/core/dyninst_helper.hpp"

#include "Barghest/core/instruction_API/expressions/register.hh"
#include "Barghest/core/instruction_API/expressions/immediate.hh"
#include "Barghest/core/instruction_API/expressions/dereference.hh"
#include "Barghest/core/instruction_API/expressions/binary_function.hh"

namespace Barghest
{
  namespace core
  {
    // auto get_dyn_expr_typeid(Dyninst::InstructionAPI::Expression const &op)
    //   -> std::type_info const &
    // {
    //   return typeid(expr);
    // }

    auto is_dyn_expr_reg(Dyninst::InstructionAPI::Expression const &expr) -> bool
    {
      return (typeid(expr) == typeid(Dyninst::InstructionAPI::RegisterAST));
    }

    auto is_dyn_expr_deref(Dyninst::InstructionAPI::Expression const &expr) -> bool
    {
      return (typeid(expr) == typeid(Dyninst::InstructionAPI::Dereference));
    }

    auto is_dyn_expr_imm(Dyninst::InstructionAPI::Expression const &expr) -> bool
    {
      return (typeid(expr) == typeid(Dyninst::InstructionAPI::Immediate));
    }

    auto is_dyn_expr_binary_function(Dyninst::InstructionAPI::Expression const &expr)
      -> bool
    {
      return (typeid(expr) == typeid(Dyninst::InstructionAPI::BinaryFunction));
    }

    auto get_mach_register(Dyninst::InstructionAPI::RegisterAST::Ptr reg)
      -> Dyninst::MachRegister
    {
      return (reg->getID());
    }

    auto get_mach_register_val(Dyninst::InstructionAPI::RegisterAST::Ptr reg)
      -> signed int
    {
      return get_mach_register(reg).val();
    }

    auto get_mach_register_val(Dyninst::MachRegister const &reg) -> signed int
    {
      return reg.val();
    }

    auto get_base_register(Dyninst::InstructionAPI::RegisterAST::Ptr reg)
      -> Dyninst::MachRegister
    {
      return get_mach_register(reg).getBaseRegister();
    }

    auto init_operand_expr(Dyninst::InstructionAPI::Operand const &dyn_operand)
      -> instruction_API::expression *
    {
      return (init_dyn_expr(dyn_operand.getValue()));
    }

    /* *************************************************************** */

    auto init_dyn_expr(Dyninst::InstructionAPI::Expression::Ptr expr)
      -> instruction_API::expression *
    {
      if (is_dyn_expr_reg(*expr))
        return (new instruction_API::expressions::register_t(expr));
      else if (is_dyn_expr_deref(*expr))
        return (new instruction_API::expressions::dereference(expr));
      else if (is_dyn_expr_imm(*expr))
        return (new instruction_API::expressions::immediate(expr));
      else if (is_dyn_expr_binary_function(*expr))
        return (new instruction_API::expressions::binary_function(expr));
      else
      {
        throw std::runtime_error("Unknown dyninst operand type.");
        assert(0);
        return (nullptr); // should never get there (to avoid warning)
      }
    }

  } // !namespace core
} // !namespace Barghest
