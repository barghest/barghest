// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#pragma once

#include <string>

#include <boost/graph/adjacency_list.hpp>

#include <dyninst/CodeObject.h>
#include <dyninst/CodeSource.h>

#include "Barghest/core/parse_API/graph.hpp"

namespace Barghest
{
  namespace core
  {
    class graph
    {
    public:
      graph();
      graph(graph const &) = delete;
      graph(graph &&) = default;
      auto operator=(graph const &) -> graph & = delete;
      auto operator=(graph &&) -> graph & = default;
      ~graph();

      auto get_boost_graph() const -> parse_API::graph const &;
      auto get_boost_graph() -> parse_API::graph &;
      auto set_stcs(Dyninst::ParseAPI::SymtabCodeSource *stcs) -> void;
      auto set_co(Dyninst::ParseAPI::CodeObject *co) -> void;
      auto set_path(std::string const &path) -> void;
      auto get_path() const -> std::string;
    private:
      parse_API::graph boost_graph_;
      Dyninst::ParseAPI::SymtabCodeSource *stcs_;
      Dyninst::ParseAPI::CodeObject *co_;
      std::string path_;
    };
  }
}
