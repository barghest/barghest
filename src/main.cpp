// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#if 0

#include "Barghest/core/core.hpp"
#include "Barghest/front_end/front_end.hpp"

auto main() -> int
{
  auto core = Barghest::core::make_core(&Barghest::front_end::create_fe);
  return core.run();
}

#else


// Boost: TODO: Revoir les includes vraiment necessaires
// #include <boost/graph/graphviz.hpp>
// #include <boost/graph/adjacency_list.hpp>
// #include <boost/property_map/property_map.hpp>

// #include <boost/graph/depth_first_search.hpp>
// #include <boost/graph/breadth_first_search.hpp>

// #include <boost/graph/graph_utility.hpp>

#include <utility>    // std::pair
#include <iostream>    // std::cout

#include "Barghest/VM/VM.hpp"
// #include "Barghest/core/emulation_API/context.hh"
// #include "Barghest/core/emulation_API/graph_visitor.hh"
// #include "Barghest/core/emulation_API/context_helpers.hh"
// #include "Barghest/core/instruction_API/instruction.hh"
// #include "Barghest/core/instruction_API/operation.hh"

// #include "Barghest/core/tasks/implementations.hpp"


// class    print_path : public Barghest::core::emulation_API::graph_visitor
// {
// public:
//   print_path(Barghest::core::graph const &g) : graph_visitor(g)
//   {
//   }
//   auto    on_instruction(std::pair<size_t,
//                  Barghest::core::instruction_API::instruction const &> inst) -> void
//   {
//     std::cout << (void *)inst.first << ": " << inst.second.format() << std::endl;
//     if (inst.second.get_operation().get_ID() == ::e_idiv)
//     {

//       auto    &val = Barghest::core::emulation_API::get_value(this->get_context(), inst.second.get_operand(2));
//       if (val.get_bitfield_arith().contain(0) != 0)
//       {
//     std::cout << std::endl
//           << "[===============================]"
//           << "[===============================]"
//           << "[===============================]" << std::endl
//           << "[===============================]"
//           << "[== Division by zero detected ==]"
//           << "[===============================]" << std::endl
//           << "[===============================]"
//           << "[===============================]"
//           << "[===============================]" << std::endl
//           << "[==" << std::endl;
//     std::cout << "[== Integer divide by zero at address " << (void*) inst.first
//           << std::endl << "[== --> " << (void*) inst.first
//           << ": " << inst.second.format()
//           << std::endl << "[==" << std::endl
//           << std::endl << "[== --> Static multi value dump: "
//           << std::endl << "[==     " << val.get_bitfield_arith().prettyPrintMemory(false, true, true, false, true)
//           << std::endl << "[==" << std::endl;

//     std::cout << "[== Dump of 'C' source code for function main(int, char**):" << std::endl;
//     std::cout << "[== Source context:" << std::endl << "[==" << std::endl;
//     std::ifstream    ifs("/home/test/Barghest_project/barghest/rendu/libDYN-test/sample/c_test/zero_div_1.c");
//     std::string    src_line;
//     unsigned int    i = 1;
//     while (std::getline(ifs, src_line))
//     {
//       if (i - 7 <= 35 && 35 <= i + 2)
//       {
//         if (i == 35)
//           std::cout << "[== " << "zero_div_1.c" << ":" << i << " == ==> " << src_line << std::endl;
//         else
//           std::cout << "[== " << "zero_div_1.c" << ":" << i << " ==     " << src_line << std::endl;
//       }
//       i = i + 1;
//     }
//     std::cout << "[==" << std::endl << "[== Exiting ..."
//           << std::endl<< std::endl<< std::endl;
//     exit(42);
//       }
//     }
//   }
// };

int    main(int argc, char **argv)
{
  Barghest::VM::VM      g;

  std::cout << "Parsing: " << argv[1] << std::endl;
  g.parse(argv[1] ? argv[1] : "");
  // Barghest::core::emulation_API::context    context;
  // print_path    visitor(g);
  //  Barghest::Graph::outEdgeIterator    outedgeIt;
  //  Barghest::Graph::outEdgeIterator    outedgeEnd;
  //  Barghest::PrettyPrintInstFlow        dfs(g);
  //  Barghest::DivByZero            zeroDiv(g);

  // Barghest::core::tasks::do_parse_file(argv[1], g);
  // visitor.start();

  //  //Barghest::Graph::BGLGraph g1;
  // // boost::add_vertex(g1);
  //  //boost::write_graphviz(std::cout, g1);
  //  // std::cout << "Hello world " << argc << std::endl;
  //  // std::cout << "Hello world " << argv[1] << std::endl;
  //  if (argc <= 1)
  //  {
  //    std::cout << "Usage: " << argv[0] << " binary" << std::endl;// << std::flush;
  //    return (EXIT_FAILURE);
  //  }
  //  g.parse(argv[1]);
  //  std::cout << "End parsing" << std::endl;

  //  // auto pair_it = boost::vertices(bgl_graph);

  //  // boost::tie(outedgeIt, outedgeEnd) = boost::out_edges(0, bgl_graph);
  //  // auto vertex = boost::get(0, bgl_graph);
  //  // for (; pair_it.first < pair_it.second ; pair_it.first++)
  //  // {
  //  // }
  //  // std::find(pair_it.first, pair_it.second, )


  //  std::cout << "Main: *** Depth First ***" << std::endl;
  //  // dfs.start();
  //  zeroDiv.start();
  //  // depth_first_search
  //  //   (bgl_graph,
  //  //    visitor(make_dfs_visitor(boost::make_list(check_nodes(),
  //  //                                              check_edges(),
  //  //                                              finish(),
  //  //                            starting_node()
  //  //                 ))));
  //  // std::cout << std::endl;

  //  boost::write_graphviz(std::cerr, g.getBGLGraph());

  return (EXIT_SUCCESS);
}

#endif
