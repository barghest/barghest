// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#include "Barghest/core/symtab_API/symtab.hpp"

namespace Barghest
{
  namespace core
  {
    namespace symtab_API
    {

      symtab::symtab()
      {
        this->s_ = new Dyninst::SymtabAPI::Symtab();
      }

      symtab::symtab(MappedFile * mF)
      {
        this->s_ = new Dyninst::SymtabAPI::Symtab(mF);
      }

      symtab::symtab(unsigned char *mem_image, size_t image_size,
                     const std::string &name, bool defensive_binary, bool &err)
      {
        this->s_ = new Dyninst::SymtabAPI::Symtab(mem_image, image_size, name, defensive_binary, err);
      }

      symtab::~symtab()
      {
        if (this->s_ != NULL)
          delete this->s_;
      }

/**************************************
 *** LOOKUP FUNCTIONS *****************
 **************************************/

// Symbol

      auto    symtab::findSymbol(const std::string& name,    DynSymbol::SymbolType sType,
                                 Dyninst::SymtabAPI::NameType nameType,    bool isRegex, bool checkCase,
                                 bool includeUndefined) -> std::vector<DynSymbol*>
      {
        std::vector<DynSymbol*> vec;

        this->ret_ = this->s_->findSymbol(vec, name, sType, nameType, isRegex, checkCase, includeUndefined);
        return vec;
      }

      auto    symtab::getAllSymbols() -> std::vector<DynSymbol*>
      {
        std::vector<DynSymbol*> vec;

        this->ret_ = this->s_->getAllSymbols(vec);
        return vec;
      }

      auto    symtab::getAllSymbolsByType(DynSymbol::SymbolType sType) -> std::vector<DynSymbol*>
      {
        std::vector<DynSymbol*> vec;

        this->ret_ = this->s_->getAllSymbolsByType(vec, sType);
        return vec;
      }

      auto    symtab::findSymbolByOffset(Dyninst::Offset offset) -> std::vector<DynSymbol *> *
      {
        return this->s_->findSymbolByOffset(offset);
      }

      auto    symtab::getAllUndefinedSymbols() -> std::vector<DynSymbol*>
      {
        std::vector<DynSymbol*> vec;

        this->ret_ = this->s_->getAllUndefinedSymbols(vec);
        return vec;
      }

      auto    symtab::getAllDefinedSymbols() -> std::vector<DynSymbol*>
      {
        std::vector<DynSymbol*> vec;

        this->ret_ = this->s_->getAllDefinedSymbols(vec);
        return vec;
      }

      auto    symtab::findFuncByEntryOffset(DynFunc*& ret, const Dyninst::Offset offset) -> bool
      {
        this->ret_ = this->s_->findFuncByEntryOffset(ret, offset);
        return this->ret_;
      }

// Function

      auto    symtab::findFunctionsByName(std::string const& name, Dyninst::SymtabAPI::NameType nameType,
                                          bool isRegex, bool checkCase) -> std::vector<DynFunc *>
      {
        std::vector<DynFunc*> vec;

        this->ret_ = this->s_->findFunctionsByName(vec, name, nameType, isRegex, checkCase);
        return vec;
      }

      auto    symtab::getAllFunctions() -> std::vector<DynFunc*>
      {
        std::vector<DynFunc*> vec;

        this->ret_ = this->s_->getAllFunctions(vec);
        return vec;
      }

      auto    symtab::getContainingFunction(Dyninst::Offset offset, DynFunc*& ret) -> bool
      {
        this->ret_ = this->s_->getContainingFunction(offset, ret);
        return this->ret_;
      }

      auto    symtab::getContainingInlinedFunction(Dyninst::Offset offset, DynFuncBase*& ret) -> bool
      {
        this->ret_ = this->s_->getContainingInlinedFunction(offset, ret);
        return this->ret_;
      }

// Variable

      auto     symtab::findVariableByOffset(DynVar *&ret, const Dyninst::Offset offset) -> bool
      {
        this->ret_ = this->s_->findVariableByOffset(ret, offset);
        return this->ret_;
      }

      auto    symtab::findVariablesByName(std::string const& name, Dyninst::SymtabAPI::NameType nameType,
                                          bool isRegex, bool checkCase) -> std::vector<DynVar *>
      {
        std::vector<DynVar*> vec;

        this->ret_ = this->s_->findVariablesByName(vec, name, nameType, isRegex, checkCase);
        return vec;
      }

      auto    symtab::getAllVariables() -> std::vector <DynVar *>
      {
        std::vector<DynVar*> vec;

        this->ret_ = this->s_->getAllVariables(vec);
        return vec;
      }

// Module

      auto    symtab::getAllModules() -> std::vector<DynMod *>
      {
        std::vector<DynMod*> vec;

        this->ret_ = this->s_->getAllModules(vec);
        return vec;
      }

      auto    symtab::findModuleByOffset(DynMod *&ret, Dyninst::Offset off) -> bool
      {
        this->ret_ = this->s_->findModuleByOffset(ret, off);
        return this->ret_;
      }

      auto    symtab::findModuleByName(DynMod *&ret, std::string const& name) -> bool
      {
        this->ret_ = this->s_->findModuleByName(ret, name);
        return this->ret_;
      }

      auto    symtab::getDefaultModule() -> DynMod*
      {
        return this->s_->getDefaultModule();
      }

// Region

      auto    symtab::getCodeRegions() -> std::vector<DynRegion *>
      {
        std::vector<DynRegion*> vec;

        this->ret_ = this->s_->getCodeRegions(vec);
        return vec;
      }

      auto    symtab::getDataRegions() -> std::vector<DynRegion *>
      {
        std::vector<DynRegion*> vec;

        this->ret_ = this->s_->getDataRegions(vec);
        return vec;
      }

      auto    symtab::getAllRegions() -> std::vector<DynRegion *>
      {
        std::vector<DynRegion*> vec;

        this->ret_ = this->s_->getAllRegions(vec);
        return vec;
      }

      auto    symtab::getAllNewRegions() -> std::vector<DynRegion *>
      {
        std::vector<DynRegion*> vec;

        this->ret_ = this->s_->getAllNewRegions(vec);
        return vec;
      }

      auto    symtab::findRegion(DynRegion *&ret, std::string regname) -> bool
      {
        this->ret_ = this->s_->findRegion(ret, regname);
        return this->ret_;
      }

      auto    symtab::findRegion(DynRegion *&ret, const Dyninst::Offset addr, const unsigned long size) -> bool
      {
        this->ret_ = this->s_->findRegion(ret, addr, size);
        return this->ret_;
      }

      auto    symtab::findRegionByEntry(DynRegion *&ret, const Dyninst::Offset offset) -> bool
      {
        this->ret_ = this->s_->findRegionByEntry(ret, offset);
        return this->ret_;
      }

      auto    symtab::findEnclosingRegion(const Dyninst::Offset offset) -> DynRegion*
      {
        return this->s_->findEnclosingRegion(offset);
      }

// Exceptions

      auto    symtab::findException(DynExceptionB &excp, Dyninst::Offset addr) -> bool
      {
        this->ret_ = this->s_->findException(excp, addr);
        return ret_;
      }

      auto    symtab::getAllExceptions() -> std::vector<DynExceptionB *>
      {
        std::vector<DynExceptionB*> vec;

        this->ret_ = this->s_->getAllExceptions(vec);
        return vec;
      }

      auto    symtab::findCatchBlock(DynExceptionB &excp, Dyninst::Offset addr, unsigned size) -> bool
      {
        this->ret_ = this->s_->findCatchBlock(excp, addr, size);
        return ret_;
      }

// Relocation entries

      auto    symtab::getFuncBindingTable() -> std::vector<DynRelocatEntry>
      {
        std::vector<DynRelocatEntry> vec;

        this->ret_ = this->s_->getFuncBindingTable(vec);
        return vec;
      }

      auto    symtab::updateFuncBindingTable(Dyninst::Offset stub_addr, Dyninst::Offset plt_addr) -> bool
      {
        this->ret_ = this->s_->updateFuncBindingTable(stub_addr, plt_addr);
        return ret_;
      }


/**************************************
 *** SYMBOL ADDING FUNCS **************
 **************************************/

      auto    symtab::addSymbol(DynSymbol* newSym) -> bool
      {
        this->ret_ = this->s_->addSymbol(newSym);
        return ret_;
      }

      auto    symtab::addSymbol(DynSymbol* newSym, DynSymbol* referringSymbol) -> bool
      {
        this->ret_ = this->s_->addSymbol(newSym, referringSymbol);
        return ret_;
      }

      auto    symtab::createFunction(std::string name, Dyninst::Offset offset, size_t size, DynMod* mod) -> DynFunc*
      {
        return this->s_->createFunction(name, offset, size, mod);
      }

      auto    symtab::createVariable(std::string name, Dyninst::Offset offset, size_t size, DynMod* mod) -> DynVar*
      {
        return this->s_->createVariable(name, offset, size, mod);
      }

      auto    symtab::deleteFunction(DynFunc* func) -> bool
      {
        this->ret_ = this->s_->deleteFunction(func);
        return ret_;
      }

      auto    symtab::deleteVariable(DynVar* var) -> bool
      {
        this->ret_ = this->s_->deleteVariable(var);
        return ret_;
      }

      auto    symtab::isExec() -> bool
      {
        this->ret_ = this->s_->isExec();
        return ret_;
      }

      auto    symtab::isStripped() -> bool
      {
        this->ret_ = this->s_->isStripped();
        return ret_;
      }

      auto    symtab::getObjectType() const -> DynObjectType
      {
        return this->s_->getObjectType();
      }

      auto    symtab::getArchitecture() -> Dyninst::Architecture
      {
        return this->s_->getArchitecture();
      }

      auto    symtab::isCode(const Dyninst::Offset where) -> bool
      {
        this->ret_ = this->s_->isCode(where);
        return ret_;
      }

      auto    symtab::isData(const Dyninst::Offset where) -> bool
      {
        this->ret_ = this->s_->isData(where);
        return ret_;
      }

      auto    symtab::isValidOffset(const Dyninst::Offset where) -> bool
      {
        this->ret_ = this->s_->isValidOffset(where);
        return ret_;
      }

      auto    symtab::isNativeCompiler() -> bool
      {
        this->ret_ = this->s_->isNativeCompiler();
        return ret_;
      }

      auto    symtab::getMappedRegions() -> std::vector<DynRegion *>
      {
        std::vector<DynRegion*> vec;

        this->ret_ = this->s_->getMappedRegions(vec);
        return vec;
      }

/***** Line Number Information *****/

      auto    symtab::getAddressRanges(std::string lineSource, unsigned int LineNo) -> std::vector<std::pair<Dyninst::Offset, Dyninst::Offset> >
      {
        std::vector<std::pair<Dyninst::Offset, Dyninst::Offset> > vec;

        this->ret_ = this->s_->getAddressRanges(vec, lineSource, LineNo);
        return vec;
      }

      auto    symtab::getSourceLines(Dyninst::Offset addressInRange) -> std::vector<DynStatement *>
      {
        std::vector<DynStatement *> vec;

        this->ret_ = this->s_->getSourceLines(vec, addressInRange);
        return vec;
      }

      auto    symtab::addLine(std::string lineSource, unsigned int lineNo, unsigned int lineOffset,
                              Dyninst::Offset lowInclAddr, Dyninst::Offset highExclAddr) -> bool
      {
        this->ret_ = this->s_->addLine(lineSource, lineNo, lineOffset, lowInclAddr, highExclAddr);
        return ret_;
      }

      bool    symtab::addAddressRange(Dyninst::Offset lowInclAddr, Dyninst::Offset highExclAddr, std::string const& lineSource,
                                      unsigned int lineNo, unsigned int lineOffset)
      {
        this->ret_ = this->s_->addAddressRange(lowInclAddr, highExclAddr, lineSource, lineNo, lineOffset);
        return ret_;
      }

      void    symtab::setTruncateLinePaths(bool value)
      {
        this->s_->setTruncateLinePaths(value);
      }

      bool    symtab::getTruncateLinePaths()
      {
        this->ret_ = this->s_->getTruncateLinePaths();
        return ret_;
      }

/***** Type Information *****/

      auto    symtab::findType(DynType *&type, std::string const& name) -> bool
      {
        this->ret_ = this->s_->findType(type, name);
        return ret_;
      }

      auto    symtab::findType(unsigned type_id) -> DynType*
      {
        return this->s_->findType(type_id);
      }

      auto    symtab::findVariableType(DynType *&type, std::string const& name) -> bool
      {
        this->ret_ = this->s_->findVariableType(type, name);
        return ret_;
      }

      auto    symtab::addType(DynType *type) -> bool
      {
        this->ret_ = this->s_->addType(type);
        return ret_;
      }

      auto    symtab::parseTypesNow() -> void
      {
        this->s_->parseTypesNow();
      }

/***** Local Variable Information *****/

      auto    symtab::findLocalVariable(std::string const& name) -> std::vector<DynlocalVar *>
      {
        std::vector<DynlocalVar *> vec;

        this->ret_ = this->s_->findLocalVariable(vec, name);
        return vec;
      }

/***** Relocation Sections *****/

      auto    symtab::hasRel() const -> bool
      {
        return this->s_->hasRel();
      }

      auto    symtab::hasRela() const -> bool
      {
        return this->s_->hasRela();
      }

      auto    symtab::hasReldyn() const -> bool
      {
        return this->s_->hasReldyn();
      }

      auto    symtab::hasReladyn() const -> bool
      {
        return this->s_->hasReladyn();
      }

      auto    symtab::hasRelplt() const -> bool
      {
        return this->s_->hasRelplt();
      }

      auto    symtab::hasRelaplt() const -> bool
      {
        return this->s_->hasRelaplt();
      }

      auto    symtab::isStaticBinary() const -> bool
      {
        return this->s_->isStaticBinary();
      }


/***** Write Back binary functions *****/

      auto    symtab::emitSymbols(DynObject *linkedFile, std::string const& filename, unsigned flag) -> bool
      {
        this->ret_ = this->s_->emitSymbols(linkedFile, filename, flag);
        return ret_;
      }

      auto    symtab::addRegion(Dyninst::Offset vaddr, void *data, unsigned int dataSize,
                                std::string const& name, DynRegion::RegionType rType, bool loadable,
                                unsigned long memAlign, bool tls) -> bool
      {
        this->ret_ = this->s_->addRegion(vaddr, data, dataSize, name, rType, loadable, memAlign, tls);
        return ret_;
      }

      auto    symtab::addRegion(DynRegion *newreg) -> bool
      {
        this->ret_ = this->s_->addRegion(newreg);
        return ret_;
      }

      auto     symtab::emit(std::string const& filename, unsigned flag) -> bool
      {
        this->ret_ = this->s_->emit(filename, flag);
        return ret_;
      }

      auto     symtab::addDynLibSubstitution(std::string const& oldName, std::string const& newName) -> void
      {
        this->s_->addDynLibSubstitution(oldName, newName);
      }

      auto     symtab::getDynLibSubstitution(std::string const& name) -> std::string
      {
        return this->s_->getDynLibSubstitution(name);
      }

      auto     symtab::getSegments() -> std::vector<DynSegment>
      {
        std::vector<DynSegment> vec;

        this->ret_ = this->s_->getSegments(vec);
        return vec;
      }

      auto     symtab::fixup_code_and_data(Dyninst::Offset newImageOffset,    Dyninst::Offset newImageLength,
                                           Dyninst::Offset newDataOffset, Dyninst::Offset newDataLength) -> void
      {
        this->s_->fixup_code_and_data(newImageOffset, newImageLength, newDataOffset, newDataLength);
      }

      auto     symtab::fixup_RegionAddr(const char* name, Dyninst::Offset memOffset, long memSize) -> bool
      {
        this->ret_ = this->s_->fixup_RegionAddr(name, memOffset, memSize);
        return ret_;
      }

      auto     symtab::fixup_SymbolAddr(const char* name, Dyninst::Offset newOffset) -> bool
      {
        this->ret_ = this->s_->fixup_SymbolAddr(name, newOffset);
        return ret_;
      }

      auto     symtab::updateRegion(const char* name, void *buffer, unsigned size) -> bool
      {
        this->ret_ = this->s_->updateRegion(name, buffer, size);
        return ret_;
      }

      auto     symtab::updateCode(void *buffer, unsigned size) -> bool
      {
        this->ret_ = this->s_->updateCode(buffer, size);
        return ret_;
      }

      auto     symtab::updateData(void *buffer, unsigned size) -> bool
      {
        this->ret_ = this->s_->updateData(buffer, size);
        return ret_;
      }

      auto     symtab::getFreeOffset(unsigned size) -> Dyninst::Offset
      {
        return this->s_->getFreeOffset(size);
      }

      auto     symtab::addLibraryPrereq(std::string const& libname) -> bool
      {
        this->ret_ = this->s_->addLibraryPrereq(libname);
        return ret_;
      }

      auto     symtab::addSysVDynamic(long name, long value) -> bool
      {
        this->ret_ = this->s_->addSysVDynamic(name, value);
        return ret_;
      }

      auto     symtab::addLinkingResource(DynArchive *library) -> bool
      {
        this->ret_ = this->s_->addLinkingResource(library);
        return ret_;
      }

      auto     symtab::getLinkingResources() -> std::vector<DynArchive*>
      {
        std::vector<DynArchive*> vec;

        this->ret_ = this->s_->getLinkingResources(vec);
        return vec;
      }

      auto     symtab::addExternalSymbolReference(DynSymbol *externalSym, DynRegion *localRegion, DynRelocatEntry localRel) -> bool
      {
        this->ret_ = this->s_->addExternalSymbolReference(externalSym, localRegion, localRel);
        return ret_;
      }

      auto     symtab::addTrapHeader_win(Dyninst::Address ptr) -> bool
      {
        this->ret_ = this->s_->addTrapHeader_win(ptr);
        return ret_;
      }

      auto     symtab::updateRelocations(Dyninst::Address start, Dyninst::Address end, DynSymbol *oldsym, DynSymbol *newsym) -> bool
      {
        this->ret_ = this->s_->updateRelocations(start, end, oldsym, newsym);
        return ret_;
      }

/***** Data Member Access *****/

      auto    symtab::file() const -> std::string
      {
        return this->s_->file();
      }

      auto    symtab::name() const -> std::string
      {
        return this->s_->name();
      }

      auto    symtab::memberName() const -> std::string
      {
        return this->s_->memberName();
      }

      auto    symtab::mem_image() const -> char *
      {
        return this->s_->mem_image();
      }

      auto    symtab::imageOffset() const -> Dyninst::Offset
      {
        return this->s_->imageOffset();
      }

      auto    symtab::dataOffset() const -> Dyninst::Offset
      {
        return this->s_->dataOffset();
      }

      auto    symtab::dataLength() const -> Dyninst::Offset
      {
        return this->s_->dataLength();
      }

      auto    symtab::imageLength() const -> Dyninst::Offset
      {
        return this->s_->imageLength();
      }

      auto    symtab::getInitOffset() -> Dyninst::Offset
      {
        return this->s_->getInitOffset();
      }
//
      auto    symtab::getFiniOffset() -> Dyninst::Offset
      {
        return this->s_->getFiniOffset();
      }

      auto    symtab::getInterpreterName() const -> const char*
      {
        return this->s_->getInterpreterName();
      }

      auto    symtab::getAddressWidth() const -> unsigned
      {
        return this->s_->getAddressWidth();
      }

      auto    symtab::getLoadOffset() const -> Dyninst::Offset
      {
        return this->s_->getLoadOffset();
      }

      auto    symtab::getEntryOffset() const -> Dyninst::Offset
      {
        return this->s_->getEntryOffset();
      }

      auto    symtab::getBaseOffset() const -> Dyninst::Offset
      {
        return this->s_->getBaseOffset();
      }

      auto     symtab::getTOCoffset(DynFunc *func) const -> Dyninst::Offset
      {
        return this->s_->getTOCoffset(func);
      }

      auto     symtab::getTOCoffset(Dyninst::Offset off) const -> Dyninst::Offset
      {
        return this->s_->getTOCoffset(off);
      }

      auto     symtab::getLoadAddress() -> Dyninst::Offset
      {
        return this->s_->getLoadAddress();
      }

      auto     symtab::isDefensiveBinary() const -> bool
      {
        return this->s_->isDefensiveBinary();
      }

      auto     symtab::fileToDiskOffset(Dyninst::Offset off) const -> Dyninst::Offset
      {
        return this->s_->fileToDiskOffset(off);
      }

      auto     symtab::fileToMemOffset(Dyninst::Offset off) const -> Dyninst::Offset
      {
        return this->s_->fileToMemOffset(off);
      }

      auto     symtab::getDefaultNamespacePrefix() const -> std::string
      {
        return this->s_->getDefaultNamespacePrefix();
      }

      auto     symtab::getNumberofRegions() const -> unsigned
      {
        return this->s_->getNumberofRegions();
      }

      auto     symtab::getNumberofSymbols() const -> unsigned
      {
        return this->s_->getNumberofSymbols();
      }

      auto     symtab::getDependencies() -> std::vector<std::string>&
      {
        return this->s_->getDependencies();
      }

      auto     symtab::removeLibraryDependency(std::string const& lib) -> bool
      {
        return this->s_->removeLibraryDependency(lib);
      }

      auto     symtab::getParentArchive() const -> DynArchive *
      {
        return this->s_->getParentArchive();
      }

/***** Error Handling *****/

      auto     symtab::delSymbol(DynSymbol *sym) -> bool
      {
        return this->deleteSymbol(sym);
      }

      auto     symtab::deleteSymbol(DynSymbol *sym) -> bool
      {
        return this->s_->deleteSymbol(sym);
      }

      // auto     symtab::getSymbolByIndex(unsigned i) -> DynSymbol *
      // {
      //     return this->s_->getSymbolByIndex(i);
      // }

// Used by binaryEdit.C...

      auto     symtab::canBeShared() -> bool
      {
        return this->s_->canBeShared();
      }

      auto     symtab::getOrCreateModule(std::string const& modName, const Dyninst::Offset modAddr) -> DynMod*
      {
        return this->s_->getOrCreateModule(modName, modAddr);
      }

//Only valid on ELF formats

      auto     symtab::getElfDynamicOffset() -> Dyninst::Offset
      {
        return this->s_->getElfDynamicOffset();
      }

// SymReader interface

      auto     symtab::getSegmentsSymReader() -> std::vector<Dyninst::SymSegment>
      {
        std::vector<Dyninst::SymSegment> vec;

        this->s_->getSegmentsSymReader(vec);
        return vec;
      }

      auto     symtab::rebase(Dyninst::Offset offset) -> void
      {
        this->s_->rebase(offset);
      }
//

      auto     symtab::getLastReturn() const -> bool
      {
        return this->ret_;
      }


    } /* !namespace symtab_API */
  } /* !namespace core */
} /* !namespace Barghest */
