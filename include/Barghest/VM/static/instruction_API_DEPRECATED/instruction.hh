#ifndef BARGHEST_INSTRUCTION_HH__
# define BARGHEST_INSTRUCTION_HH__

# include <vector>
# include <set>

# include <dyninst/Instruction.h>

# include "Barghest/core/instruction_API/operand.hh"
# include "Barghest/core/instruction_API/operation.hh"
# include "Barghest/core/instruction_API/expressions/register.hh"

/*!
** If defined, the Instruction class precache all internal information
** in the constructor and allows the class to be thread safe as it,
** and more getter to be defined const. The graph construction is slower
** but the latter analysis are speed up.
**
** If undefined, the Instruction class perform a lazy caching (only
** get and cache information at the moment they are asked). Which is
** less expensive in memory but cost at the first time they are used.
** Also, this is not thread safe and allows module to have indirect
** write impact on the binary graph. (which is wrong in term of design)
*/
// # define BARGHEST_FULL_PRECACHED_INSTRUCTION

namespace Barghest
{
  namespace core
  {
    namespace instruction_API
    {
      /*!
      ** TODO: trouver un moyen de s'affranchir des races-condition avec le prechaching
      ** Note: This class is intended to be used after a block->getInsns() (which
      **       return a copy of all Instruction.). It is not thread safe as it, as
      **       each thread is supposed to have his own copy.
      **       In this case:
      **       0 Lazy caching
      **       1 A module use Instruction method which need to load dyninst::instruction
      **       2 Dyninst Instruction loaded + all underlaying wrapper build (Expression/Register/etc)
      **         by a single thread on the shared graph.
      **      -3 If an other thread needs it, there is an error
      **      TODO: Add mutex system in lazy mode
      */
      class instruction
      {
      public:
	/*!
	** Instruction constructor.
	** It makes a copy of the instruction shared pointer and precached read
	** and written register, instruction and operands for optimisation.
	** Precaching is done if "BARGHEST_FULL_PRECACHED_INSTRUCTION"
	** is defined.
	*/
	instruction(Dyninst::InstructionAPI::Instruction::Ptr const instruction);
	instruction(instruction const &other) = delete;
	instruction(instruction &&other) = default;
	auto operator=(instruction const) -> instruction & = delete;
	auto operator=(instruction &&) -> instruction & = default;

	/*!
	** Return the raw Dyninst instruction class.
	** Note: If the setInsnt has not been used, this method return an empty
	** shared pointer.
	*/
	auto get_raw(void) const
	  -> Dyninst::InstructionAPI::Instruction::Ptr const;

	/*!
	** All the following methods are Dyninst method overloads.
	*/

	/*!
	** Return the operation of this Instruction
	** (Ex: For mov al, 5, The operation is "mov").
	*/
	auto get_operation(void) const -> const operation &;
	/*!
	** Return all the operands associated with this instruction.
	*/
	auto get_operands(void) const -> std::vector<operand> const &;
	/*!
	** Like getOperands(void), but instead of returning a precached operand list,
	** it make a copy and append it to the variable given in parameter. If you
	** aim for speed and do not need to edit those operand, it is recommanded to
	** use the getOperands(void) version.
	*/
	auto get_operands(std::vector<operand> &operands) const -> void;

	/*!
	** Return the operand number "index" of this instruction starting at 0.
	** If index is out of range an exception occured.
	*/
	auto get_operand(unsigned int index) const -> operand const &;

	/*!
	** Returns the index th byte in the instruction.
	** If the "index" is out of range the value returned is equal to zero.
	*/
	auto raw_byte(unsigned int index) const -> unsigned char;
	auto size(void) const -> size_t;
	auto ptr(void) const -> void const *;

	auto format(void) const -> std::string;

      private:
	Dyninst::InstructionAPI::Instruction::Ptr inst_;
	Barghest::core::instruction_API::operation operation_;

	std::vector<Dyninst::InstructionAPI::Operand> dyn_operands_;
	std::vector<Barghest::core::instruction_API::operand> operands_;

	std::set<Barghest::core::instruction_API::expressions::register_t> regs_read_;
	std::set<Barghest::core::instruction_API::expressions::register_t> regs_written_;

	int read_memory_;
	int write_memory_;

      }; // !class instruction
    } // !namespace instruction_API
  } // !namespace core
} // !namespace Barghest

#endif /* !BARGHEST_INSTRUCTION_HH__ */
