#ifndef BARGHEST_CONTEXT_HH_
# define BARGHEST_CONTEXT_HH_

// # include <fwdios>	// forward definition
# include <list>	// std::list
// # include <map>		// std::map
# include <vector>	// std::vector

# include <boost/numeric/interval.hpp>	// boost interval<>

# include <dyninst/entryIDs.h> // Dyninst instruction ID

# include <bitField/bitfield_t.hpp>		// bitfield_t
# include <bitField/BitFieldArithmetic/BitFieldArithmetic.hh>	// BitFieldArithmetic<T>

# include "Barghest/core/parse_API/block.hh"
# include "Barghest/core/instruction_API/instruction.hh"

namespace Barghest
{
  namespace core
  {
    namespace emulation_API
    {

      /*
      **
      */
      class context
      {
      public:
	/*!
	** A type allowing to store the trace: The list of all the instruction
	** emulated by now.
	*/
	typedef std::list<std::pair<size_t,
				    instruction_API::instruction const &> > t_trace;

	// typedef std::map<unsigned int, BitField::BitFieldAlloc>			t_reg;

	// typedef std::vector<std::pair<
	// 			  /* Variable interval */	  boost::numeric::interval_lib::interval<size_t>,
	// 			  /* Variable interval */ int value>
	// 			> t_memory;

	/*!
	** @brief An array of MAX_X86_64_BASE_VAL element which will hold data for
	** the value emulator. MAX_X86_64_BASE_VAL is the number of register
	** this context have to emulate.
	** Ex: The x86_64::base and x86::base can be used as index
	**     (array[x86_64::base::rax])
	*/
	typedef std::array<MultiValEmulator<size_t>,
			   instruction_API::x86_64::base::MAX_X86_64_BASE_VAL> t_reg;

      public:
	context();
	~context();

	context(context const &other);
	auto	operator=(context const &other) -> context &;


	auto	copy_trace(t_trace const &other_trace) -> void;


	/*!
	** Return a reference of the trace of this context.
	*/
	auto	get_trace(void) const -> t_trace const &;
	auto	get_trace_mutable(void) -> t_trace &;

	/*!
	** Return the map with the emulated values of the register.
	*/
	auto	get_registers(void) const -> t_reg const &;
	auto	get_registers_mutable(void) -> t_reg &;

	/* TODO */
	// auto	getRegister(mon register) const -> ?? const &;

	/*!
	** Update the current context with the instruction of the block at the
	** given offset.
	*/
	auto	emulate(std::pair<size_t, instruction_API::instruction const &> inst) -> void;

	/*!
	** Pretty print the instruction already emulated by this context.
	*/
	auto	print_trace(std::ostream& stream = std::cout) const -> void;

	// TODO: passer en private et mettre le context helper en friend dans une class
	auto	get_value_last_return(void) -> MultiValEmulator<size_t> &;

      private:

#define BARGHEST_CONTEXT_INST_PROTO					\
	size_t offset,							\
	  instruction_API::instruction const &inst,			\
	  std::vector<instruction_API::operand> const &operands

	auto	unknown_inst_(BARGHEST_CONTEXT_INST_PROTO) -> void;
	auto	add_emul_(BARGHEST_CONTEXT_INST_PROTO) -> void;
	auto	sub_emul_(BARGHEST_CONTEXT_INST_PROTO) -> void;
	auto	mov_emul_(BARGHEST_CONTEXT_INST_PROTO) -> void;
	auto	shl_sal_emul_(BARGHEST_CONTEXT_INST_PROTO) -> void;

      private:
	/*!
	** TODO: class contextData
	** TODO: class context contient les methodes pour instrumenter contextData
	** Operand Fourni les informations sur le graph (quelles registres, quel memoire, etc)
	** contextData Fourni les valeurs de l'emulation
	** context utilise les deux. Ex: context.set(Operand -clefs-, context -valeur-)
	*/

	/*!
	** @brief A list of all the instruction successfully emulated by this context.
	** The first element of the pair is the offset of the instruction, while the
	** the second one is the instruction itself.
	** Internal notes: It is design to contain a reference on the Instruction
	** precached by the ParseAP::Block in order to avoid a strong memory usage
	** when used in a multhreaded environment.
	** TODO: faire des methodes qui recree l'historique de valeur et memoire en
	**       utilisant la trace.
	** TODO: create a class that agregate registers and auto return a
	** generic type with all arithmetic operator using the right aliasing
	*/
	t_trace				trace_;

	// ValueEmulation	_valueEmul;
	t_reg				registers_;

	/*!
	** A MultiValEmulator memory used to stored tmp data.
	** Exemple: When required to do a get_value() with an immediate operand
	** Exemple: To handle aliasing (get ah from eax) whitout editing the context.
	*/
	MultiValEmulator<size_t>	tmp_storage_;

	void    (context::*fct_[::_entry_ids_max_])
	(
	  size_t offset,
	  instruction_API::instruction const &,
	  std::vector<instruction_API::operand> const &operands
	  );
      };

    } // !namespace emulation_API
  } // !namespace core
} // !namespace Barghest

#endif /* !BARGHEST_CONTEXT_HH_ */
