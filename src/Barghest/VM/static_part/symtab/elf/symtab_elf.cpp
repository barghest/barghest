// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#include <iostream> // std::cout
#include <sys/mman.h> // mmap / munmap
#include <cstring> // memset & co, strerror

#include <sys/types.h> // open()
#include <sys/stat.h> // open()
#include <fcntl.h> // open()
#include <unistd.h> // open()

#include <cassert> // assert()
#include <cstdint> // std::*int*_t type
#include <cerrno> // for strerror and errno

#include <boost/current_function.hpp> // BOOST_CURRENT_FUNCTION


// #include <fstream> // fopen()

#include "Barghest/VM/static_part/symtab/elf/symtab_elf.hpp"

namespace Barghest
{
  namespace VM
  {
    namespace static_part
    {
      namespace symtab
      {
        namespace elf
        {

          symtab_elf::symtab_elf(std::string const &file_name)
            : symtab(""), file_name_(file_name),
              fd_(-1), e_(NULL), ek_(ELF_K_NONE)
          {
            if (file_name.size())
              this->open(file_name);
          }

          symtab_elf::~symtab_elf()
          {
            this->close();
          }

          auto    symtab_elf::open(std::string const &file_name) -> void
          {
            try {this->close();} catch (...){} // TODO: add to boost log if fail
            try
            {
              if ((this->fd_ = ::open(file_name.c_str(), O_RDONLY)) != -1)
              {
                this->file_name_ = file_name;
                if (elf_version(EV_CURRENT) == EV_NONE)
                  throw err::ELF_VERSION;
                if ((this->e_ = elf_begin(this->fd_, ELF_C_READ, NULL)) == nullptr)
                  throw err::ELF_BEGIN;
                if ((this->elf_class_ = gelf_getclass(e_)) == ELFCLASSNONE)
                  throw err::GELF_GETCLASS;
                if (gelf_getehdr(e_, &ehdr_) == nullptr)
                  throw err::GELF_GETEHDR;

                this->ek_ = elf_kind(this->e_);
              }
              else
                throw err::OPEN;
            }
            catch (err e)
            {
              std::string msg = BOOST_CURRENT_FUNCTION;

              msg += ": ";
              switch (e) // regular error
              {
                case err::OPEN:
                {
                  msg += "unable to open ";
                  msg += file_name;
                  msg += ": ";
                  msg += strerror(errno);
                }; break;
                default:
                {
                  switch (e) // elf error
                  {
                    case err::ELF_VERSION:
                      msg += "elf_version() failed: "; break;
                    case err::ELF_BEGIN:
                      msg += "elf_begin() failed: "; break;
                    case err::GELF_GETCLASS:
                      msg += "gelf_getclass() : not an ELF{32, 64} file. "; break;
                    case err::GELF_GETEHDR:
                      msg += "gelf_getehdr() failed: "; break;
                    default: assert(0 && "Internal error: unhandled exception");
                  }; // end switch
                  if (e != err::GELF_GETCLASS)
                    msg += elf_errmsg(-1);
                }
              }; // end switch
              try {this->close();} catch (...){} // TODO: add to boost log if fail
              // std::cout << "msg => " << msg << std::endl;
              throw std::runtime_error(msg);
            } // end catch
            return ; // Success
          }

          auto symtab_elf::close(void) -> void
          {
            if (this->e_)
              elf_end(e_);
            if (this->fd_ != -1)
              ::close(this->fd_);
            this->e_ = nullptr;
            this->fd_ = -1;
            this->file_name_ = "";
            // for (auto &mapped_pair : this->mapped_region_)
            // {
            //   if (::munmap(mapped_pair.first, mapped_pair.second) == -1)
            //   {
            //     std::string err_msg = BOOST_CURRENT_FUNCTION;
            //     err_msg += ": unable to unmap: ";
            //     err_msg += strerror(errno);
            //     // this->VM_ERR_OBJ_NAME.set(err_id::munmap, err_msg);
            //     throw std::runtime_error(err_msg);
            //     // last_err = err::MUNMAP;
            //     // last_err_msg = err_msg;
            //     // return (false);
            //   }
            // }
            // return (true);
          }

          // TODO: like IDA, normalize address type
          // TODO: review return value
          auto symtab_elf::get_entry_point(size_t &entry) const -> bool
          {
            if (fd_ >= 0)
            {
              if (elf_kind(e_) == ELF_K_ELF)
                entry = ehdr_.e_entry;
              else // AR and DATA elf type remaining
                throw std::runtime_error("Unsupported elf type");
              return (true);
            }
            else
              throw std::logic_error("No file previously opened");
            return (false);
          }

          auto    symtab_elf::get_file_name(void) const -> std::string const &
          {
            return (this->file_name_);
          }

          auto  symtab_elf::get_mapped_region(std::vector<mapped_region> &mr) const -> void
          {
            Elf_Scn             *scn;
            size_t              shdr_str_index;
            GElf_Shdr           shdr;
            std::string         sname;
            mapped_region       region;
            Elf_Data            *data;

            scn = nullptr;
            data = nullptr;
            elf_getshdrstrndx(this->e_, &shdr_str_index);
            while ((scn = elf_nextscn(this->e_, scn)))
            {
              gelf_getshdr(scn, &shdr);
              sname = elf_strptr(this->e_, shdr_str_index, shdr.sh_name);
              region.name = sname;
              region.start_addr = 0;
              region.size = 0;
              region.offset = 0;
              region.right.r = false;
              region.right.w = false;
              region.right.x = false;
              region.right.s = false;
              region.right.p = false;
              // region.mem = nullptr;
              // std::cout << "Found one section : " << sname << std::endl;
              // while (data = elf_getdata(scn, data))
              //   std::cout << "* One data found" << std::endl;
              data = elf_getdata(scn, nullptr);
              if (data && sname == ".text") // only take care of .text for now
              {
                // extern Elf_Data *elf_getdata (Elf_Scn *__scn, Elf_Data *__data);
                // char    *begin = (char *)data->d_buf;
                // char    *end = (char *)((size_t)data->d_buf + data->d_size);
                // region.mem = new std::vector<std::uint8_t>(begin, end);
                region.mem = new std::uint8_t[data->d_size];
                std::cout << data->d_size << std::endl;
                std::memcpy(region.mem, data->d_buf, data->d_size);
                region.size = data->d_size;
                // TODO: free the mem and manage with std::shared_ptr

                // mr.mem = nullptr;
                mr.push_back(region);
              }

            }
            // throw std::logic_error("Unimplemented");
          }

          auto  symtab_elf::free_mapped_region(std::vector<mapped_region> &mr) const -> void
          {
            throw std::logic_error("Unimplemented");
          }

// #include <sys/mman.h>
//
//   void *mmap(void *addr, size_t length, int prot, int flags,
//       int fd, off_t offset);


          // auto symtab_elf::map_section(std::string const &section_name,
          //                          void *&memory,
          //                          size_t &size) -> bool
          // {
          //   void        *mem;
          //   size_t      shstrndx; // SectionHeaderSTRingiNDeX
          //   size_t      section_len;
          //   off_t       section_offset;
          //   Elf_Scn     *scn;
          //   GElf_Shdr   shdr;
          //   char const  *sect_name;

          //   scn = nullptr;
          //   memory = nullptr;
          //   // shdr = nullptr;
          //   size = 0;
          //   if (fd >= 0) // check if file has correctly been opened
          //   {
          //     if (elf_getshdrstrndx(e_, &shstrndx) == 0)
          //     {
          //       bool found;

          //       while ((scn = elf_nextscn(e_, scn))
          //              && gelf_getshdr(scn, &shdr)
          //              && (sect_name = elf_strptr(e_, shstrndx, shdr.sh_name))
          //              && !(found = (!strcmp(sect_name, section_name.c_str()))));
          //       if (found == true)
          //       {
          //         std::cout << "Found section: " << sect_name << std::endl;
          //       }
          //       // See man(3) elf (libelf) for Elf_Data struct
          //       Elf_Data *text_mem = elf_getdata(scn, nullptr);
          //       memory = text_mem->d_buf;
          //       size = text_mem->d_size;
          //       // std::ofstream test;
          //       // test.open("/tmp/test.txt", std::ofstream::binary | std::ofstream::out);
          //       // test.write((char const *)text_mem->d_buf, text_mem->d_size);
          //       // test.close();
          //       return (true);
          //     }
          //     else
          //       std::cerr << "Unable to recover the section string table" << std::endl;
          //     // mem = mmap(nullptr, section_len,
          //     //    PROT_READ, MAP_PRIVATE,
          //     //    this->fd_, section_offset);
          //   }
          //   else
          //     throw std::logic_error("No file previously opened");
          //   return (false);
          // }

          // auto symtab_elf::get_fct_addr(void) const -> std::vector<intptr_t>
          // {
          // }

        } // !namespace elf
      } // !namespace symtab
    } // !namespace static_part
  } // !namespace VM
} // !namespace Barghest
