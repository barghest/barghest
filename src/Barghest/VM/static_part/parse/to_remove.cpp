/*
 Avoir des fonctions dans parse qui
 * prennent en entrée un mapping éxécutable
 * ressorte un graph boost complet (egde et block bien tagé)

Propriété:
 * thread safe ?
Modules:
 * Première passe linéaire
 * Se servir des symboles de fonctions
 * Se servir d'heuristique (comme IDA), pour détecter du code
 * * Prologe/Epilogue, vtable, appel, syscall, etc
 * Emulation par un module "stack only" ?
 * Emulation par un module "complete emulation" pour le cas ou un ret est variable
 * liste de passe pour générer le graph ? (modulaire)


create_block(start_addr) -> end_addr
 * so le block n'existe pas, le crée et trouver le end
 * * le "end" peut être trouvé facilement en parcours linéaire
 * sinon, trouver le block correspondant et split avec un FallThrow
 * info supplémentaire sur ce qui se passe (est ce que c'est un nouveau, un split ?)

Comment gérer les edges ?

add_edge(from_inst, to_inst, type) -> bool
 * creer un edge, retourne true/false (true si tout est okay)

type:
* FallThrow
* call
* call fallthrow
* ret
* jmp
* (jcc) branch taken (+ tell if false predicate)
* (jcc) branch not taken (+ tell if false predicate)
* sink

get_last_error() -> enum ?;
* block
* * block exist
* * address out of scope
* * Can't split

* edge
* * Invalid "from" instruction (not a jmp/call/etc)
* * Invalid "to" instruction  (not a jmp/call/etc)

*/
