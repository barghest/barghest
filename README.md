# Barghest

## What is Barghest

Barghest is a static binary static analysis tool suite targeting x86 and x86_64
architectures. It is modular and opensource.

The goal of Barghest is to provide mutiple layers of tools to improve software
analysis in a variety of domains, incouding non exclusively debugging, reverse
engineering, research, and testing.

Barghest main strength comes from its ability to directly analyse the binary,
and from its ability to do static analysis.

Of course Barghest's approach still comes with several drawbacks, the main ones
being speed and resource consumption.

## What is this repository about

This repository provides a static binary analyser built with the set of tools
provided by the Barghest project.

Usage exemple:

>> ./barghest /bin/ls ## Run the analyser on the program "ls"

## Notes:

We are currently actively working on this part of the project and the code
presented here as to be considered for now as a work in progress.