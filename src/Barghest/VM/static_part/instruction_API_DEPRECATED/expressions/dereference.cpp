// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#include <cassert>
#include <vector>

#include <boost/pointer_cast.hpp>
#include <dyninst/Expression.h>
#include <dyninst/Dereference.h>

#include "Barghest/core/instruction_API/expressions/dereference.hh"
#include "Barghest/core/dyninst_helper.hpp"

namespace Barghest
{
  namespace core
  {
    namespace instruction_API
    {
      namespace expressions
      {
        dereference::dereference(Dyninst::InstructionAPI::Expression::Ptr expr)
          : expression(expression::DEREFERENCE)
        {
          std::vector<Dyninst::InstructionAPI::Expression::Ptr>	child;

          auto deref = boost::dynamic_pointer_cast<Dyninst::InstructionAPI::Dereference>(expr);
          assert(deref);
          set_raw(deref);

          deref->getChildren(child); // A deref has only one child
          assert(child.size() == 1);

          this->binary_func_expr_ = init_dyn_expr(child[0]);

        }

        dereference::~dereference()
        {
          delete this->binary_func_expr_;
        }

        auto dereference::format(void) const -> std::string
        {
          return (this->get_raw()->format());
        }

        auto dereference::get_binary_func_expr(void) const -> instruction_API::expression const &
        {
          return (*this->binary_func_expr_);
        }

        auto dereference::get_raw_expr(void) const
          -> Dyninst::InstructionAPI::Expression::Ptr
        {
          return (this->dyn_deref_);
        }

        auto dereference::get_raw(void) const
          -> Dyninst::InstructionAPI::Dereference::Ptr
        {
          return (this->dyn_deref_);
        }

        auto dereference::set_raw(Dyninst::InstructionAPI::Dereference::Ptr)
        -> dereference const &
        {
          return (*this);
        }

        auto dereference::is_deref(void) const -> bool
        {
          return (true);
        }

      } // !namespace expressions
    } // !namespace instruction_API
  } // !namespace core
} // !namespace Barghest
