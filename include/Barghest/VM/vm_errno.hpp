// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#ifndef VM_ERRNO_HPP
# define VM_ERRNO_HPP

# include <string>
# include <map>
# include <algorithm>

# include "Barghest/VM/types.hpp"

namespace Barghest
{
  namespace VM
  {
    // extern  int                 err_no;
    // extern  std::string const   err_list[];
    // extern  int const           err_n;

    // auto    strerror(int err) -> std::string;
    // auto    perror(std::string const &msg) -> void;

    // // //! Default return value when a function success
    // // const int    success = true;
    // // //! Default return value when a function failed
    // // const int    failure = false;

    // # define VM_ERR_MAP_NAME errors_map
    // # define VM_ERR_OBJ_NAME err
    // template <typename T>
    // using err_map = const std::map<T, std::string const &>;

    // /*!
    // ** Manage error return values convention and messages.
    // ** It distinguish two things : return values from function and errors ID
    // ** * return values are boolean values (true/false, -1/0, etc) used for
    // **   quickly check the return value of a function
    // ** * Errors ID give the exact ID of the error.
    // ** Note: Errors ID and return values are independant (ex, if 1 is a success,
    // **       an errors ID of 1 can be an error (the corresponding ID would be 0))
    // ** Exemple:
    // ** int success = true;
    // ** int failure = false;
    // ** map
    // ** {
    // **   {0, "Success"}
    // ** }
    // ** INTERNAL NOTE: warning.
    // ** we can't use the following template since g++ Segfault with it.
    // ** Crash with: gcc version 4.8.3 20140911 (Red Hat 4.8.3-7) (GCC)
    // ** We have to work with the associated container instead of the key id.
    // **
    // ** template <class owner,
    // **        typename ERR_ID,
    // **        ERR_ID SUCCESS_ERR_ID, // optional template param
    // **        int SUCCESS = success, int FAILURE = failure>
    // */
    // template <class owner,
    //           typename ERR_MAP,
    //           typename ERR_MAP::key_type SUCCESS_ERR_ID>
    // class vm_errno
    // {
    // protected:
    //   friend owner;
    // public:

    //   /*!
    //   ** Construct an error management object.
    //   ** @param m The mapping between error ID and messages
    //   ** @param err_default_val The ID corresponding to a success.
    //   */
    //   vm_errno(ERR_MAP &m)
    //     : err_val_(SUCCESS_ERR_ID), err_map_(m), details_("")
    //   {
    //   }

    //   /*!
    //   ** Return a string with the last error that occured.
    //   ** The string returned do not include the carriage return.
    //   ** If the ID is not found, assert.
    //   */
    //   auto  errmsg(void) const -> std::string
    //   {
    //     if (details_.size() == 0)
    //     {
    //       try
    //       {
    //         std::string str = err_map_.at(err_val_);
    //         return (str);
    //       }
    //       catch (std::out_of_range &e)
    //       {
    //         assert(0 && "Unable to find the corresponding string.");
    //         return ("");
    //       }
    //     }
    //     else
    //     {
    //       return (details_);
    //     }
    //   }

    //   /*!
    //   ** Return the last errcode set.
    //   */
    //   auto  errcode(void) const -> typename ERR_MAP::key_type
    //   {
    //     return (err_val_);
    //   }

    //   /*!
    //   ** Reset the error ID to success.
    //   */
    //   auto  reset(void) -> void
    //   {
    //     this->err_val_ = SUCCESS_ERR_ID;
    //   }

    // protected:

    //   /*!
    //   ** Set an error code.
    //   ** @param err_val The ID of the error
    //   ** @param details override the string in the private map.
    //   */
    //   auto  set(typename ERR_MAP::key_type err_val,
    //             std::string const &details = "") -> void
    //   {
    //     this->err_val_ = err_val;
    //     this->details_ = details;
    //   }

    // private:
    //   typename ERR_MAP::key_type           err_val_;
    //   ERR_MAP          &err_map_;
    //   std::string       details_;

    // };
  } // !namespace VM
} // !namespace Barghest

#endif /* VM_ERRNO_HPP */
