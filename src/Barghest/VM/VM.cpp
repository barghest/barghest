// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#include <exception>
#include "Barghest/VM/VM.hpp"

#include <boost/graph/graphviz.hpp> // to print the graph
#include "Barghest/VM/static_part/parse/parse.hpp"

namespace Barghest
{
  namespace VM
  {
    decltype(last_err)        last_err;
    decltype(last_err_msg)    last_err_msg;

    VM::VM()
    {
      this->capstone_handle_ = 0;
    }

    VM::VM(std::string const &binaryPath)
    {
      this->capstone_handle_ = 0;
    }

    VM::~VM()
    {
      for (auto insn : this->insns_) // free capstone allocated instruction
        cs_free(insn, 1);
      this->insns_.clear();
      cs_close(&this->capstone_handle_); // Finally close capstone handle
    }

    auto    VM::init_capstone_() -> bool
    {
      int       ret;
      // void      *mem;
      // size_t    mem_size;
      std::string const fct = "VM::init_capstone_(): ";

      if ((ret = cs_open(CS_ARCH_X86, CS_MODE_64, &this->capstone_handle_))
          == CS_ERR_OK)
      {
        // mem = nullptr;
        // if (this->sym_.map_section(".text", mem, mem_size) == 0)
        // {
        cs_option(this->capstone_handle_, CS_OPT_DETAIL, CS_OPT_ON);
        cs_option(this->capstone_handle_, CS_OPT_SYNTAX, CS_OPT_SYNTAX_INTEL);
        return (true);
        // }
        // else
        //   std::cerr << fct << "Unable to map .text section" << std::endl;
      }
      else
        std::cerr << fct << "Unable to init capstone, error: " << ret << std::endl;
      return (false);
    }

    auto    VM::extract_basic_block(void) -> void
    {
    }

    /*
    ** Loop over all the instructions of the code
    ** When an instruction is found
    */
    auto    VM::first_pass_create_basic_bloc_(void) -> void
    {
      unsigned int                              cpt;
      std::map<off_t, vertex_descriptor>        pending;
      // std::set<std::pair<off_t, off_t> >        edges;
      vertex_descriptor                         v;

      cs_x86 const *x86;

      assert(this->insns_.size() > 0 && "Trying to create a graph with no instructions");
      v = boost::add_vertex(graph_);
      graph_[v].set_start(this->insns_[0]->address);
      for (cpt = 0 ; cpt < this->insns_.size() ; ++cpt)
      {
        cs_insn const   *insn = this->insns_[cpt];

        x86 = &(insn->detail->x86);
        printf("0x%x:\t%s\t%s\n",
               insn->address,
               insn->mnemonic,
               insn->op_str);
        if (insn->detail != nullptr)
        {
          auto  group = insn->detail->groups[0];
          if (group == CS_GRP_JUMP || group == CS_GRP_CALL
              || group == CS_GRP_RET)
          {
            graph_[v].set_end(insn->address + insn->size);
            if (group == CS_GRP_CALL || group == CS_GRP_JUMP)
            {
              if (x86->op_count == 1 && x86->operands[0].type == X86_OP_IMM)
              {
                pending[x86->operands[0].imm] = boost::add_vertex(graph_);
                boost::add_edge(v, pending[x86->operands[0].imm], graph_);
              }
            }
            if (cpt + 1 < this->insns_.size())
            {
              if (pending.find(this->insns_[cpt + 1]->address) == pending.end())
              {
                vertex_descriptor new_v = boost::add_vertex(graph_);
                // Go to start if ret not found (used as a sink for now)
                graph_[new_v].set_start(this->insns_[0]->address);
                boost::add_edge(v, new_v, graph_);
                v = new_v;
              }
              else
              {
                vertex_descriptor new_v = pending.find(this->insns_[cpt + 1]->address)->second;
                boost::add_edge(v, new_v, graph_);
                v = new_v;
              }
            }

            printf("--> Jump found\n");
          }
          // else if (this->inst_list_[cpt].detail->groups[0] == CS_GRP_CALL)
          // {
          //   if (x86->op_count == 1 && x86->operands[0].type == X86_OP_IMM)
          //     edges.insert(std::make_pair(this->inst_list_[cpt].address,
          //                                    x86->operands[0].imm));
          //   else
          //     edges.insert(std::make_pair(this->inst_list_[cpt].address, 0));
          //   printf("--> Call found\n");
          // }
          // else if (this->inst_list_[cpt].detail->groups[0] == CS_GRP_RET)
          // {
          //   edges.insert(std::make_pair(this->inst_list_[cpt].address, 0));
          //   printf("--> Ret found\n");
          // }
        }
      }
      // off_t     b_start = 0;
      // off_t     b_end = 0;
      // vertex_descriptor v;
      // std::map<off_t, vertex_descriptor>    tmp;
      // for (auto const &edge : edges)
      // {
      //   tmp[edge.first] = boost::add_vertex(graph_);
      //   graph_[boost::add_vertex(graph_)].set(edge.second, 0);
      // }
      boost::write_graphviz(std::cerr, graph_);
    }

    auto    VM::init_boost_graph_(void) -> void
    {
      // static_part::parse::disassemble_linear_dumb<std::vector>
      //   d(this->insns_, this->sym_, this->capstone_handle_);

      // d.run();


      // Barghest::VM::VM::BGLGraph g1; // entry point, filled later
      // boost::add_vertex(g1);
      // first_pass_create_basic_bloc_();
      // VM::static_part::parse::disassemble_linear_dumb<std::vector<cs_insn> >::run();
    }

    auto    VM::parse(std::string const &binaryPath) -> BGLGraph const &
    {
      std::string const fct = "VM::parse(): ";

      try
      {
        if (this->init_capstone_())
        {
          // std::vector<static_part::symtab::mapped_region>  mr;

          this->sym_.open(binaryPath);
          // this->sym_.get_mapped_region(mr);
          // for (auto &region : mr)
          // {
          //   size_t      j;
          //   size_t      count;
          //   cs_insn     *insn;

          //   std::cout << region.name << std::endl;
          //   if (region.name == ".text")
          //   {

          static_part::parse::disassemble_linear_dumb<std::vector>   d(this->insns_, this->sym_, this->capstone_handle_);

          d.run();

          Barghest::VM::VM::BGLGraph g1; // entry point, filled later
          boost::add_vertex(g1);
          first_pass_create_basic_bloc_();


          // count = cs_disasm(this->capstone_handle_, (const std::uint8_t *)region.mem, region.size, 0x1000, 0, &insn);
          // for (j = 0 ; j < count ; ++j)
          // {
          //   printf("0x%lx: %s %s\n", insn[j].address, insn[j].mnemonic, insn[j].op_str);
          // }
          // cs_free(insn, count);
          //   }
          //   // this->init_boost_graph_();
          // }
        }

      }
      catch (std::exception &e)
      {
        std::cout << "[==> " << e.what() << std::endl;
      }

      // if (this->sym_.open(binaryPath))
      // {
      //   if (this->init_capstone_())
      //     this->init_boost_graph_();
      // }
      // else
      //   std::cerr << this->sym_.VM_ERR_OBJ_NAME.errmsg() << std::endl;
    }

    auto    VM::entry_point(void) const -> vertex_descriptor
    {
    }

    // template <template <typename, typename ...ARGS> class I>
    // auto    VM::insns_to_graph(I const &insns, BGLGraph &g) const -> void
    // {
    // }

    auto    VM::get_BGL_graph(void) const -> BGLGraph const &
    {
    }
  } // !namespace VM
} // !namespace Barghest
