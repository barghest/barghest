// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#ifndef SYMTAB_ELF_HPP
# define SYMTAB_ELF_HPP

// elf lib
# include <libelf.h>
# include <gelf.h>

# include <string>

# include "Barghest/VM/static_part/symtab/symtab.hpp"

namespace Barghest
{
  namespace VM
  {
    namespace static_part
    {
      namespace symtab
      {
        namespace elf
        {
          // List of errors used by symtab_elf
          enum class  err : int
          {
            SUCCESS = 0,
              OPEN,             // Failed to open the file
            //
              NO_INIT,          // No file opened (but needed)
            //
              ELF_VERSION,      // libelf function failure
              ELF_BEGIN,        //           ---
              GELF_GETCLASS,    //           ---
              GELF_GETEHDR,     //           ---
            //
              MUNMAP,           // munmap() failure
            //
              MAX, // Number of item in the enum
              };

          // extern err                            last_err;
          // extern std::string                    last_err_msg;
          // std::map<err, std::string> const      err_list =
          // {
          //   {err::SUCCESS,        "Success"},
          //   {err::OPEN,           "Unable to open the given file"},
          //   {err::ELF_VERSION,    "elf_version() failed"},
          //   {err::ELF_BEGIN,      "elf_begin() failed"},
          //   {err::GELF_GETCLASS,  "gelf_getclass() failed"},
          //   {err::GELF_GETEHDR,   "gelf_getehdr failed"},
          //   {err::MUNMAP,         "munmap() failed"},
          // };

          // enum class err_id : int
          // {
          //     };

          // // List of errors ID for symtab
          // namespace err_id_elf
          // {
          //   enum
          //   {
          //     err = err_id::max,
          //     max,
          //   };
          // } // !namespace err_id
          // //

          // static const std::map<int, std::string const &> errors_elf =
          // {
          //   {err_id::success, "Success"}
          // };

          class symtab_elf : public symtab
          {
          public:
            symtab_elf(std::string const &file_name = "");
            ~symtab_elf();

            virtual auto    open(std::string const &file_name) -> void;
            virtual auto    close(void) -> void;

            virtual auto    get_entry_point(size_t &entry) const -> bool;
            virtual auto    get_file_name(void) const -> std::string const &;
            // virtual auto    map_section(std::string const &section_name,
            //                             void *&memory,
            //                             size_t &size) -> bool;
            // virtual auto    get_fct_addr(void) const -> std::vector<intptr_t>;

            virtual auto    get_mapped_region(std::vector<mapped_region> &mr) const -> void;

            /*!
            ** Free the memory allocated previously and clear the allocator.
            */
            virtual auto    free_mapped_region(std::vector<mapped_region> &mr) const -> void;

          private:
            std::string file_name_;
            int         fd_;        /// File descriptor
            Elf         *e_;        /// Elf object
            Elf_Kind    ek_;        /// Elf type (AR, ELF, NONE, etc)
            GElf_Ehdr   ehdr_;      /// Elf Executable HeaDeR
            int         elf_class_;

          };

        } // !namespace elf
      } // !namespace symtab
    } // !namespace static_part
  } // !namespace VM
} // !namespace Barghest

#endif /* !SYMTAB_ELF_HPP */
