#ifndef BARGHEST_BINARY_FUNCTION_HH__
# define BARGHEST_BINARY_FUNCTION_HH__

# include <string>

# include <dyninst/BinaryFunction.h>

# include "Barghest/core/instruction_API/expression.hh"

namespace Barghest
{
  namespace core
  {
    namespace instruction_API
    {
      namespace expressions
      {
	class binary_function : public expression
	{
	public:
	  binary_function(Dyninst::InstructionAPI::Expression::Ptr expr);
	  virtual ~binary_function() = default;

	  virtual auto format(void) const -> std::string;
	  virtual auto get_raw_expr(void) const
	    -> Dyninst::InstructionAPI::Expression::Ptr;
	  virtual auto get_raw(void) const
	    -> Dyninst::InstructionAPI::BinaryFunction::Ptr;
	  virtual auto set_raw(Dyninst::InstructionAPI::BinaryFunction::Ptr deref)
	    -> binary_function const &;

	  /*!
	  ** @See "instruction_API/expression.hpp" for documentation about this
	  ** function.
	  */
	  virtual auto is_bin_func(void) const -> bool;

	private:
	  Dyninst::InstructionAPI::BinaryFunction::Ptr bin_func_;
	}; // !namespace binary_function
      } // !namespace expressions
    } // !namespace instruction_API
  } // !namespace core
} // !namespace Barghest

#endif /* !BARGHEST_BINARY_FUNCTION_HH__ */
