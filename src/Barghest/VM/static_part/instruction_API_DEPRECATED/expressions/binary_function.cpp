// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#include <cassert>

#include <boost/pointer_cast.hpp>

#include "Barghest/core/instruction_API/expressions/binary_function.hh"

namespace Barghest
{
  namespace core
  {
    namespace instruction_API
    {
      namespace expressions
      {
        binary_function::binary_function(Dyninst::InstructionAPI::Expression::Ptr expr)
          : expression(expression::BINARY_FUNCTION)
        {
          auto bin_func = boost::dynamic_pointer_cast<Dyninst::InstructionAPI::BinaryFunction>(expr);
          assert(bin_func);
          set_raw(bin_func);
        }

        auto binary_function::format(void) const -> std::string
        {
          return get_raw()->format();
        }

        auto binary_function::get_raw_expr(void) const
          -> Dyninst::InstructionAPI::Expression::Ptr
        {
          return bin_func_;
        }

        auto binary_function::get_raw(void) const
          -> Dyninst::InstructionAPI::BinaryFunction::Ptr
        {
          return bin_func_;
        }

        auto binary_function::set_raw(Dyninst::InstructionAPI::BinaryFunction::Ptr)
        -> binary_function const &
        {
          return *this;
        }

        auto binary_function::is_bin_func(void) const -> bool
        {
          return true;
        }

      } // !namespace expressions
    } // !namespace instruction_API
  } // !namespace core
} // !namespace Barghest
