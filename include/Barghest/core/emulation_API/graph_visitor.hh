#ifndef BARGHEST_GRAPH_VISITOR_HH__
# define BARGHEST_GRAPH_VISITOR_HH__

# include <list>	// std::list
// # include "BarghestGraph.hh"
// # include "BarghestContext.hh"

# include "Barghest/core/emulation_API/context.hh"
# include "Barghest/core/graph.hpp"

namespace Barghest
{
  namespace core
  {
    namespace emulation_API
    {

      class graph_visitor
      {
      private:
	context			context_;

	std::list<context>	context_stack_;

	graph const		&g_;
	// Graph const		&g_;
	// protected:
      // private:
      // 	dfs_events		visitor_;
	// bgl_visitor_		boost_visitor_;

	// typedef typename boost::property_map<Barghest::core::parse_API::graph,
	// 				     boost::vertex_color_t>::type t_color_map;

	// t_color_map	color_map_;
	// auto	get_barghest_graph(void) -> graph &;

      protected:
	auto	get_barghest_graph(void) const -> graph const &;
      public:
	graph_visitor(Barghest::core::graph const &g);
	~graph_visitor();

	auto	start(void) -> void;
	auto	stop(void) -> void;

	/*!
	** Used for context recovery when we move back on the recursion on the tree.
	** TODO: utiliser le mot clef friend pour que la class fille n'y ai pas acces.
	**       (just check_nodes/starting_node/etc)
	*/
	auto	push_context(void) -> void;
	auto	pop_context(void) -> void;
	auto	get_context(void) -> context &;

	// virtual auto	onInstructions(std::set<Instruction>) -> void; // Filtre
	virtual auto	on_instruction(std::pair<size_t,
				      instruction_API::instruction const &> inst) -> void;
	virtual auto	on_back_edge(void) -> void;
      }; // !class graph_visitor

    } // !namespace emulation_API
  } // !namespace core
} // !namespace Barghest

#endif /* !BARGHEST_GRAPH_VISITOR_HH__ */
