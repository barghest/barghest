#include <string>

#include <dyninst/Immediate.h>

#include "Barghest/core/instruction_API/expression.hh"

namespace Barghest
{
  namespace core
  {
    namespace instruction_API
    {
      namespace expressions
      {
	class immediate : public expression
	{
	public:
	  immediate(Dyninst::InstructionAPI::Expression::Ptr expr);
	  virtual ~immediate() = default;

	  virtual auto format(void) const -> std::string;
	  virtual auto get_raw_expr(void) const
	    -> Dyninst::InstructionAPI::Expression::Ptr;
	  virtual auto get_raw(void) const
	    -> Dyninst::InstructionAPI::Immediate::Ptr;
	  virtual auto set_raw(Dyninst::InstructionAPI::Immediate::Ptr)
	    -> immediate const &;

	  template <typename T>
	  auto get_value(void) const -> T
	  {
	    return (this->dyn_imm_->eval().convert<T>());
	  }

	  /*!
	  ** @See "instruction_API/expression.hpp" for documentation about this
	  ** function.
	  */
	  virtual auto is_imm(void) const -> bool;

	private:
	  Dyninst::InstructionAPI::Immediate::Ptr	dyn_imm_;

	}; // !class immediate
      } // !namespace expressions
    } // !namespace instruction_API
  } // !namespace core
} // !namespace Barghest
