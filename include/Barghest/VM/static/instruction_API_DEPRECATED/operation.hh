#ifndef BARGHEST_OPERATION_HH__
# define BARGHEST_OPERATION_HH__

# include <string>

# include <dyninst/Instruction.h>
# include <dyninst/Operation.h>
# include <dyninst/entryIDs.h>

namespace Barghest
{
  namespace core
  {
    namespace instruction_API
    {
      /*!
      ** A typedef to wrap the enum declared in global in entryIDs.h (Dyninst Include)
      ** Ref:
      ** enum entryID {
      ** e_jb = 0,
      ** e_jb_jnaej_j,
      ** e_jbe,
      ** e_jcxz_jec,
      ** ...
      **   _entry_ids_max_
      ** };
      **
      ** enum prefixEntryID {
      **  prefix_none,
      **  prefix_rep,
      **  prefix_repnz
      ** };
      */
      // TODO: move these in a specific instruction_API.hh header
      using inst_ID = enum entryID;
      using inst_prefix_ID = enum prefixEntryID;

      /*!
      ** This class is not intended to be created directly by the user.
      ** TODO : then explain what may create this class.
      */
      class operation
      {
      public:
	/*!
	** We construct the object on an instruction and not an operation because
	** dyninst only give us a const ref, and would denied us to copy/mov
	** this class with operator=().
	*/
	operation(Dyninst::InstructionAPI::Instruction::Ptr instruction);

	/*!
	** Returns the mnemonic for the operation.  Like \c instruction::format.
	*/
	auto format(void) const -> std::string;

	/*!
	** Returns the entry ID corresponding to this operation.  Entry IDs are
	** enumerated values that correspond
	** to assembly mnemonics. (Ex: e_jmp/e_call/e_add)
	*/
	auto get_ID(void) const -> inst_ID;

	/*!
	** Returns the prefix entry ID corresponding to this operation, if any.
	** Prefix IDs are enumerated values that correspond to assembly prefix mnemonics.
	*/
	auto get_prefix_ID(void) const -> inst_prefix_ID;

	/*!
	** Return a reference on the Dyninst Object wrapped.
	*/
	auto get_raw(void) const -> Dyninst::InstructionAPI::Operation const &;
      private:
	Dyninst::InstructionAPI::Instruction::Ptr inst_;

      }; // !class operation
    } // !namespace instruction_API
  } // !namespace core
} // !namespace Barghest

#endif /* !BARGHEST_OPERATION_HH__ */
