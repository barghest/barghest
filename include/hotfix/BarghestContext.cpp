// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#include <boost/log/trivial.hpp>
#include "BarghestContext.hh"

using namespace bitField;
using namespace bitField::BitFieldArithmetic;

/* TODO: retirer ces anciens defines des que tout sera remis au propre */

/*!
** Assume the operand is not an empty one
*/
# define _GET_OPERAND_EXPR_TYPEID(op)                                   \
  (typeid(*((static_cast<Dyninst::InstructionAPI::Operand const &>(op)).getValue())))
# define IS_OPERAND_REG(op)     (_GET_OPERAND_EXPR_TYPEID(op) == typeid(Dyninst::InstructionAPI::RegisterAST))
# define IS_OPERAND_DEREF(op)   (_GET_OPERAND_EXPR_TYPEID(op) == typeid(Dyninst::InstructionAPI::Dereference))
# define IS_OPERAND_IMM(op)     (_GET_OPERAND_EXPR_TYPEID(op) == typeid(Dyninst::InstructionAPI::Immediate))

/* Return the base register of a Dyninst::MachRegister (Ex: BASEA, BASEDI, etc) */
/* (reg & 0111b) (0x0 -> 0xf) */
/* Cf: /usr/include/dyn_regs.h */
# define GET_BASE_MACH_REGISTER(reg)    ((static_cast<Dyninst::MachRegister const &>(reg)).val() & 0xff)
# define GET_MACH_REGISTER_CATEGORY(reg)    ((static_cast<Dyninst::MachRegister const &>(reg)).val() & 0xff0000)

/* Return the 'class MachRegister' associated with a RegisterAST object (here more to reduce define size) */
# define GET_MACH_REGISTER(reg) ((static_cast<Dyninst::InstructionAPI::RegisterAST const &>(reg)).getID())
/* Return the base register of a Dyninst::InstructionAPI::RegisterAST::Ptr */
# define GET_BASE_REGISTER(reg) (GET_BASE_MACH_REGISTER((GET_MACH_REGISTER(reg))))
/* Return true if the machRegister correspond to a GeneralPurposeRegister */
# define IS_GPR_REGISTER(reg) (GET_MACH_REGISTER_CATEGORY((GET_MACH_REGISTER(reg))) == Dyninst::x86::GPR)

/*!
** Convert the value of a Dyninst::InstructionAPI::Immediate into a size_t
*/
# define GET_IMM_VAL(imm)                                               \
  ((static_cast<Dyninst::InstructionAPI::Immediate const &>(imm)).eval().convert<size_t>())

/*!
** Check the register subrange.
** Ex: in 64b:
** * rax is a FULL register
** * eax is a DW (Double Word) register
** * ax is a W (Word) register
** * al is a L (Low) register
** * ah is a H (High) register
** Cf: DyninstAPI-8.1.2/common/src/dyn_regs.C and /usr/include/dyn_regs.h
** Ps: value of x86::FULL and x86_64::FULL are the same, but need to find a more clean way to do.
*/
# define GET_REGISTER_RAW_VAL(reg) (GET_MACH_REGISTER(reg).val())
# define IS_REGISTER_FULL(reg)    ((GET_REGISTER_RAW_VAL(reg) & 0x0000ff00) == Dyninst::x86::FULL)
# define IS_REGISTER_L(reg)       ((GET_REGISTER_RAW_VAL(reg) & 0x0000ff00) == Dyninst::x86::L_REG)
# define IS_REGISTER_H(reg)       ((GET_REGISTER_RAW_VAL(reg) & 0x0000ff00) == Dyninst::x86::H_REG)
# define IS_REGISTER_W(reg)       ((GET_REGISTER_RAW_VAL(reg) & 0x0000ff00) == Dyninst::x86::W_REG)
# define IS_REGISTER_DW(reg)      ((GET_REGISTER_RAW_VAL(reg) & 0x0000ff00) == Dyninst::x86_64::D_REG)

  namespace Barghest
  {
    // TODO: Pour les saves context, faire une list de changement a restore (comme git/emacs)
    Context::Context()
    {
      BOOST_LOG_TRIVIAL(debug) << "Context::" << __func__ << "()";
      size_t    i;
      auto    it = this->_getRegisters().begin();
      BitFieldArithmetic<size_t>    init_value;

      i = -1;
      while (it != this->_getRegisters().end())
      {
        init_value.setMem(&(*it)).setAllToUndefined();
        it++;
      }
      i = -1;
      while (++i < (sizeof(_fct) / sizeof(*_fct)))
        _fct[i] = &Context::_unknown_inst;
      _fct[::e_mov] = &Context::_mov_emul;
      _fct[::e_add] = &Context::_add_emul;
      _fct[::e_sub] = &Context::_sub_emul;
      _fct[::e_shl_sal] = &Context::_shl_sal_emul;
      BOOST_LOG_TRIVIAL(trace) << "Context::" << __func__ << "() END";
      /* TODO: A RETIRER: PAtch pour le forum eip pour l'unsigned */
      // _fct[::e_sar] = &Context::_shl_sal_emul;
    }

    Context::~Context()
    {
      BOOST_LOG_TRIVIAL(debug) << "Context::" << __func__ << "()";
      // std::cout << "Context destructor called" << std::endl;
    }

    Context::Context(Context const &other)
    {
      BOOST_LOG_TRIVIAL(debug) << "Context::" << __func__ << "(Context const &other)";
      (void)this->operator=(other);
    }

    auto    Context::_copyTrace(t_trace const &other_trace) -> void
    {
      BOOST_LOG_TRIVIAL(trace) << "Context::" << __func__ << "(t_trace const &other_trace)";
      auto    it = other_trace.begin();

      this->_getTrace().clear(); // In order to be sure
      for (; it != other_trace.end() ; it++)
      {
        // this->_getTrace().push_back(std::make_pair((*it).first, (*it).second));
        this->_getTrace().push_back((*it));
      }
    }

    /*
    ** Internal notes:
    ** We do not use vector::operator=() because it won't know how to make a copy
    ** of the reference in the std::pair (and we not not want to make full copy of
    ** InstructionAPI::Instruction class.). Instead we perform the copy by hand
    ** element after element.
    */
    auto    Context::operator=(Context const &other) -> Context &
    {
      BOOST_LOG_TRIVIAL(debug) << "Context::" << __func__ << "(Context const &other)";
      // this->_getTrace().clear();
      this->_copyTrace(other.getTrace());
      this->_getRegisters() = other.getRegisters();
#if 0
      // std::cout << "Clear ?" << std::endl;
      // _trace = other.getTrace();
      // std::cout << "Clear done" << std::endl;
      for (auto pair = other.getTrace().begin() ; pair != other.getTrace().end() ; pair++)
      {
        // std::cout << "push_back" << std::endl;
        _trace.push_back(*pair);
        // std::cout << "First: " << pair.first << std::endl;
        // _trace.push_back(std::make_pair(pair.first, pair.second));
        // std::cout << "Second: " << pair.second.format() << std::endl;
      }
      for (auto base_bitfield = other.getRegisters().begin() ;
           base_bitfield != other.getRegisters().end() ; base_bitfield++)
        _registers[(*base_bitfield).first] = (*base_bitfield).second;
      // std::cout << "end" << std::endl;
#endif /* 0 */
      BOOST_LOG_TRIVIAL(trace) << "Context::" << __func__ << "(Context const &other) END";
      return (*this);
    }

    auto    Context::_getTrace(void) -> t_trace &
    {
      return (this->_trace);
    }

    auto    Context::_getRegisters(void) -> t_reg &
    {
      return (this->_registers);
    }

    auto    Context::getTrace(void) const -> t_trace const &
    {
      return (this->_trace);
    }

    auto    Context::getRegisters(void) const -> t_reg const &
    {
      return (this->_registers);
    }

    auto    Context::emulate(std::pair<size_t, InstructionAPI::Instruction const &> inst) -> void
    {
      BOOST_LOG_TRIVIAL(debug) << "Context::" << __func__ << "(std::pair<size_t, InstructionAPI::Instruction const &> inst) -> " << inst.second.format();
      // this->_getTrace().push_back(std::make_pair(offset, b.getInsn(offset)));
      // std::cout << "Emulated: " << inst.second.format() << std::endl;
      this->_getTrace().push_back(inst);
      (this->*(_fct[inst.second.getOperation().getID()]))(inst.first, inst.second,
                                                          inst.second.getOperands());
      BOOST_LOG_TRIVIAL(trace) << "Context::" << __func__ << "(std::pair<size_t, InstructionAPI::Instruction const &> inst) END";
    }

    auto    Context::printTrace(std::ostream& stream) const -> void
    {
      auto    flags_backup = stream.flags();
      for (auto inst = this->getTrace().begin() ; inst != this->getTrace().end() ; inst++)
      {
        stream << std::hex << std::showbase << (*inst).first << ": "
               << ((*inst).second).format() << std::endl;
      }
      stream << std::endl;
      stream.setf(flags_backup);
    }

# define __BARGHESTCONTEXT_INST_INCLUDE
#  include "_BarghestContext_unknown_inst.cpp"
#  include "_BarghestContext_add_emul.cpp"
#  include "_BarghestContext_sub_emul.cpp"
#  include "_BarghestContext_mov_emul.cpp"
#  include "_BarghestContext_shl_sal_emul.cpp"
# undef __BARGHESTCONTEXT_INST_INCLUDE
/**/
  } // ! namespace Barghest
