
#ifndef BARGHEST_OPERAND_HH__
# define BARGHEST_OPERAND_HH__

#include <string>		// std::sring
#include <dyninst/Operand.h>

#include "Barghest/core/instruction_API/expression.hh"

namespace Barghest
{
  namespace core
  {
    namespace instruction_API
    {
      class operand
      {
      public:

	/*!
	** @param dyn_operand The raw operand from Dyninst we are wrapping.
	** @param arch The binary architecture associated with this operand,
	**             (as dyninst operands do not record their architectures,
	**             we do not want the user to provide it by hand. However we
	**             construct the object from
	**             Dyninst::InstructionAPI::Instruction which have
	**             this information).
	*/
	operand(Dyninst::InstructionAPI::Operand const &dyn_operand,
		Dyninst::Architecture arch);
	operand(operand const &other) = delete;
	operand(operand &&other);
	// operand(operand const &&other) = default;
	auto operator=(const operand &) -> operand & = default;

	~operand();

	/// Returns true if this operand is read
	auto is_read(void) const -> bool;
	/// Returns true if this operand is written
	auto is_written(void) const -> bool;
	/// Returns true if this operand reads memory
	auto reads_memory(void) const -> bool;
	/// Returns true if this operand writes memory
	auto writes_memory(void) const -> bool;
	/*!
	** Return a printable string representation of the operand.
	** The optional addr parameter specifies the value of the program counter.
	** TODO: ajouter un exemple de avec et sans addr
	** @return The operand in a disassembly format
	*/
	auto format(size_t addr = 0) const -> std::string;

	/*
	** Return the Barghest expression of this operand.
	*/
	auto get_expr(void) const -> expression const &;

	/*!
	** Return a reference on the Dyninst Object wrapped.
	*/
	auto get_raw(void) const -> Dyninst::InstructionAPI::Operand const &;
	auto get_raw_expr(void) const -> Dyninst::InstructionAPI::Expression const &;

      private:
	/*!
	** The raw Dyninst Operand class.
	*/
	Dyninst::InstructionAPI::Operand const &op_;

	/*!
	** The binary archecture we are working on {Arch_x86, Arch_x86_64, Arch_ppc32, Arch_ppc64}.
	*/
	Dyninst::Architecture	arch_;
	expression		*expr_;

	auto init_expr(Dyninst::InstructionAPI::Operand const
		       &dyn_operand) const -> expression *;
	auto get_arch(void) const -> Dyninst::Architecture;

      }; // !class operand
    } // !namespace instruction_API
  } // !namespace core
} // !namespace Barghest

#endif /* !BARGHEST_OPERAND_HH__ */
