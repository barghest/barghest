// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#include <chrono>
#include <thread>
#include <ratio>

#include "Barghest/core/core.hpp"
#include "Barghest/front_end/front_end.hpp"

#include "Barghest/core/tasks/task.hpp"
#include "Barghest/core/tasks/parse_file.hpp"

namespace Barghest
{
  namespace core
  {
    namespace Internal
    {
      template <class Period>
      using Interval = std::chrono::duration<std::intmax_t, Period>;

      template <class Period, typename Operation, typename Predicate, typename ...Args>
      static auto cycle(Operation operation, Predicate predicate, Args&&... args)
        -> decltype(operation(std::forward<Args>(args)...))
      {
        decltype(operation(std::forward<Args>(args)...)) ret;
        auto lastPoint = std::chrono::steady_clock::now();
        do
        {
          ret = operation(std::forward<Args>(args)...);
          auto now = std::chrono::steady_clock::now();
          auto cycleDuration = now - lastPoint;
          if (std::chrono::duration_cast<Interval<Period> >(cycleDuration).count() < 0)
            std::this_thread::sleep_for(Interval<Period>() - cycleDuration);
          lastPoint = now;
        } while(predicate());
        return ret;
      }
    }

    template <typename Ratio, typename Operation, typename Predicate,
              typename ...Args>
    static auto cycleRatio(Ratio ratio, Operation operation, Predicate predicate,
                           Args&&... args)
      -> decltype(operation(std::forward<Args>(args)...))
    {
      return Internal::cycle<ratio>(operation, predicate,
                                    std::forward<Args>(args)...);
    }

    template <typename Operation, typename Predicate, typename ...Args>
    static auto cycle(Operation operation, Predicate predicate, Args&&... args)
      -> decltype(operation(std::forward<Args>(args)...))
    {
      return Internal::cycle<std::ratio<1, 1> >(operation, predicate,
                                                std::forward<Args>(args)...);
    }

    static auto process_task(Barghest::core::tasks::task const& task) -> bool
    {
      if (task.get_type() == Barghest::core::tasks::type_t::parse_file)
      {
        auto parse_task = *reinterpret_cast<Barghest::core::tasks::parse_file const *>(&task);
        static_cast<void>(parse_task.get_path());
      }
      return true;
    }

    static auto loop(Barghest::front_end::front_end &fe,
                     Barghest::core::task_queue &queue) -> bool
    {
      fe.start();
      cycle([&fe, &queue]()
            {
              fe.process_events();
              if (!queue.empty())
              {
                process_task(queue.front());
                queue.pop();
              }
              return true;
            }, [&fe]() { return fe.is_running(); });
      // while (fe.is_running())
      //   {
      //     fe.process_events();
      //     if (!queue.empty())
      //       {
      //         process_task(queue.front());
      //         queue.pop();
      //       }
      //     // todo : add a timer here
      //   }
      return true;
    }

    core::~core()
    {
      delete fe_;
    }

    auto core::run() -> int
    {
      return fe_->initialize() && loop(*fe_, queue_) ? 0 : 1;
    }

  }
}
