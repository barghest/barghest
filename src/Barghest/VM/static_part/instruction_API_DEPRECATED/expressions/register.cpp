// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#include <cassert>
#include <string>
#include <stdexcept>

#include <boost/pointer_cast.hpp>
#include <boost/log/trivial.hpp>

#include "Barghest/core/dyninst_helper.hpp"
#include "Barghest/core/instruction_API/expressions/register.hh"

namespace Barghest
{
  namespace core
  {
    namespace instruction_API
    {
      namespace expressions
      {
        register_t::register_t(Dyninst::InstructionAPI::Expression::Ptr reg)
          : expression(expression::REGISTER)
        {
          auto r = boost::dynamic_pointer_cast<Dyninst::InstructionAPI::RegisterAST>(reg);
          assert(r);
          set_raw(r);
        }

        auto register_t::is_reg(void) const -> bool
        {
          return (true);
        }

        auto register_t::get_raw_expr(void) const
          -> Dyninst::InstructionAPI::Expression::Ptr
        {
          return (this->dyn_reg_);
        }

        auto register_t::get_raw(void) const
          -> Dyninst::InstructionAPI::RegisterAST::Ptr
        {
          return (this->dyn_reg_);
        }

        // TODO: use dyninst register table to speed up (no need to use a find())
        auto register_t::set_raw(Dyninst::InstructionAPI::RegisterAST::Ptr dyn_reg)
          -> register_t const &
        {
          this->dyn_reg_ = dyn_reg;
          if (dyn_reg)
          {
            auto it = g_regLinkTable.find(core::get_mach_register_val(dyn_reg));

            if (it != g_regLinkTable.end())
            {
              std::cout << "=> Setting reg (barghest)[" << this << " " << (*it).second << "]"
                        << "(dyninst)[" << dyn_reg << "]" << std::endl;
              this->set_ID((*it).second);
            }
            else // Error: registerId not found
            {
              unsigned int i;
              std::string err_msg;
              auto register_ID_it = g_regLinkTable.begin();

              i = 0;
              err_msg = "Register::setRaw(...): Error: Register '"
                + dyn_reg->format()
                + "' does not have any corresponding reference RegisterId. "
                + "The following RegisterId are available:\n";
              while (register_ID_it != g_regLinkTable.end())
              {
                err_msg += "RegisterId N'" + std::to_string(i++) + ": ";
                // err_msg += (RegisterIdIt == g_regLinkTable.begin() ? " " : ", ");
                // err_msg += (*RegisterIdIt).second.name;
                err_msg += (*register_ID_it).second.full_name;
                err_msg += "\n";
                ++register_ID_it;
              }
              BOOST_LOG_TRIVIAL(fatal) << err_msg;
              throw std::runtime_error (err_msg);
            }
          }
          else
            BOOST_LOG_TRIVIAL(warning) << "Register::" << __func__ << "(... dynReg) "
                                       << "-> param dynReg (a shared_ptr) is empty";
          return (*this);
        }

        auto register_t::set_ID(register_ID ID) -> register_t const &
        {
          this->ID_ = ID;
          return (*this);
        }

        auto register_t::get_ID(void) const -> register_ID const &
        {
          return (this->ID_);
        }

        auto register_t::format(void) const -> std::string
        {
          return (this->get_raw()->format());
        }

        auto register_t::get_raw_val(void) const -> signed int
        {
          return (core::get_mach_register(get_raw()).val());
        }

        auto register_t::get_base_mach_register(void) const -> signed int
        {
          // TODO 0xff is not clear. change it
          return (this->get_raw_val() & 0xff);
        }

        auto register_t::get_mach_register_aliasing(void) const -> signed int
        {
          // TODO 0xff00 is not clear. change it
          return (this->get_raw_val() & 0xff00);
        }

        auto register_t::get_mach_register_category(void) const -> signed int
        {
          // TODO 0xff0000 is not clear. change it
          return (this->get_raw_val() & 0xff0000);
        }

      } // !namespace expressions
    } // !namespace instruction_API
  } // !namespace core
} // !namespace Barghest
