// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#include "Barghest/VM/static_part/parse/block.hpp"

namespace Barghest
{
  namespace VM
  {
    namespace static_part
    {
      namespace parse
      {
        block::block(off_t start, off_t end)
          : start_(start), end_(end)
        {
          if (this->end_ > this->start_)
          {
            this->reset();
          }
        }

        block::~block()
        {
        }

        auto  block::start(void) const -> off_t
        {
          return (this->start_);
        }

        auto  block::first(void) const -> off_t
        {
          return (this->start_);
        }

        auto  block::end(void) const -> off_t
        {
          return (this->end_);
        }

        auto  block::last(void) const -> off_t
        {
          return (0); // not implemented yet
        }

        auto  block::set(off_t start, off_t end) -> void
        {
          this->start_ = start;
          this->end_ = end;
        }
        auto  block::set_end(off_t end) -> void
        {
          this->end_ = end;
        }
        auto  block::set_start(off_t start) -> void
        {
          this->start_ = start;
        }

        auto  block::reset(void) -> void
        {
          this->start_ = this->end_ = 0;
        }

        auto  block::size(void) const -> off_t
        {
          if (this->end_ > this->start_)
            return (this->end_ - this->start_);
          throw std::runtime_error("Internal error, 'end' attribute is lower than 'start'");
          return (0); // can't call reset since we are const
        }


      } // !namespace parse
    } // !namespace static_part
  } // !namespace VM
} // !namespace Barghest
