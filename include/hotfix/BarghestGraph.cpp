// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#include <map>        // std::map<>
#include <cstring>    // strdup()
#include <boost/graph/graphviz.hpp>    // TODO: a retirer (pour debug)
#include <boost/log/trivial.hpp>    // boost log
#include "hotfix/BarghestGraph.hh"

namespace Barghest
{
  // namespace Graph
  // {
  // static void    _completeDyninstGraph(Dyninst::ParseAPI::CodeObject *co,
  //                       Dyninst::ParseAPI::CodeObject::funclist const &all,
  //                       Dyninst::Offset addr)
  // {
  //   typedef std::map<Dyninst::Offset, Dyninst::InstructionAPI::Instruction::Ptr> Insns;
  //   Insns    insns;
  //   Dyninst::Offset    max;
  //   static std::set<Dyninst::Offset> viewedBlock;

  //   // void getInsns(Insns &insns) const;

  //   max = 0;
  //   if (viewedBlock.find(addr) != viewedBlock.end())
  //     return ;
  //   std::cout << "Parsing adresse " << addr << std::endl;
  //   viewedBlock.insert(addr);
  //   co->parse(addr, true);
  //   for (auto func : all)
  //   {
  //     for (auto block : func->blocks())
  //     {
  //       insns.clear();
  //       block->getInsns(insns);
  //       for (auto inst : insns)
  //       {
  //         if (inst.first > max)
  //           max = inst.first + (inst.second)->size();
  //         // std::cout << std::hex << std::showbase << inst.first << ": "
  //         //         << (inst.second)->format() << std::endl;
  //       }
  //       _completeDyninstGraph(co, co->funcs(), max);
  //     }
  //   }
  // }

  Graph::Graph()
    : /* _binary(""), _graph(0), */ _sts(nullptr), _co(nullptr)
  {
  }

  Graph::Graph(std::string const &binaryPath)
  {
    this->parse(binaryPath);
  }

  Graph::~Graph()
  {
    if (_co)
    {
      delete _co;
      // delete _sts;
      // _sts = nullptr;
      // _co = nullptr;
    }
    // _binary.clear();
  }

  auto    Graph::getBGLGraph(void) const -> BGLGraph const &
  {
    return (this->_graph);
  }

  /*
  ** Loop over the graph and fill the BGL graph attribute.
  */
  auto    Graph::_initGraph(std::map<Dyninst::ParseAPI::Block const *, vertex_descriptor> &map_block_id,
                            std::map<vertex_descriptor, Dyninst::ParseAPI::Block const *> &) -> void
  {
    Dyninst::ParseAPI::Block const                *dyn_block;
    vertex_descriptor                        bgl_block_id;

    std::set<std::pair<vertex_descriptor, vertex_descriptor> >    removeDuplicate;
    std::pair<Graph::nodeIterator, Graph::nodeIterator>        bound;
    ParseAPI::Edge                        edgeProperty;

    // bound = vertices(_graph);
    BOOST_LOG_TRIVIAL(debug) << "Graph::" << __func__ << "(...)";
    for (auto map_block_desc = map_block_id.begin() ;
         map_block_desc != map_block_id.end() ; map_block_desc++)
    {
      std::cout << "1" << std::endl;
      // std::cout << "1n for" << std::endl;
      dyn_block = (*map_block_desc).first;
      bgl_block_id = (*map_block_desc).second;
      std::cout << "2" << std::endl;
      // std::cout << "Associate block " << bgl_block_id << std::endl;
      // node.setBlock(block);
      this->_graph[bgl_block_id].setBlock(dyn_block);
      // std::cout << "Associate block END " << bgl_block_id << std::endl;
      // _graph[*bound.first].setBlock(block);
      for (auto outEdge = dyn_block->targets().begin() ;
           outEdge != dyn_block->targets().end() ; outEdge++)
      {
        std::cout << "3" << std::endl;
        // std::cout << "2n for" << std::endl;
        auto    pair = std::make_pair(map_block_id[(*outEdge)->src()], map_block_id[(*outEdge)->trg()]);
        if (removeDuplicate.find(pair) == removeDuplicate.end())
        {
          // std::cout << "Add edge (" << map_block_id[(*outEdge)->src()]
          // << ", " << map_block_id[(*outEdge)->trg()] << ")" << std::endl;
          edgeProperty.setEdge((*outEdge));
          // std::cout << "Here" << std::endl;
          add_edge(map_block_id[(*outEdge)->src()], map_block_id[(*outEdge)->trg()], edgeProperty, _graph);
          // std::cout << "Here1" << std::endl;
          removeDuplicate.insert(pair);
          // std::cout << "Here2" << std::endl;
        }
        std::cout << "4" << std::endl;
      }
    }
    BOOST_LOG_TRIVIAL(trace) << "Graph::" << __func__ << "(...) END";
  }

  /*
  ** Create all blocks and edges while linking them together correctly.
  ** Attribute are not set here.
  */
  auto    Graph::_createBGLGraph(Dyninst::ParseAPI::CodeObject::funclist const &all) -> void
  {
    BOOST_LOG_TRIVIAL(debug) << "Graph::" << __func__ << "(...)";
    vertex_descriptor                            block_decs;
    // Dyninst::ParseAPI::Block const                    *dynBlock;
    std::map<vertex_descriptor, Dyninst::ParseAPI::Block const *>    map_id_block;
    std::map<Dyninst::ParseAPI::Block const *, vertex_descriptor>    map_block_id;
    std::pair<Graph::nodeIterator, Graph::nodeIterator>            bound;

    std::cout << "_createBGLGraph: 0" << std::endl;
    //BGLGraph g;
    _entryPointDescriptor = boost::add_vertex(this->_graph);
    std::cout << "_createBGLGraph: 0.5" << std::endl;
    for (auto function = all.begin() ; function != all.end() ; function++)
    {
      std::cout << "_createBGLGraph: 1" << std::endl;
      for (auto _block = (*function)->blocks().begin()
             ; _block != (*function)->blocks().end() ; _block++)
      {
        std::cout << "_createBGLGraph: 2" << std::endl;
        if (((*_block)->low() <= _entryPoint && _entryPoint <= (*_block)->high()) == false)
          block_decs = boost::add_vertex(_graph);
        else
          block_decs = _entryPointDescriptor;
        map_id_block[block_decs] = (*_block);
        map_block_id[(*_block)] = block_decs;
        std::cout << "_createBGLGraph: 3" << std::endl;
        // std::cout << "End loop (!block)" << std::endl;
      }
      std::cout << "End loop1 (!(*function))" << std::endl;
    }
    std::cout << "_createBGLGraph: 4" << std::endl;
    _initGraph(map_block_id, map_id_block);
    std::cout << "_createBGLGraph: 5" << std::endl;
    // bound = vertices(g);
    // while (bound.first != bound.second)
    // {
    //   g[*bound.first].format();
    //   bound.first++;
    // }
    // std::cout << "End1" << std::endl;
    BOOST_LOG_TRIVIAL(trace) << "Graph::" << __func__ << "(...) END";
  }


  auto    Graph::entryPoint(void) const -> Graph::vertex_descriptor
  {
    return (_entryPointDescriptor);
  }

  /*!
  ** TODO: check return value from SymtabCodeSource and CodeObject
  ** TODO: Regler les leaks
  */
  auto    Graph::parse(std::string const &binaryPath) -> Graph::BGLGraph const&
  {
    BOOST_LOG_TRIVIAL(debug) << "Graph::" << __func__ << "(...)";
/* Because SymtabCodeSource does not take a const (this leak !)  */
    this->_sts = new Dyninst::ParseAPI::SymtabCodeSource(strdup(binaryPath.c_str())); // Crash here
    this->_co = new Dyninst::ParseAPI::CodeObject(_sts);

    _entryPoint = _sts->getSymtabObject()->getEntryOffset();
    _entryPoint = 0x4005ab; // hard coded offset for the zero_div sampler binary (test only)
    _co->parse(_entryPoint, true);

    // unsigned int    i;
    // for (auto region : sts->regions())
    // {
    //     for (i = region->low() ; i <= region->high() ; i++) /* Force parsing all byte of .text of /bin/nm */
    //       co->parse(i, false);
    // }
    //_co->parse();
    _co->finalize();
    std::cout << "Binary parse by Dyninst" << std::endl;
// Dyninst::ParseAPI::CodeObject::funclist     &flist = co->funcs();
    std::cout << "Before _createBGLGraph" << std::endl;
    Barghest::Graph::BGLGraph g1;
    boost::add_vertex(g1);


    boost::write_graphviz(std::cout, g1);
    std::cout << "Before _createBGLGraph1" << std::endl;
    _createBGLGraph(_co->funcs());
    std::cout << "Boost BGL graph created and finished" << std::endl;
    // std::cout << "End2" << std::endl;
    this->_binary = binaryPath;

    BOOST_LOG_TRIVIAL(trace) << "Graph::" << __func__ << "(...) END";
    return (this->_graph);
/* delete co; */
  }
  // }; /* ! Graph */
} /* ! Barghest */
