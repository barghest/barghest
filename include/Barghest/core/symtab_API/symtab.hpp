// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#ifndef BARGHEST_SYMTAB_HH_
# define BARGHEST_SYMTAB_HH_
# pragma once

# include    <string>
# include    <dyninst/Symtab.h> // Dyninst symtab
# include    <dyninst/SymReader.h> // Dyninst symtab

typedef Dyninst::SymtabAPI::Archive            DynArchive;
typedef Dyninst::SymtabAPI::ExceptionBlock    DynExceptionB;
typedef Dyninst::SymtabAPI::Function        DynFunc;
typedef Dyninst::SymtabAPI::FunctionBase    DynFuncBase;
typedef Dyninst::SymtabAPI::LineNoTuple        DynLineNoYuple;
typedef Dyninst::SymtabAPI::localVar        DynlocalVar;
typedef Dyninst::SymtabAPI::Module            DynMod;
typedef Dyninst::SymtabAPI::Object            DynObject;
typedef Dyninst::SymtabAPI::ObjectType        DynObjectType;
typedef Dyninst::SymtabAPI::Region            DynRegion;
typedef Dyninst::SymtabAPI::relocationEntry    DynRelocatEntry;
typedef Dyninst::SymtabAPI::Segment            DynSegment;
typedef Dyninst::SymtabAPI::Statement        DynStatement;
typedef Dyninst::SymtabAPI::Symbol            DynSymbol;
typedef Dyninst::SymtabAPI::Symtab            DynSymtab;
typedef Dyninst::SymtabAPI::Type            DynType;
typedef Dyninst::SymtabAPI::Variable        DynVar;

namespace Barghest
{
  namespace core
  {
    namespace symtab_API
    {

      class symtab
      {
      public:
        symtab();
        symtab(MappedFile *);
        symtab(unsigned char *mem_image, size_t image_size,
               const std::string &name, bool defensive_binary, bool &err);

        ~symtab();

        /**************************************
         *** LOOKUP FUNCTIONS *****************
         **************************************/

        // Symbol

        auto findSymbol(const std::string& name, DynSymbol::SymbolType sType = DynSymbol::ST_UNKNOWN,
                        Dyninst::SymtabAPI::NameType nameType = Dyninst::SymtabAPI::anyName,
                        bool isRegex = false, bool checkCase = false,
                        bool includeUndefined = false) -> std::vector < DynSymbol* > ;
        auto getAllSymbols() -> std::vector < DynSymbol * > ;
        auto getAllSymbolsByType(DynSymbol::SymbolType sType) -> std::vector < DynSymbol * > ;
        auto findSymbolByOffset(Dyninst::Offset) -> std::vector<DynSymbol *> *;
        auto getAllUndefinedSymbols() -> std::vector < DynSymbol * > ;
        auto getAllDefinedSymbols() -> std::vector < DynSymbol * > ;


        // Function

        auto findFuncByEntryOffset(DynFunc*& ret, const Dyninst::Offset offset) -> bool;
        auto findFunctionsByName(std::string const& name, Dyninst::SymtabAPI::NameType nameType = Dyninst::SymtabAPI::anyName,
                                 bool isRegex = false, bool checkCase = true) -> std::vector < DynFunc * > ;
        auto getAllFunctions() -> std::vector < DynFunc * > ;
        auto getContainingFunction(Dyninst::Offset offset, DynFunc*& ret) -> bool;
        auto getContainingInlinedFunction(Dyninst::Offset offset, DynFuncBase*& ret) -> bool;

        // Variable
        auto findVariableByOffset(DynVar *&ret, const Dyninst::Offset offset) -> bool;
        auto findVariablesByName(std::string const& name,
                                 Dyninst::SymtabAPI::NameType nameType = Dyninst::SymtabAPI::anyName,
                                 bool isRegex = false, bool checkCase = true) -> std::vector < DynVar * > ;
        auto getAllVariables() -> std::vector < DynVar * > ;

        // Module

        auto getAllModules() -> std::vector < DynMod * > ;
        auto findModuleByOffset(DynMod *&ret, Dyninst::Offset off) -> bool;
        auto findModuleByName(DynMod *&ret, std::string const& name) -> bool;
        auto getDefaultModule() -> DynMod*;

        // Region

        auto getCodeRegions() -> std::vector < DynRegion * > ;
        auto getDataRegions() -> std::vector < DynRegion * > ;
        auto getAllRegions() -> std::vector < DynRegion * > ;
        auto getAllNewRegions() -> std::vector < DynRegion * > ;
        auto findRegion(DynRegion *&ret, std::string regname) -> bool;
        auto findRegion(DynRegion *&ret, const Dyninst::Offset addr, const unsigned long size) -> bool;
        auto findRegionByEntry(DynRegion *&ret, const Dyninst::Offset offset) -> bool;
        auto findEnclosingRegion(const Dyninst::Offset offset) -> DynRegion*;

        // Exceptions

        auto findException(DynExceptionB &excp, Dyninst::Offset addr) -> bool;
        auto getAllExceptions() -> std::vector < DynExceptionB * > ;
        auto findCatchBlock(DynExceptionB &excp, Dyninst::Offset addr, unsigned size = 0) -> bool;

        // Relocation entries

        auto getFuncBindingTable() -> std::vector < DynRelocatEntry > ;
        auto updateFuncBindingTable(Dyninst::Offset stub_addr, Dyninst::Offset plt_addr) -> bool;

        /**************************************
         *** SYMBOL ADDING FUNCS **************
         **************************************/

        auto addSymbol(DynSymbol* newSym) -> bool;
        auto addSymbol(DynSymbol* newSym, DynSymbol* referringSymbol) -> bool;
        auto createFunction(std::string name, Dyninst::Offset offset, size_t size, DynMod* mod = NULL) -> DynFunc*;
        auto createVariable(std::string name, Dyninst::Offset offset, size_t size, DynMod* mod = NULL) -> DynVar*;

        auto deleteFunction(DynFunc* func) -> bool;
        auto deleteVariable(DynVar* var) -> bool;


        /*****Query Functions*****/

        auto isExec() -> bool;
        auto isStripped() -> bool;
        auto getObjectType() const-> DynObjectType;
        auto getArchitecture() -> Dyninst::Architecture;
        auto isCode(const Dyninst::Offset where) -> bool;
        auto isData(const Dyninst::Offset where) -> bool;
        auto isValidOffset(const Dyninst::Offset where) -> bool;

        auto isNativeCompiler() -> bool;
        auto getMappedRegions() -> std::vector < DynRegion * > ;

        /***** Line Number Information *****/

        auto getAddressRanges(std::string lineSource, unsigned int LineNo) -> std::vector < std::pair<Dyninst::Offset, Dyninst::Offset> > ;
        auto getSourceLines(Dyninst::Offset addressInRange) -> std::vector < DynStatement * > ;
        // auto getSourceLines(Dyninst::Offset addressInRange) -> std::vector<DynLineNoYuple>;
        auto addLine(std::string lineSource, unsigned int lineNo, unsigned int lineOffset,
                     Dyninst::Offset lowInclAddr, Dyninst::Offset highExclAddr) -> bool;
        bool addAddressRange(Dyninst::Offset lowInclAddr, Dyninst::Offset highExclAddr, std::string const& lineSource,
                             unsigned int lineNo, unsigned int lineOffset = 0);
        void setTruncateLinePaths(bool value);
        bool getTruncateLinePaths();

        /***** Type Information *****/

        auto findType(DynType *&type, std::string const& name) -> bool;
        auto findType(unsigned type_id) -> DynType*;
        auto findVariableType(DynType *&type, std::string const& name) -> bool;
        auto addType(DynType *type) -> bool;
        auto parseTypesNow() -> void;

        /***** Local Variable Information *****/

        auto findLocalVariable(std::string const& name) -> std::vector < DynlocalVar * > ;

        /***** Relocation Sections *****/

        auto hasRel() const -> bool;
        auto hasRela() const -> bool;
        auto hasReldyn() const -> bool;
        auto hasReladyn() const -> bool;
        auto hasRelplt() const -> bool;
        auto hasRelaplt() const -> bool;
        auto isStaticBinary() const -> bool;


        /***** Write Back binary functions *****/

        auto emitSymbols(DynObject *linkedFile, std::string const& filename, unsigned flag = 0) -> bool;
        auto addRegion(Dyninst::Offset vaddr, void *data, unsigned int dataSize,
                       std::string const& name, DynRegion::RegionType rType_, bool loadable = false,
                       unsigned long memAlign = sizeof(unsigned), bool tls = false) -> bool;
        auto addRegion(DynRegion *newreg) -> bool;
        auto emit(std::string const& filename, unsigned flag = 0) -> bool;
        auto addDynLibSubstitution(std::string const& oldName, std::string const& newName) -> void;
        auto getDynLibSubstitution(std::string const& name) -> std::string;
        auto getSegments() -> std::vector < DynSegment > ;
        auto fixup_code_and_data(Dyninst::Offset newImageOffset, Dyninst::Offset newImageLength,
                                 Dyninst::Offset newDataOffset, Dyninst::Offset newDataLength) -> void;
        auto fixup_RegionAddr(const char* name, Dyninst::Offset memOffset, long memSize) -> bool;
        auto fixup_SymbolAddr(const char* name, Dyninst::Offset newOffset) -> bool;
        auto updateRegion(const char* name, void *buffer, unsigned size) -> bool;
        auto updateCode(void *buffer, unsigned size) -> bool;
        auto updateData(void *buffer, unsigned size) -> bool;
        auto getFreeOffset(unsigned size) -> Dyninst::Offset;
        auto addLibraryPrereq(std::string const& libname) -> bool;
        auto addSysVDynamic(long name, long value) -> bool;
        auto addLinkingResource(DynArchive *library) -> bool;
        auto getLinkingResources() -> std::vector < DynArchive * > ;
        auto addExternalSymbolReference(DynSymbol *externalSym, DynRegion *localRegion, DynRelocatEntry localRel) -> bool;
        auto addTrapHeader_win(Dyninst::Address ptr) -> bool;
        auto updateRelocations(Dyninst::Address start, Dyninst::Address end, DynSymbol *oldsym, DynSymbol *newsym) -> bool;


        /***** Data Member Access *****/

        auto file() const-> std::string;
        auto name() const-> std::string;
        auto memberName() const-> std::string;
        auto mem_image() const -> char *;
        auto imageOffset() const-> Dyninst::Offset;
        auto dataOffset() const-> Dyninst::Offset;
        auto dataLength() const-> Dyninst::Offset;
        auto imageLength() const-> Dyninst::Offset;
        auto getInitOffset() -> Dyninst::Offset;
        auto getFiniOffset() -> Dyninst::Offset;
        auto getInterpreterName() const -> const char*;
        auto getAddressWidth() const -> unsigned;
        auto getLoadOffset() const-> Dyninst::Offset;
        auto getEntryOffset() const-> Dyninst::Offset;
        auto getBaseOffset() const-> Dyninst::Offset;
        auto getTOCoffset(DynFunc *func = NULL) const-> Dyninst::Offset;
        auto getTOCoffset(Dyninst::Offset off) const-> Dyninst::Offset;
        auto getLoadAddress() -> Dyninst::Offset;
        auto isDefensiveBinary() const -> bool;
        auto fileToDiskOffset(Dyninst::Offset) const-> Dyninst::Offset;
        auto fileToMemOffset(Dyninst::Offset) const-> Dyninst::Offset;
        auto getDefaultNamespacePrefix() const-> std::string;
        auto getNumberofRegions() const -> unsigned;
        auto getNumberofSymbols() const -> unsigned;

        auto getDependencies() -> std::vector<std::string>&;
        auto removeLibraryDependency(std::string const& lib) -> bool;

        auto getParentArchive() const-> DynArchive *;

        /***** Error Handling *****/

        auto delSymbol(DynSymbol *sym) -> bool;
        auto deleteSymbol(DynSymbol *sym) -> bool;
        // undefined symbol getSymbolByIndex() in Dyninst lib
        // auto getSymbolByIndex(unsigned i) -> DynSymbol *;

        // Used by binaryEdit.C...

        auto canBeShared() -> bool;
        auto getOrCreateModule(std::string const& modName, const Dyninst::Offset modAddr) -> DynMod*;

        //Only valid on ELF formats

        auto getElfDynamicOffset() -> Dyninst::Offset;

        // SymReader interface

        auto getSegmentsSymReader() -> std::vector < Dyninst::SymSegment > ;
        auto rebase(Dyninst::Offset offset) -> void;

        //

        auto getLastReturn() const -> bool;

      private:
        DynSymtab*        s_;
        bool            ret_;

      }; /* !class symtab */

    } /* !namespace symtab_API */
  } /* !namespace core */
} /* !namespace Barghest */

#endif /* !BARGHEST_SYMTAB_HH_ */
