#ifndef BARGHEST_BOOST_EVENT_FILTERS_HH_
# define BARGHEST_BOOST_EVENT_FILTERS_HH_

# include "Barghest/core/emulation_API/boost_event_filters/on_discover_vertex.hpp"
# include "Barghest/core/emulation_API/boost_event_filters/on_examine_edge.hpp"
# include "Barghest/core/emulation_API/boost_event_filters/on_finish_vertex.hpp"
# include "Barghest/core/emulation_API/boost_event_filters/on_start_vertex.hpp"

#endif /* !BARGHEST_BOOST_EVENT_FILTERS_HH_ */
