#ifndef BARGHEST_DEREFERENCE_HH_
# define BARGHEST_DEREFERENCE_HH_

# include <string>
# include <dyninst/Dereference.h>
# include "Barghest/core/instruction_API/expression.hh"

namespace Barghest
{
  namespace core
  {
    namespace instruction_API
    {
      namespace expressions
      {
	class dereference : public expression
	{
	public:
	  dereference(Dyninst::InstructionAPI::Expression::Ptr expr);
	  // Nothing may be copied implicitely
	  dereference(dereference const &) = delete;
	  virtual ~dereference();

	  auto	get_binary_func_expr(void) const -> instruction_API::expression const &;

	  virtual auto format(void) const -> std::string;
	  virtual auto get_raw_expr(void) const
	    -> Dyninst::InstructionAPI::Expression::Ptr;
	  virtual auto get_raw(void) const
	    -> Dyninst::InstructionAPI::Dereference::Ptr;
	  virtual auto set_raw(Dyninst::InstructionAPI::Dereference::Ptr)
	    -> dereference const &;

	  /*!
	  ** @See "instruction_API/expression.hpp" for documentation about this
	  ** function.
	  */
	  virtual auto is_deref(void) const -> bool;

    private:
	  Dyninst::InstructionAPI::Dereference::Ptr	dyn_deref_;
	  instruction_API::expression			*binary_func_expr_;

	}; // !class dereference
      } // !namespace expressions
    } // !namespace instruction_API
  } // !namespace core
} // !namespace Barghest

#endif /* !BARGHEST_DEREFERENCE_HH_ */
