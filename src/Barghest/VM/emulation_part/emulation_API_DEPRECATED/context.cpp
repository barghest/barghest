// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#include <dyninst/Immediate.h>
#include <dyninst/Dereference.h>
#include <boost/log/trivial.hpp>

#include "Barghest/core/emulation_API/context.hh"
#include "Barghest/core/emulation_API/context_helpers.hh"

namespace bfa = bitField::BitFieldArithmetic;

/* TODO: retirer ces anciens defines des que tout sera remis au propre */

/*!
** Assume the operand is not an empty one
*/
# define _GET_OPERAND_EXPR_TYPEID(op)                                   \
  (typeid(*((static_cast<Dyninst::InstructionAPI::Operand const &>(op)).getValue())))
# define IS_OPERAND_REG(op)     (_GET_OPERAND_EXPR_TYPEID(op) == typeid(Dyninst::InstructionAPI::RegisterAST))
# define IS_OPERAND_DEREF(op)   (_GET_OPERAND_EXPR_TYPEID(op) == typeid(Dyninst::InstructionAPI::Dereference))
# define IS_OPERAND_IMM(op)     (_GET_OPERAND_EXPR_TYPEID(op) == typeid(Dyninst::InstructionAPI::Immediate))

/* Return the base register of a Dyninst::MachRegister (Ex: BASEA, BASEDI, etc) */
/* (reg & 0111b) (0x0 -> 0xf) */
/* Cf: /usr/include/dyn_regs.h */
# define GET_BASE_MACH_REGISTER(reg)    ((static_cast<Dyninst::MachRegister const &>(reg)).val() & 0xff)
# define GET_MACH_REGISTER_CATEGORY(reg)    ((static_cast<Dyninst::MachRegister const &>(reg)).val() & 0xff0000)

/* Return the 'class MachRegister' associated with a RegisterAST object (here more to reduce define size) */
# define GET_MACH_REGISTER(reg) ((static_cast<Dyninst::InstructionAPI::RegisterAST const &>(reg)).getID())
/* Return the base register of a Dyninst::InstructionAPI::RegisterAST::Ptr */
# define GET_BASE_REGISTER(reg) (GET_BASE_MACH_REGISTER((GET_MACH_REGISTER(reg))))
/* Return true if the machRegister correspond to a GeneralPurposeRegister */
# define IS_GPR_REGISTER(reg) (GET_MACH_REGISTER_CATEGORY((GET_MACH_REGISTER(reg))) == Dyninst::x86::GPR)

/*!
** Convert the value of a Dyninst::InstructionAPI::Immediate into a size_t
*/
# define GET_IMM_VAL(imm)                                               \
  ((static_cast<Dyninst::InstructionAPI::Immediate const &>(imm)).eval().convert<size_t>())

/*!
** Check the register subrange.
** Ex: in 64b:
** * rax is a FULL register
** * eax is a DW (Double Word) register
** * ax is a W (Word) register
** * al is a L (Low) register
** * ah is a H (High) register
** Cf: DyninstAPI-8.1.2/common/src/dyn_regs.C and /usr/include/dyn_regs.h
** Ps: value of x86::FULL and x86_64::FULL are the same, but need to find a more clean way to do.
*/
# define GET_REGISTER_RAW_VAL(reg) (GET_MACH_REGISTER(reg).val())
# define IS_REGISTER_FULL(reg)    ((GET_REGISTER_RAW_VAL(reg) & 0x0000ff00) == Dyninst::x86::FULL)
# define IS_REGISTER_L(reg)       ((GET_REGISTER_RAW_VAL(reg) & 0x0000ff00) == Dyninst::x86::L_REG)
# define IS_REGISTER_H(reg)       ((GET_REGISTER_RAW_VAL(reg) & 0x0000ff00) == Dyninst::x86::H_REG)
# define IS_REGISTER_W(reg)       ((GET_REGISTER_RAW_VAL(reg) & 0x0000ff00) == Dyninst::x86::W_REG)
# define IS_REGISTER_DW(reg)      ((GET_REGISTER_RAW_VAL(reg) & 0x0000ff00) == Dyninst::x86_64::D_REG)

  namespace Barghest
  {
    namespace core
    {
      namespace emulation_API
      {

        // TODO: Pour les saves context, faire une list de changement a restore (comme git/emacs)
        context::context()
        {
          BOOST_LOG_TRIVIAL(debug) << "context::" << __func__ << "()";
          size_t    i;
          // auto    it = this->get_registers_mutable().begin();

          // i = -1;
          // while (it != this->get_registers_mutable().end())
          // {
          //   init_value.setMem(&(*it)).reset();
          //   it++;
          // }
          i = -1;
          while (++i < (sizeof(fct_) / sizeof(*fct_)))
            fct_[i] = &context::unknown_inst_;
          fct_[::e_mov] = &context::mov_emul_;
          fct_[::e_add] = &context::add_emul_;
          fct_[::e_sub] = &context::sub_emul_;
          fct_[::e_shl_sal] = &context::shl_sal_emul_;
          BOOST_LOG_TRIVIAL(trace) << "context::" << __func__ << "() END";
          /* TODO: A RETIRER: PAtch pour le forum eip pour l'unsigned */
          // fct_[::e_sar] = &context::shl_sal_emul_;
        }

        context::~context()
        {
          BOOST_LOG_TRIVIAL(debug) << "context::" << __func__ << "()";
          // std::cout << "context destructor called" << std::endl;
        }

        context::context(context const &other)
        {
          BOOST_LOG_TRIVIAL(debug) << "context::" << __func__ << "(context const &other)";
          (void)this->operator=(other);
        }

        auto    context::copy_trace(t_trace const &other_trace) -> void
        {
          BOOST_LOG_TRIVIAL(trace) << "context::" << __func__ << "(t_trace const &other_trace)";
          auto    it = other_trace.begin();

          this->get_trace_mutable().clear(); // In order to be sure
          for (; it != other_trace.end() ; it++)
          {
            // this->get_trace_mutable().push_back(std::make_pair((*it).first, (*it).second));
            this->get_trace_mutable().push_back((*it));
          }
        }

        /*
        ** Internal notes:
        ** We do not use vector::operator=() because it won't know how to make a copy
        ** of the reference in the std::pair (and we not not want to make full copy of
        ** instruction_API::instruction class.). Instead we perform the copy by hand
        ** element after element.
        */
        auto    context::operator=(context const &other) -> context &
        {
          BOOST_LOG_TRIVIAL(debug) << "context::" << __func__ << "(context const &other)";
          // this->get_trace_mutable().clear();
          this->copy_trace(other.get_trace());
          this->get_registers_mutable() = other.get_registers();
#if 0
          // std::cout << "Clear ?" << std::endl;
          // trace_ = other.get_trace();
          // std::cout << "Clear done" << std::endl;
          for (auto pair = other.get_trace().begin() ; pair != other.get_trace().end() ; pair++)
          {
            // std::cout << "push_back" << std::endl;
            this->trace_.push_back(*pair);
            // std::cout << "First: " << pair.first << std::endl;
            // trace_.push_back(std::make_pair(pair.first, pair.second));
            // std::cout << "Second: " << pair.second.format() << std::endl;
          }
          for (auto base_bitfield = other.get_registers().begin() ;
               base_bitfield != other.get_registers().end() ; base_bitfield++)
            registers_[(*base_bitfield).first] = (*base_bitfield).second;
          // std::cout << "end" << std::endl;
#endif /* 0 */
          BOOST_LOG_TRIVIAL(trace) << "context::" << __func__ << "(context const &other) END";
          return (*this);
        }

        /**/

        auto    context::get_trace(void) const -> t_trace const &
        {
          return (this->trace_);
        }
        auto    context::get_trace_mutable(void) -> t_trace &
        {
          return (this->trace_);
        }
        /**/
        auto    context::get_registers(void) const -> t_reg const &
        {
          return (this->registers_);
        }
        auto    context::get_registers_mutable(void) -> t_reg &
        {
          return (this->registers_);
        }

        /**/

        auto    context::emulate(std::pair<size_t, instruction_API::instruction const &> inst) -> void
        {
          BOOST_LOG_TRIVIAL(debug) << "context::" << __func__ << "(std::pair<size_t, instruction_API::instruction const &> inst) -> " << inst.second.format();
          // this->get_trace_mutable().push_back(std::make_pair(offset, b.getInsn(offset)));
          // std::cout << "Emulated: " << inst.second.format() << std::endl;
          this->get_trace_mutable().push_back(inst);
          (this->*(fct_[inst.second.get_operation().get_ID()]))(inst.first, inst.second,
                                                                inst.second.get_operands());
          BOOST_LOG_TRIVIAL(trace) << "context::" << __func__ << "(std::pair<size_t, instruction_API::instruction const &> inst) END";
        }

        auto    context::print_trace(std::ostream& stream) const -> void
        {
          auto    flags_backup = stream.flags();
          for (auto inst = this->get_trace().begin() ; inst != this->get_trace().end() ; inst++)
          {
            stream << std::hex << std::showbase << (*inst).first << ": "
                   << ((*inst).second).format() << std::endl;
          }
          stream << std::endl;
          stream.setf(flags_backup);
        }

        auto    context::get_value_last_return(void) -> MultiValEmulator<size_t> &
        {
          return (this->tmp_storage_);
        }

/* C++ code to emulation instruction in seperate files */
# define __BARGHESTCONTEXT_INST_INCLUDE
#  include "_context_unknown_inst.i"
#  include "_context_add_emul.i"
#  include "_context_sub_emul.i"
#  include "_context_mov_emul.i"
#  include "_context_shl_sal_emul.i"
# undef __BARGHESTCONTEXT_INST_INCLUDE

      } // !namespace emulation_API
    } // !namespace core
  } // ! namespace Barghest
