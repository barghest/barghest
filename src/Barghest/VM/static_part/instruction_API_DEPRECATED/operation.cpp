// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#include "Barghest/core/instruction_API/operation.hh"

namespace Barghest
{
  namespace core
  {
    namespace instruction_API
    {
      operation::operation(Dyninst::InstructionAPI::Instruction::Ptr instruction)
        : inst_(instruction)
      { }

      auto operation::format(void) const -> std::string
      {
        return get_raw().format();
      }


      /// Returns the entry ID corresponding to this operation.  Entry IDs are
      /// enumerated values that correspond
      /// to assembly mnemonics. (Ex: e_jmp/e_call/e_add)
      auto operation::get_ID(void) const -> inst_ID
      {
        return get_raw().getID();
      }


      /// Returns the prefix entry ID corresponding to this operation, if any.
      /// Prefix IDs are enumerated values that correspond to assembly prefix mnemonics.
      auto operation::get_prefix_ID(void) const -> inst_prefix_ID
      {
        return get_raw().getPrefixID();
      }


      /// Return a reference on the Dyninst Object wrapped.
      auto operation::get_raw(void) const -> Dyninst::InstructionAPI::Operation const &
      {
        return inst_->getOperation();
      }
    }
  }
}
