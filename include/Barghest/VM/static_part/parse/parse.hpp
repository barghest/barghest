// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#ifndef PARSE_HPP
# define PARSE_HPP

# include <string>
# include <capstone/capstone.h> // capstone disassembler engine
# include "Barghest/VM/static_part/symtab/symtab.hpp"

namespace Barghest
{
  namespace VM
  {
    namespace static_part
    {
      namespace parse
      {

        // /*!
        // ** Main class to work to construct the instruction list.
        // */
        // template <typename I/*instruction*/, typename S/*symtab*/>
        // class flow_graph
        // {
        //   flow_graph(I &insns, S const &symtab);
        //   ~flow_graph();

        //   // TODO: logging ?

        //   /*!
        //   ** Run all the decompiler.
        //   */
        //   auto  construct(void) -> bool;

        //   /*!
        //   ** Return the name of the sub module.
        //   */
        //   auto  name(void) const -> std::string const &;
        // };

        /*!
        ** If an error occured, the given container is guaranted to end in a
        ** valid state, and left unchanged.
        ** Memory: It's up to the user to free the memory in insns.
        */
        template <template <typename = cs_insn *, typename ...ARGS> class I
                  /*instructions*/
                  , typename S/*symtab*/
                  , typename D/*Disassembler engine*/>
        class decompiler
        {
        public:
          decompiler(I<> &insns, S &symtab, D &disass_engine_handle)
            : insns_(insns), symtab_(symtab), disass_engine_handle_(disass_engine_handle)
          {
          }

          virtual auto  run(void) -> bool = 0;
          auto  get_insns(void) const -> I<> & { return (this->insns_); }
          auto  get_symtab(void) const -> S & { return (this->symtab_); }
          auto  get_disass(void) const -> D & { return (this->disass_engine_handle_); }

          // callback ?
          // virtual auto  on_instruction(void) -> bool = 0;

        private:
          I<>    &insns_;
          S      &symtab_;
          D      &disass_engine_handle_;
        };

        /*!
        ** Disassemble everything blindly, byte by byte. This is the most
        ** exhaustive and expensive disassembling possible since each byte
        ** is considered as the beginning of a valid instruction.
        */
        template <typename I, typename S, typename D>
        class disassemble_linear_exhaustive// : public decompiler<I, S, D>
        {
        public:
          auto  run(void) -> bool;
        };

        /*!
        ** Perform a raw disassembling of the binary. It start to disassemble
        ** on a known entry (function symbol, entry point, etc) until the first
        ** error.
        ** If the entry is already disassembled, does nothing.
        */
        template <template <typename = cs_insn> class I, typename S, typename D>
        class  disassemble_linear_smart : public decompiler<I, S, D>
        {
          auto  run(void) -> bool;
        };

        /*!
        ** Resolves all known
        */
        template <template <typename = cs_insn> class I, typename S, typename D>
        class flow_tracing_static : public decompiler<I, S, D>
        {
          auto  run(void) -> bool;
        };

        template <template <typename = cs_insn> class I, typename S, typename D>
        class flow_tracing_emulation : public decompiler<I, S, D>
        {
          auto  run(void) -> bool;
        };

# define PARSE_IMPL_HPP
# include "Barghest/VM/static_part/parse/parse_impl.hpp"
# undef PARSE_IMPL_HPP

      } // !namespace parse
    } // !namespace static_part
  } // !namespace VM
} // !namespace Barghest

#endif /* !PARSE_HPP */
