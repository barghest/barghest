## Copyright (c) 2016, Jean-Baptiste Laurent
## All rights reserved.
##
## Redistribution and use in source and binary forms, with or without
## modification, are permitted provided that the following conditions are met:
##
## 1. Redistributions of source code must retain the above copyright notice, this
##    list of conditions and the following disclaimer.
## 2. Redistributions in binary form must reproduce the above copyright notice,
##    this list of conditions and the following disclaimer in the documentation
##    and/or other materials provided with the distribution.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
## ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
## WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
## DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
## ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
## (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
## LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
## ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
## (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## The views and conclusions contained in the software and documentation are those
## of the authors and should not be interpreted as representing official policies,
## either expressed or implied, of the Barghest Project.
##

include(ExternalProject)

###########################################
# boost

add_definitions(-DBOOST_LOG_DYN_LINK)

find_package(Boost 1.54 REQUIRED graph log)

set(deps_libraries ${deps_libraries} ${Boost_LIBRARIES})

###########################################
# barghestAPI

# ExternalProject_Add(BarghestAPI
#   # GIT_REPOSITORY "git@bitbucket.org:krauser/barghestapi.git"
#   DOWNLOAD_COMMAND ""
#   GIT_TAG develop
#   CONFIGURE_COMMAND ""
#   BUILD_COMMAND ""
#   INSTALL_COMMAND ""
#   PREFIX ${CMAKE_BINARY_DIR}/deps)

# set(dependencies ${dependencies} BarghestAPI)

# include_directories("${CMAKE_BINARY_DIR}/deps/src/BarghestAPI/include")

###########################################
# barghestGUI-vanilla

# ExternalProject_Add(barghestGUI-vanilla_
#   GIT_REPOSITORY "git@bitbucket.org:krauser/barghestgui-vanilla.git"
#   GIT_TAG develop
#   PREFIX ${CMAKE_BINARY_DIR}/deps
#   INSTALL_COMMAND "")

# link_directories("${CMAKE_BINARY_DIR}/deps/src/barghestGUI-vanilla_-build/")

# set(dependencies ${dependencies} barghestGUI-vanilla_)
# set(deps_libraries ${deps_libraries} barghestGUI-vanilla)

###########################################
# libBitField

ExternalProject_Add(libbitField
  GIT_REPOSITORY "git@bitbucket.org:Barghest/libbitField.git"
  # DOWNLOAD_COMMAND ""
  GIT_TAG develop
  PREFIX ${CMAKE_BINARY_DIR}/deps
  INSTALL_COMMAND "")

link_directories("${CMAKE_BINARY_DIR}/deps/src/libbitField-build/")
include_directories("${CMAKE_BINARY_DIR}/deps/src/libbitField/include/")

set(dependencies ${dependencies} libbitField)
set(deps_libraries ${deps_libraries} bitField)

###########################################
# multiValEmulator

ExternalProject_Add(libValEmulator
  GIT_REPOSITORY "git@bitbucket.org:Barghest/libValEmulator.git"
  # DOWNLOAD_COMMAND ""
  GIT_TAG develop
  PREFIX ${CMAKE_BINARY_DIR}/deps
  INSTALL_COMMAND "")

link_directories("${CMAKE_BINARY_DIR}/deps/src/libValEmulator-build/")
include_directories("${CMAKE_BINARY_DIR}/deps/src/libValEmulator/include/")

set(dependencies ${dependencies} libValEmulator)
set(deps_libraries ${deps_libraries} ValEmulator)

###########################################
# dyninst ## -> No more need for dyninst

# find_package(Dyninst)

# set(deps_libraries ${deps_libraries} parseAPI instructionAPI common symtabAPI)

###########################################
# libelf

set(deps_libraries ${deps_libraries} elf capstone)
