// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#ifndef __BARGHESTCONTEXT_INST_INCLUDE
# error "BarghestContext instruction cpp can't be compiled alone (see context.cpp)"
#endif /* !__BARGHESTCONTEXT_INST_INCLUDE */

auto    context::unknown_inst_(size_t ,
                   instruction_API::instruction const &inst,
                   std::vector<instruction_API::operand> const &operands) -> void
{

  BOOST_LOG_TRIVIAL(debug) << "context::" << __func__ << "(...) -> " << inst.format();
  BOOST_LOG_TRIVIAL(info) << "Instruction [" << inst.format() << "] is not emulated yet.";

  static MultiValEmulator<size_t>    const undef_val; // All bits are auto set to "Undefined"

  for (auto &op : operands) // Only reset written operand
  {
    if (op.is_written())
    {
      /* std::cout << "Get_value() " << get_value(*this, op) << std::endl; */
      set_value(*this, op, undef_val);
      /* std::cout << "Operand " << op.format() << " is written & has been reset" << std::endl; */
    }
  }
}
