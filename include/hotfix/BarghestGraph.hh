///
/// \file BarghestGraph.hh
/// 
/// \author Jean-Baptiste Laurent
/// \<jeanbaptiste.laurent.pro@gmail.com\>
/// 
/// \date Started on  Wed Jun 25 17:08:55 2014 Jean-Baptiste Laurent
/// \date Last update Mon Jul 27 19:57:52 2015 Jean-Baptiste Laurent
/// \brief File description to place here
///

#ifndef BARGHEST_GRAPH_HH_
# define BARGHEST_GRAPH_HH_

// TODO: Revoir les includes vraiment necessaires
# include <string>
# include <boost/config.hpp>
# include <boost/graph/adjacency_list.hpp>
//# include <CodeObject.h> // Dyninst::ParseAPI::*

#ifdef WIN32
# include <Dyninst/include/CodeObject.h> // Dyninst::ParseAPI::*
#else
# include <dyninst/CodeObject.h> // Dyninst::ParseAPI::*
#endif // WIN32

// # include "BarghestBlock.hh"
// # include "BarghestEdge.hh"
#include "Barghest/core/parse_API/block.hpp"
#include "Barghest/core/parse_API/edge.hpp"

    /*!
    ** TODO: ajouter des subgraphs pour tenir compte des fonctions de Dyninst
    ** see: http://www.boost.org/doc/libs/1_55_0/libs/graph/doc/subgraph.html
    */
    // class GraphProperties
    // {
    // public:

    //   // std::map<boost::numeric::interval<size_t>, Graph::vertex_descriptor>	allBlocks;
    //   /*!
    //   ** The name of the binary loaded.
    //   */
    //   std::string	binary;

    //   /*!
    //   ** Little trix to use the BGL graph as a class.
    //   */
    //   void		*graph;
    // };

namespace Barghest
{
  class Graph
  {
  public:
    typedef boost::adjacency_list<boost::setS, boost::vecS, boost::bidirectionalS,
				  Barghest::core::parse_API::block,
				  Barghest::core::parse_API::edge>	BGLGraph;
    typedef BGLGraph::vertex_descriptor				vertex_descriptor;
    typedef boost::graph_traits<BGLGraph>::vertex_iterator	nodeIterator;
    typedef boost::graph_traits<BGLGraph>::in_edge_iterator	inEdgeIterator;
    typedef boost::graph_traits<BGLGraph>::out_edge_iterator	outEdgeIterator;
  private:
    BGLGraph		_graph;



    size_t		_entryPoint;
    // TODO: a changer
    vertex_descriptor	_entryPointDescriptor;

  public:
    // TODO: a changer
    Dyninst::ParseAPI::SymtabCodeSource	*_sts;
    Dyninst::ParseAPI::CodeObject	*_co;
    /*!
    ** The name of the binary loaded.
    */
    std::string		_binary;

    Graph();
    Graph(std::string const &binaryPath);
    ~Graph();

    /*!
    ** Load the binary with the given path and initialise all necessary metadata.
    ** If this method is called with an other graph, the first one is cleared.
    ** TODO: Clear the BGL graph if we parse a new one.
    ** @param binaryPath The path of the binary to load.
    ** @return A const reference to a BoostGraphLibrary graph.
    */
    auto	parse(std::string const &binaryPath) -> BGLGraph const &;

    /*!
    ** Return the descriptor of the vertex containing the block where is situated
    ** the entry point of the program. (by default the node '0' if the BGL graph
    ** is always the root)
    */
    auto	entryPoint(void) const -> vertex_descriptor;

    /*!
    ** @return A const reference to a BoostGraphLibrary graph.
    */
    auto	getBGLGraph(void) const -> BGLGraph const &;
  private:
    auto	_initGraph(std::map<Dyninst::ParseAPI::Block const *, vertex_descriptor> &map_block_id,
			   std::map<vertex_descriptor, Dyninst::ParseAPI::Block const *> &map_id_block) -> void;
    auto	_createBGLGraph(Dyninst::ParseAPI::CodeObject::funclist const &all) -> void;
  }; /* ! class Graph */
} /* !namespace Barghest */

#endif /* !BARGHEST_GRAPH_HH_ */
