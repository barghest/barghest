// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#include <set>

#include "Barghest/core/instruction_API/instruction.hh"
#include "Barghest/core/instruction_API/expressions/register.hh"

namespace Barghest
{
  namespace core
  {
    namespace instruction_API
    {
      instruction::instruction(Dyninst::InstructionAPI::Instruction::Ptr const
                               instruction)
        : inst_(instruction), operation_(instruction)
      {
        read_memory_ = -1;
        write_memory_ = -1;
        get_raw()->getOperands(dyn_operands_);
        get_operands(operands_);
#ifdef BARGHEST_FULL_PRECACHED_INSTRUCTION
        std::set<Dyninst::InstructionAPI::RegisterAST::Ptr> dyn_regs;

        read_memory_ = get_raw()->readsMemory();
        write_memory_ = get_raw()->writesMemory();
        get_raw()->getReadSet(dyn_regs);
        for (auto &dyn_regist : dyn_regs)
          regs_read_.insert(Barghest::instruction_API::register_t(dyn_regist));
        dyn_regs.clear();
        get_raw()->getWriteSet(dyn_regs);
        for (auto &dyn_reg : dyn_regs)
          regs_written_.insert(Barghest::instruction_API::register_t(dyn_reg));
#endif /* !BARGHEST_FULL_PRECACHED_INSTRUCTION */
      }

      auto instruction::get_raw(void) const
        -> Dyninst::InstructionAPI::Instruction::Ptr const
      {
        return (inst_);
      }

      auto instruction::get_operation(void) const -> const operation &
      {
        return (operation_);
      }

      auto instruction::get_operands(void) const -> std::vector<operand> const &
      {
        return (operands_);
      }

      auto instruction::get_operands(std::vector<operand> &operands) const -> void
      {
        for (auto const &operand_elem : dyn_operands_)
          operands.push_back(operand(operand_elem, get_raw()->getArch()));
      }

      auto instruction::get_operand(unsigned int index) const -> operand const &
      {
        if (index < operands_.size())
          return (operands_[index]);
        throw std::out_of_range("Index " + std::to_string(index) + "out of range "
                                + "for " + this->format());
        // return (operand(get_raw()->getOperand(index), get_raw()->getArch()));
      }

      auto instruction::raw_byte(unsigned int index) const -> unsigned char
      {
        return (get_raw()->rawByte(index));
      }

      auto instruction::size(void) const -> size_t
      {
        return (get_raw()->size());
      }

      auto instruction::ptr(void) const -> void const *
      {
        return (get_raw()->ptr());
      }

      auto instruction::format(void) const -> std::string
      {
        return (this->get_raw()->format());
      }
    }
  }
}
