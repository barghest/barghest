// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#ifndef SYMTAB_HPP
# define SYMTAB_HPP

# include <cstdlib> //
# include <string> // std::string
# include <vector> // std::vector
# include <utility> // std::pair

// # include <memory> // std::shared_ptr

# include "Barghest/VM/vm_errno.hpp"

namespace Barghest
{
  namespace VM
  {
    namespace static_part
    {
      namespace symtab
      {

        struct mapped_region
        {
          std::string   name;
          size_t        start_addr;
          size_t        size;
          size_t        offset;
          struct        right
          {
            bool        r; // read
            bool        w; // write
            bool        x; // exec
            bool        s; // shared
            bool        p; // private (or copy-on-write)
          }             right;
          // std::shared_ptr<void const *>   mem;
          void          *mem;
        };

        /*
        ** Dev notes for symtab: I used exception return style because
        ** * need to have a detailed error message (without integrating log object
        **   hosted by the VM)
        ** * might need latter to add an error code (like std::system_error)
        ** * return_value style would be much more complexe for the same things
        **   (was there before rewriting with exept (with enum class, get_err_msg(),
        **   set_err_msg(), err_code, map err_msg_list, etc (errno style))
        ** * Simplify usage of symtab (we do not want code that looks the one we
        **   would have with libelf (need to check return val for every single call,
        **   and we can need 5+ function call to get what we want, the code get
        **   polluted with more error management code than the "usefull" main code
        **   (see "libelf by example" by Joseph koshy for code))
        ** * Split error management code from the main code
        ** * train myself with exception programming
        */
        /*!
        ** Interface for all the child class managing a specific file format
        ** Exception safety: strong
        */
        class symtab
        {
        public:
          symtab(std::string const &file_name = "");
          virtual ~symtab();

          /*!
          ** Open the given file. If a file has already been opened,
          ** close it first.
          ** @exception
          */
          virtual auto    open(std::string const &file_name) -> void;

          /*!
          ** Close the file previously opened.
          ** @exception
          */
          virtual auto    close(void) -> void;


          /*!
          ** Fill entry with the entry point of the underlaying file format.
          ** @return true if there is an entry point, false otherwise
          */
          virtual auto  get_entry_point(size_t &entry) const -> bool;

          // /*!
          // ** @return true, or false if an error occured
          // */
          // auto    map_section(std::string const &section_name,
          //                   void *&memory,
          //                   size_t &size) -> bool
          // {
          // }

          // /*!
          // **
          // */
          // auto    get_fct_addr(void) const -> std::vector<intptr_t>
          // {
          // }

          /*!
          ** @return The name of the file opened or an empty string.
          */
          virtual auto    get_file_name(void) const -> std::string const &;

          // // Error management part
          // /*!
          // ** Return the last error that occured.
          // */
          // virtual auto  get_last_error(int error = -1) = 0;

          /*!
          ** Allocate and append to 'mr' all mapped regions defined in the given
          ** file. Mapped regions returned should be passed to free_mapped_region()
          ** when it is no longer needed to avoid memory leaks.
          ** @param mr The container to append the mapped region into
          ** @exception TODO to add
          */
          virtual auto  get_mapped_region(std::vector<mapped_region> &mr) const -> void;

          /*!
          ** Free the memory allocated previously and clear the allocator.
          */
          virtual auto  free_mapped_region(std::vector<mapped_region> &mr) const -> void;

          // protected:
          // public:
          //   static Barghest::VM::err_map<T>               VM_ERR_MAP_NAME;
          //   vm_errno<symtab, err_map<T>, err_id::success> VM_ERR_OBJ_NAME;

        private:
          symtab    *sff_; // symtab_file_formats
          // std::vector< >    mapped_region_;
        };

      } // !namespace symtab
    } // !namespace static_part
  } // !namespace VM
} // !namespace Barghest

// typedef struct s_section
// {
//   std::string const    &name;
//   int            right;
//   void            *mem;
//   unsigned int        mem_size;
// } t_section;

#endif /* !SYMTAB_HPP */
