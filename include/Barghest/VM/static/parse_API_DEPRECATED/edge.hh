#pragma once

#ifndef BARGHEST_EDGE_HH__
# define BARGHEST_EDGE_HH__

# include <dyninst/Edge.h>
# include <dyninst/CFG.h>

namespace Barghest
{
  namespace core
  {
    namespace parse_API
    {
      class edge
      {
      public:
	using edge_type = Dyninst::ParseAPI::EdgeTypeEnum;

	auto set_edge(Dyninst::ParseAPI::Edge const *edge) -> void;
	auto get_edge(void) const -> Dyninst::ParseAPI::Edge const *;
	auto type(void) const -> edge_type;
	auto interproc(void) const -> bool;

      private:
	Dyninst::ParseAPI::Edge	const *edge_;

      }; // !class edge
    } // !namespace parse_API
  } // !namespace core
} //! namespace Barghest

#endif /* !BARGHEST_EDGE_HH__ */
