// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#ifndef BLOCK_HPP_
# define BLOCK_HPP_

# include "Barghest/VM/types.hpp"

namespace Barghest
{
  namespace VM
  {
    namespace static_part
    {
      namespace parse
      {

        class block
        {
        public:
          /*!
          ** Construct a basic block with the address of the first instruction
          ** and the byte past the end. (so we have the interval "[start, end[")
          */
          block(off_t start = 0, off_t end = 0);
          ~block();

          /*!
          ** @return the offset of the first instruction
          ** of the basic bloc.
          */
          auto  start(void) const -> off_t;
          auto  first(void) const -> off_t;

          /*!
          ** @return the offset of the first byte after the basic bloc.
          */
          auto  end(void) const -> off_t;

          /*!
          ** @return the offset of the last instruction in the basic bloc.
          */
          auto  last(void) const -> off_t;


          auto  set_end(off_t end) -> void;
          auto  set_start(off_t start) -> void;

          /*!
          ** Clear all information stored and reset them to zero.
          */
          auto  reset(void) -> void;

          auto  set(off_t start, off_t end) -> void;
          /*!
          ** @return the size in byte of the basic block.
          */
          auto  size(void) const -> off_t;
        private:
          off_t start_;
          off_t end_;
          // insns instructions;
        };

      } // !namespace parse
    } // !namespace static_part
  } // !namespace VM
} // !namespace Barghest

#endif /* !BLOCK_HPP_ */
